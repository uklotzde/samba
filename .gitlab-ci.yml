variables:
  GIT_SUBMODULE_STRATEGY: recursive
  TEST_COVERAGE_THRESHOLD: 1 # in percent

stages:
  - build
  - test
  - statistics
  - publish
  - pages

build:linux:websrv:
  stage: build
  image: nixos/nix:latest
  script:
    - cd samba-backend/websrv
    - >
      nix --extra-experimental-features "nix-command flakes" develop --command
      cargo build --release
  artifacts:
    name: "$CI_COMMIT_REF_NAME-websrv-snapshot"
    paths:
      - target/release/samba-backend-websrv
    expire_in: 1 week
  cache:
    key: shared-cache
    paths:
      - target/
  rules:
    - changes:
      - ./*
      - samba-backend/**/*
      - samba-shared/**/*

build:wasm:webapp:
  stage: build
  image: nixos/nix:latest
  script:
    - cd samba-frontend/webapp
    - >
      nix --extra-experimental-features "nix-command flakes" develop --command
      npm i daisyui
    - >
      nix --extra-experimental-features "nix-command flakes" develop --command
      trunk build --release --features csr
  artifacts:
    name: "$CI_COMMIT_REF_NAME-webapp-snapshot"
    paths:
      - samba-frontend/webapp/dist
    expire_in: 1 week
  cache:
    key: shared-cache-wasm
    paths:
      - target/
  rules:
    - changes:
      - ./*
      - samba-frontend/**/*
      - samba-shared/**/*

# testing disabled until there is a test suite; running it will require
# some workaround concerning sys dependencies like speechdispatcher and
# custom builds like tauri
test:rust:
  stage: test
  image: nixos/nix:latest
  script:
    - >
      nix --extra-experimental-features "nix-command flakes" develop --command
      cargo test --workspace
  when: manual

test:pre-commit:
  stage: test
  image: nixos/nix:latest
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  script:
    - >
      nix develop --extra-experimental-features "nix-command flakes" --command
      pre-commit run --all-files --show-diff-on-failure
  cache:
    paths:
      - ${PRE_COMMIT_HOME}
  when: manual

coverage:rust:
  stage: statistics
  image: nixos/nix:latest
  script:
    - >
      nix --extra-experimental-features "nix-command flakes" develop --command
      cargo tarpaulin --fail-under $TEST_COVERAGE_THRESHOLD --out Xml
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: cobertura.xml
  when: manual

linting_and_sec:rust:
  stage: test
  image: realcundo/docker-rust-aio
  script:
    - cargo clippy
    - cargo fmt -- --check
    # include docs to see warnings about bad docstrings
    - cargo doc
    - cargo deny check
  when: manual

.publish:dev:
  stage: publish
  image: curlimages/curl:latest
  needs: ["build:linux:websrv"]
  script:
    - cd samba-backend/target/release
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./samba-backend-websrv "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/samba-backend/0.0.0/samba-backend-websrv-linux_x86_64"'
  rules:
    - if: '$CI_COMMIT_TAG == null && $CI_MERGE_REQUEST_ID == null && $CI_COMMIT_REF_NAME == "master"'
      when: manual

publish:tag:
  stage: publish
  image: curlimages/curl:latest
  dependencies: ["build:linux:websrv"]
  script:
    - export VERSION="$CI_COMMIT_TAG"
    - cd samba-backend/target/release
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./samba-backend "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/samba-backend/$VERSION/samba-backend-linux_x86_64"'
  rules:
    - if: '$CI_COMMIT_TAG != null'
      when: manual

pages:
  image: nixos/nix:latest
  stage: pages
  needs: []
  script:
    - cd website && nix --extra-experimental-features nix-command --extra-experimental-features flakes develop .#website -c zola build
    # GL pages needs the artifact at the root directory
    - cd .. && cp -r website/public ./public
  artifacts:
    paths:
      - public
    expire_in: 1 week
  rules:
    - if: ($CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG)
      changes:
        - website/**/*
