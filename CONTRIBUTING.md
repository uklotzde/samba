# Contributing to samba

## Setting up the development environment

This project uses Nix (flakes) to provide a reproducible environment for
development and CI. The hassle free development setup therefore is as simple
as installing [Nix](https://nixos.org) and running `nix develop`. You will be
dropped into a development shell and are finished.

*If* you are not fond of using Nix, here are some of the things that you should
install:

- Rust's nightly toolchain (for the specific components and versions, if any,
  see [`rust-toolchain.toml`](./rust-toolchain.toml).
- [pre-commit](https://pre-commit.com) for running lints etc. on committing.
  Passing the project's pre-commit hooks is mandatory for MRs. For the hooks to
  pass locally, you must have the tools installed which are run by those hooks.
  Look into the [`.pre-commit-config.yaml`](./.pre-commit-config.yaml) to see
  which tools that may be.
- [trunk](https://trunkrs.dev) to run the development build for the frontend
  conveniently.
- [hurl](https://hurl.dev) for easier debugging sessions with the backend,
  especially after resetting the database.
- [audiowaveform](https://github.com/bbc/audiowaveform) and
  [weasyprint](https://weasyprint.org) as runtime dependencies of the backend.

The above list is not meant to be exhaustive, the flake is the single source of
truth for the project's dependencies and development tooling.

## Issues and merge requests

In general, merge requests (MRs) are appreciated … for anything, be it
something from the issue tracker or an own idea. As a rule of thumb:

- Things that have specific issues outlining development ideas, create a MR, it
  will probably pass with minor discussion.
- Things that have a general tracking issue outlining a complete feature set:
  it might be worth to discuss your implementation idea first (be it in an
  issue or on Matrix) but MRs are welcome anyway; if you don't discuss before
  opening, expect a discussion on the overall architecture/strategy in the MR.
- Things without issue/not on the ROADMAP: if you think, it fits into the scope
  of the project, treat it as if it was the point above. If it's a dedicated
  feature for a niche use case, definitely discuss first (it's not said that it
  won't be included, but the discussion may be longer than necessary on
  potential MRs).

Summary: if in doubt, ask.

If you tackle a specific issue, it is appreciated if you also announce it in
the issue's thread so that duplication of effort can be avoided.

## Commits

This project does not use gitmoji nor conventional commits nor anything else
providing structure within commits. Instead, please use small commits with
comprehensive commit messages. Non-empty commit message bodies are not
frowned upon.

Please also try to keep your commits focused. It may be okay to update a
dependency in your otherwise feature-oriented commit. But please don't do
mass refactorings or major reorderings in commits that also change code
functionally.

Should your commit be related to an issue or previous MR, please reference
it in the commit message (title or body) so that GitLab can provide
cross-referencing support.

## Non-technical contributions

If you are not keen on programming but would like to contribute to the project,
here are a few items that are always appreciated:

- documentation improvements, including writing a user guide
- creating a website (using a static site generator) for the project
- spreading the word about the software (in its early stages primarily to
  attract contributors)
- contributing ideas about ballroom practice workflows you would like to see
  supported by software
- … anything you think you could contribute, just get in touch

Again, feel free to get in touch and there will be a way for you to join the
effort to bring dedicated software to the ballroom.
