CREATE TABLE events (
  id           INTEGER   NOT NULL PRIMARY KEY AUTOINCREMENT,
  date         DATETIME  NOT NULL,
  title        TEXT      NOT NULL,
  description  TEXT      NOT NULL,
  location     TEXT      NOT NULL
);
CREATE INDEX events_by_date ON events (date);

CREATE TABLE event_competition_timetables (
  event_id   INTEGER   NOT NULL,
  start_time DATETIME  NOT NULL,
  end_time   DATETIME  NOT NULL,
  -- todo: use class, group etc. instead of
  --       plain comment field
  comment    TEXT,
  PRIMARY KEY(event_id, start_time, end_time, comment)
  FOREIGN KEY(event_id) REFERENCES events(id)
);

CREATE TABLE event_couples (
  event_id  INTEGER NOT NULL,
  couple_id INTEGER NOT NULL,
  PRIMARY KEY(event_id, couple_id)
  FOREIGN KEY(event_id) REFERENCES events(id)
  FOREIGN KEY(couple_id) REFERENCES couples(id)
);

CREATE TABLE event_helpers (
  event_id  INTEGER NOT NULL,
  member_id INTEGER NOT NULL,
  PRIMARY KEY(event_id, member_id)
  FOREIGN KEY(event_id) REFERENCES events(id)
  FOREIGN KEY(member_id) REFERENCES members(id)
);

CREATE TABLE event_organizers (
  event_id  INTEGER NOT NULL,
  member_id INTEGER NOT NULL,
  PRIMARY KEY(event_id, member_id)
  FOREIGN KEY(event_id) REFERENCES events(id)
  FOREIGN KEY(member_id) REFERENCES members(id)
);

CREATE TABLE event_playlists (
  event_id INTEGER NOT NULL,
  -- the id of aoide's playlist
  playlist TEXT    NOT NULL,
  comment  TEXT,
  PRIMARY KEY(event_id, playlist)
  FOREIGN KEY(event_id) REFERENCES events(id)
);

CREATE TABLE event_tags (
  event_id INTEGER NOT NULL,
  tag      TEXT    NOT NULL,
  PRIMARY KEY(event_id, tag)
  FOREIGN KEY(event_id) REFERENCES events(id)
);
