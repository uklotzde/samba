CREATE TABLE figures (
  id        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  title     TEXT    NOT NULL,
  start     TEXT,
  end       TEXT    NOT NULL,
  notes     TEXT,
  level_req INTEGER NOT NULL,
  FOREIGN KEY(level_req) REFERENCES level_requirements(id)
);

CREATE TABLE level_requirements (
  id        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  comment   TEXT
);
INSERT INTO level_requirements (id, comment)
VALUES (0, 'ToBeDetermined'), (1, 'WdsfDCSyllabus'), (2, 'WdsfBSyllabus'), (3, 'Open');

CREATE TABLE figure_steps (
  figure_id    INTEGER NOT NULL,
  leader       BOOLEAN NOT NULL,
  sequence_idx INTEGER NOT NULL,
  step_id      INTEGER NOT NULL,
  PRIMARY KEY(figure_id, leader, sequence_idx, step_id)
  FOREIGN KEY(figure_id) REFERENCES figures(id)
  FOREIGN KEY(step_id)   REFERENCES steps(id)
);

CREATE TABLE steps (
  id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  -- whether this moves the left or right foot
  right_foot      BOOLEAN NOT NULL,
  -- step duration in positive number of semiquavers
  timing          INTEGER NOT NULL,
  -- whole percent of weight to transfer
  weight_transfer INTEGER NOT NULL,
  comment         TEXT,
  CONSTRAINT check_timing CHECK (timing > 0)
  CONSTRAINT check_weight_transfer CHECK (weight_transfer >= 0 AND weight_transfer <= 100)
);
