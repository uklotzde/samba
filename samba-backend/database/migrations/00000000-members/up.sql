CREATE TABLE members (
  id           INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  display_name TEXT    NOT NULL,
  -- unique username is an extra field and not the primary key to avoid
  -- touching other tables that reference the member if the username is
  -- changed
  username     TEXT    NOT NULL UNIQUE,
  salted_pass  TEXT    NOT NULL,
  -- this will actually be a JSON object, so in sqlite we will store it as text
  -- but other backends (like postgres) might use a dedicated type, if null,
  -- default settings are to be assumed
  preferences  TEXT
);

CREATE TABLE couples (
  id        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  leader    INTEGER NOT NULL,
  follower  INTEGER NOT NULL,
  from_year INTEGER,
  to_year   INTEGER,
  FOREIGN KEY(leader) REFERENCES members(id)
  FOREIGN KEY(follower) REFERENCES members(id)
);
