CREATE TABLE practice_sessions (
  -- the playlist id in the aoide database
  id           TEXT    NOT NULL PRIMARY KEY,
  creator      INTEGER NOT NULL,
  kind         TEXT    NOT NULL,
  FOREIGN KEY(creator) REFERENCES members(id)
  CONSTRAINT check_kind CHECK (kind in ('history', 'template'))
);

CREATE TABLE practice_session_tags (
  -- the playlist id in the aoide database
  id       TEXT NOT NULL,
  tag      TEXT NOT NULL,
  PRIMARY KEY(id, tag)
  FOREIGN KEY(id) REFERENCES practice_sessions(id)
);
