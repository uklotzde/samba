CREATE TABLE routines (
  id             INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  title          TEXT,
  description    TEXT,
  notes          TEXT,
  choreographer  TEXT,
  -- TODO: think about relational representation (for now
  -- comma-separated string)
  performers     TEXT    NOT NULL,
  -- TODO: think about relational representation (for now
  -- comma-separated string)
  demonstrations TEXT    NOT NULL,
  -- temporary representation of steps: JSON of the routine
  -- elements; TODO: replace with relational model
  steps          TEXT    NOT NULL
);

CREATE TABLE routine_dances (
  routine_id    INTEGER NOT NULL,
  dance_id      INTEGER NOT NULL,
  PRIMARY KEY(routine_id, dance_id)
  FOREIGN KEY(routine_id) REFERENCES routines(id)
  FOREIGN KEY(dance_id) REFERENCES dances(id)
);

CREATE TABLE routine_tracks (
  routine_id    INTEGER NOT NULL,
  track_id      INTEGER NOT NULL,
  PRIMARY KEY(routine_id, track_id)
  FOREIGN KEY(routine_id) REFERENCES routines(id)
);
