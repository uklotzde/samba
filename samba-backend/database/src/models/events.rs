use chrono::NaiveDateTime;
use samba_shared::events::{Event as SharedEvent, StoredEvent};

use super::Member;
use crate::schema::{
    event_competition_timetables, event_helpers, event_organizers, event_tags, events,
};

#[derive(Debug, Clone, Identifiable, Insertable, Queryable, AsChangeset)]
#[diesel(primary_key(id))]
pub struct Event {
    pub id: i32,
    pub date: NaiveDateTime,
    pub title: String,
    pub description: String,
    pub location: String,
}

impl Event {
    #[must_use]
    pub fn to_be_created(evt: SharedEvent) -> Self {
        Self::with_id(0, evt)
    }

    #[must_use]
    pub fn with_id(id: i32, evt: SharedEvent) -> Self {
        StoredEvent {
            identifier: id,
            value: evt,
        }
        .into()
    }
}

impl From<StoredEvent> for Event {
    fn from(evt: StoredEvent) -> Self {
        Self {
            id: evt.identifier,
            date: evt.value.date,
            title: evt.value.title,
            description: evt.value.description,
            location: evt.value.location,
        }
    }
}

// TODO: adjust core type and this implementation
impl From<Event> for StoredEvent {
    fn from(evt: Event) -> Self {
        Self {
            identifier: evt.id,
            value: SharedEvent {
                date: evt.date,
                title: evt.title,
                description: evt.description,
                location: evt.location,
                organizers: vec![],
                tags: vec![],
                competition_details: None,
                helpers: vec![],
            },
        }
    }
}

#[derive(Debug, Clone, Associations, Identifiable, Insertable, Queryable)]
#[diesel(
  primary_key(event_id, tag),
  belongs_to(Event, foreign_key = event_id),
  table_name = event_tags
)]
pub struct EventTag {
    pub event_id: i32,
    pub tag: String,
}

#[derive(Debug, Clone, Associations, Identifiable, Insertable, Queryable)]
#[diesel(
  primary_key(event_id, member_id),
  belongs_to(Event, foreign_key = event_id),
  belongs_to(Member, foreign_key = member_id),
  table_name = event_helpers
)]
pub struct EventHelper {
    pub event_id: i32,
    pub member_id: i32,
}

#[derive(Debug, Clone, Associations, Identifiable, Insertable, Queryable)]
#[diesel(
  primary_key(event_id, member_id),
  belongs_to(Event, foreign_key = event_id),
  belongs_to(Member, foreign_key = member_id),
  table_name = event_organizers
)]
pub struct EventOrganizer {
    pub event_id: i32,
    pub member_id: i32,
}

#[derive(Debug, Clone, Associations, Identifiable, Insertable, Queryable)]
#[diesel(
  primary_key(event_id, start_time, end_time, comment),
  belongs_to(Event, foreign_key = event_id),
  table_name = event_competition_timetables
)]
pub struct EventCompetitionTimetableEntry {
    pub event_id: i32,
    pub start_time: NaiveDateTime,
    pub end_time: NaiveDateTime,
    pub comment: String,
}
