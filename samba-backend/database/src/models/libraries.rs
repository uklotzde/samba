use crate::schema::libraries;

#[derive(Debug, Clone, Insertable, Queryable, AsChangeset)]
#[diesel(primary_key(id), table_name = libraries)]
pub struct Library {
    pub id: String,
    pub name: String,
    pub path: String,
    pub aoide_collection: String,
    pub scan_subdirectories: bool,
    pub shared: bool,
}

impl From<samba_shared::libraries::Library> for Library {
    fn from(lib: samba_shared::libraries::Library) -> Self {
        Self {
            id: lib.id,
            name: lib.name,
            path: lib.path,
            aoide_collection: lib.aoide_collection,
            scan_subdirectories: lib.scan_subdirectories,
            shared: lib.shared,
        }
    }
}

impl From<Library> for samba_shared::libraries::Library {
    fn from(lib: Library) -> Self {
        Self {
            id: lib.id,
            name: lib.name,
            path: lib.path,
            aoide_collection: lib.aoide_collection,
            scan_subdirectories: lib.scan_subdirectories,
            shared: lib.shared,
            // TODO: can only be resolved from aoide
            excluded_directories: vec![],
        }
    }
}
