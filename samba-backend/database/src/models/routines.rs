use samba_shared::routines::{Routine as SharedRoutine, StoredRoutine};

use crate::schema::routines;

// TODO: properly model steps
#[derive(Debug, Clone, Insertable, Queryable, AsChangeset)]
#[diesel(primary_key(id))]
pub struct Routine {
    pub id: i32,
    pub title: Option<String>,
    pub description: Option<String>,
    pub notes: Option<String>,
    pub choreographer: Option<String>,
    pub performers: String,
    pub demonstrations: String,
    pub steps: String,
}

impl Routine {
    #[must_use]
    pub fn to_be_created(rtn: SharedRoutine) -> Self {
        Self::with_id(0, rtn)
    }

    #[must_use]
    pub fn with_id(id: i32, rtn: SharedRoutine) -> Self {
        StoredRoutine {
            identifier: id,
            value: rtn,
        }
        .into()
    }
}

impl From<StoredRoutine> for Routine {
    fn from(rtn: StoredRoutine) -> Self {
        Self {
            id: rtn.identifier,
            title: rtn.value.title,
            description: rtn.value.description,
            notes: rtn.value.notes,
            choreographer: rtn.value.choreographer,
            performers: rtn.value.performers.join(","),
            demonstrations: rtn.value.demonstration.join(","),
            steps: serde_json::to_string(&rtn.value.steps)
                .expect("Steps can always be serialized to JSON."),
        }
    }
}

impl From<Routine> for StoredRoutine {
    fn from(rtn: Routine) -> Self {
        Self {
            identifier: rtn.id,
            value: SharedRoutine {
                title: rtn.title,
                description: rtn.description,
                choreographer: rtn.choreographer,
                performers: rtn
                    .performers
                    .split(',')
                    .filter(|perf| !perf.is_empty())
                    .map(Into::into)
                    .collect::<Vec<_>>(),
                demonstration: rtn
                    .demonstrations
                    .split(',')
                    .filter(|demo| !demo.is_empty())
                    .map(Into::into)
                    .collect::<Vec<_>>(),
                steps: serde_json::from_str(&rtn.steps)
                    .expect("The database only contains valid JSON."),
                notes: rtn.notes,
            },
        }
    }
}
