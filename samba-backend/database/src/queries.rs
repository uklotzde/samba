use diesel::result::Error as DieselError;
use thiserror::Error;

use super::GatekeeperError;

pub mod dances;
pub mod events;
pub mod libraries;
pub mod members;
pub mod routines;
pub mod track_cache;

#[derive(Error, Debug)]
pub enum QueryError {
    #[error("Interacting with the database failed: {0}")]
    DatabasePooling(#[from] GatekeeperError),
    #[error("Deleting from the database failed: {0}")]
    DatabaseDeletion(DieselError),
    #[error("Inserting into database failed: {0}")]
    DatabaseInsertion(DieselError),
    #[error("Updating the database failed: {0}")]
    DatabaseUpdate(DieselError),
    #[error("Listing results failed: {0}")]
    ResultListing(DieselError),
    #[error("Fetching specific row {0} failed.")]
    RowIdentification(String),
}
