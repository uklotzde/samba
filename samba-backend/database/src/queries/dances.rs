use diesel::{prelude::*, result::Error as DieselError};
use samba_shared::dancing::Dance;

use super::QueryError;
use crate::Gatekeeper;

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_all(db_gatekeeper: &Gatekeeper) -> Result<Vec<Dance>, QueryError> {
    let dance_vec = db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::{models::Dance, schema::dances::dsl::*};
                dances.load::<Dance>(connection)
            })
        })
        .await?
        .map_err(QueryError::ResultListing)?;

    let mut shared_dance_vec = Vec::with_capacity(dance_vec.len());
    for db_dance in dance_vec {
        shared_dance_vec.push(db_dance.into());
    }

    Ok(shared_dance_vec)
}
