use diesel::{prelude::*, result::Error as DieselError};
use samba_shared::libraries::Library;

use super::QueryError;
use crate::{models::Library as DbLibrary, Gatekeeper};

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_by_id(
    db_gatekeeper: &Gatekeeper,
    library_id: &str,
) -> Result<Library, QueryError> {
    let query_library_id = library_id.to_string();

    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::libraries::dsl::*;

                libraries
                    .filter(id.eq(&query_library_id))
                    .first::<DbLibrary>(connection)
                    .map(Into::into)
            })
        })
        .await?
        .map_err(|_err| QueryError::RowIdentification(library_id.to_string()))
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_all(db_gatekeeper: &Gatekeeper) -> Result<Vec<Library>, QueryError> {
    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::libraries::dsl::*;

                libraries
                    .load::<DbLibrary>(connection)
                    .map(|vec| vec.into_iter().map(Into::into).collect::<Vec<Library>>())
            })
        })
        .await?
        .map_err(QueryError::ResultListing)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn create(db_gatekeeper: &Gatekeeper, library: Library) -> Result<(), QueryError> {
    let insertable: DbLibrary = library.into();

    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::libraries::dsl::*;

                diesel::insert_into(libraries)
                    .values(&insertable)
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseInsertion)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn update(db_gatekeeper: &Gatekeeper, library: Library) -> Result<(), QueryError> {
    let query_library_id = library.id.clone();
    let updateable: DbLibrary = library.into();

    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::libraries::dsl::*;

                diesel::update(libraries.filter(id.eq(query_library_id.as_str())))
                    .set(&updateable)
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseUpdate)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn delete(db_gatekeeper: &Gatekeeper, library_id: &str) -> Result<(), QueryError> {
    let query_library_id = library_id.to_string();

    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::libraries::dsl::*;

                diesel::delete(libraries.filter(id.eq(&query_library_id)))
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseDeletion)
}
