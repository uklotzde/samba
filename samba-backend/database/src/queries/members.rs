use diesel::{prelude::*, result::Error as DieselError};

use super::QueryError;
use crate::{models::Member as DbMember, Gatekeeper};

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_by_id(db_gatekeeper: &Gatekeeper, user_id: i32) -> Result<DbMember, QueryError> {
    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::members::dsl::*;

                members.filter(id.eq(user_id)).first::<DbMember>(connection)
            })
        })
        .await?
        .map_err(|_err| QueryError::RowIdentification(format!("{user_id}")))
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_by_name(
    db_gatekeeper: &Gatekeeper,
    user_name: &str,
) -> Result<DbMember, QueryError> {
    let query_user_name = user_name.to_string();

    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::members::dsl::*;

                members
                    .filter(username.eq(&query_user_name))
                    .first::<DbMember>(connection)
            })
        })
        .await?
        .map_err(|_err| QueryError::RowIdentification(user_name.to_string()))
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn update(db_gatekeeper: &Gatekeeper, member: DbMember) -> Result<(), QueryError> {
    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::members::dsl::*;

                diesel::update(members.filter(id.eq(member.id)))
                    .set(member)
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseUpdate)
}
