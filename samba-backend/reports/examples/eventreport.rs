use std::path::Path;

use samba_backend_reports::generate_event_report_at;
use samba_shared::events::Event;

#[tokio::main]
async fn main() {
    // HINT: do not use on windows ;)
    let path = Path::new("/tmp/out.pdf");

    if generate_event_report_at(
        &Event {
            id: 0,
            date: "2022-03-12T10:00:00".parse().unwrap(),
            title: "Sample event".into(),
            description: "Lengthy description with useless details".into(),
            location: "Somewhere over the rainbow".into(),
            organizers: vec!["Nobody but the organizing club".into()],
            tags: vec!["tag".into(), "tag".into()],
            competition_details: None,
            helpers: vec![],
        },
        path,
    )
    .await
    .is_err()
    {
        eprintln!("Report generation failed");
    } else {
        println!("Enjoy your report at {:?}", path);
    };
}
