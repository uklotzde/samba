use std::path::Path;

use askama::Template;
use samba_shared::events::Event;

use super::{call_weasyprint, prepare_report_directory, ReportError};

#[derive(Template)]
#[template(path = "event.html")]
struct EventTemplate<'a> {
    title: &'a str,
    date: &'a str,
    location: &'a str,
    organizer: &'a str,
    tags: Vec<&'a str>,
    timetable: Vec<(&'a str, &'a str, Option<&'a str>)>,
    playlist: Vec<(&'a str, &'a str, Option<&'a str>)>,
}

#[tracing::instrument]
pub async fn generate_event_report_at(event: &Event, target: &Path) -> Result<(), ReportError> {
    let working_dir = tempfile::tempdir().map_err(|e| {
        tracing::debug!("Failed to create temp dir: {}", e);
        ReportError::IOError
    })?;

    prepare_report_directory(working_dir.path()).await?;
    call_weasyprint(
        &EventTemplate {
            title: &event.title,
            date: &event.date.to_string(),
            location: &event.location,
            organizer: &event.organizers.join(", "),
            tags: vec![],
            timetable: vec![],
            playlist: vec![],
        }
        .render()
        .map_err(|e| {
            tracing::debug!("Calling weasyprint failed: {}", e);
            ReportError::ReportGenerationFailed
        })?,
        working_dir.path(),
        target,
    )
    .await
}
