use std::path::Path;

use askama::Template;
use maud::{html, Markup};
use samba_shared::routines::{
    timing_summary_for_routine, Figure, Foot, LevelRequirement, Routine, RoutineElement, Step,
    StepTiming, WeightTransfer,
};

use super::{call_weasyprint, prepare_report_directory, ReportError};

#[derive(Template)]
#[template(path = "routine.html")]
struct RoutineTemplate<'a> {
    title: &'a str,
    choreographer: &'a str,
    performers: &'a str,
    description: &'a str,
    notes: &'a str,
    demonstrations: &'a [String],
    steps: Vec<(&'a str, &'a str, Option<&'a str>)>,
    step_details: Vec<(&'a str, &'a str)>,
}

#[tracing::instrument]
pub async fn generate_routine_export_at(
    routine: &Routine,
    target: &Path,
) -> Result<(), ReportError> {
    let working_dir = tempfile::tempdir().map_err(|e| {
        tracing::debug!("Failed to create temp dir: {}", e);
        ReportError::IOError
    })?;

    prepare_report_directory(working_dir.path()).await?;

    let mut steps = vec![];
    let mut step_details = vec![];
    // Use a dedicated counter to skip separators in enumeration.
    let mut step_counter = 1;
    for element in &routine.steps {
        let (numbered, title, timing) = match element {
            RoutineElement::Figure(figure @ Figure { title, .. }) => {
                let (leaders_steps, followers_steps) = figure.steps();

                (
                    true,
                    title.clone(),
                    Some(if !leaders_steps.is_empty() {
                        let leader_timing =
                            timing_summary_for_routine(std::iter::once(element.clone()));
                        let follower_timing =
                            timing_summary_for_routine(std::iter::once(RoutineElement::Figure(
                                Figure::new("Follower's steps".into())
                                    .with_steps(followers_steps.to_vec(), leaders_steps.to_vec())
                                    .ok_or_else(|| {
                                        tracing::error!(
                                            "Valid steps expected because previously part of a \
                                             valid figure."
                                        );
                                        ReportError::ReportGenerationFailed
                                    })?,
                            )));

                        if leader_timing == follower_timing {
                            leader_timing
                        } else {
                            format!("Leader: {leader_timing}\nFollower: {follower_timing}")
                        }
                    } else {
                        "—".into()
                    }),
                )
            },
            RoutineElement::Step(Step { comment, .. }) => (
                true,
                format!(
                    "Step{}",
                    comment
                        .as_ref()
                        .map(|c| format!(" ({c})"))
                        .unwrap_or_default()
                ),
                Some(timing_summary_for_routine(std::iter::once(element.clone()))),
            ),
            RoutineElement::Pause(_) => (
                true,
                "Pause".into(),
                Some(timing_summary_for_routine(std::iter::once(element.clone()))),
            ),
            RoutineElement::Stop => (false, "Stop routine".into(), None),
            RoutineElement::Repeat(skip) => (
                false,
                format!(
                    "Repeat from beginning{}",
                    (skip.to_semiquavers() != 0)
                        .then(|| format!(" (skip {} beats)", skip.to_semiquavers().div_ceil(4)))
                        .unwrap_or_default()
                ),
                None,
            ),
            RoutineElement::Separator(comment) => (
                false,
                comment
                    .as_ref()
                    .map(|c| format!("Note: {c}"))
                    .unwrap_or_default(),
                None,
            ),
        };

        let breakdown_html = routine_element_details(element);
        let num_str = numbered
            .then(|| format!("{step_counter}"))
            .unwrap_or_default();
        steps.push((num_str.clone(), title, timing));
        if !breakdown_html.0.is_empty() {
            step_details.push((num_str, html! {
                div style="text-align: left;" {
                    (breakdown_html)
                }
            }));
        }

        step_counter += numbered as usize;
    }

    call_weasyprint(
        &RoutineTemplate {
            title: routine.title.as_deref().unwrap_or("Untitled routine"),
            choreographer: routine.choreographer.as_deref().unwrap_or_default(),
            performers: routine.performers.join(", ").as_str(),
            description: routine.description.as_deref().unwrap_or_default(),
            notes: routine.notes.as_deref().unwrap_or_default(),
            demonstrations: &routine.demonstration,
            steps: steps
                .iter()
                .map(|(title, notes, timing)| (title.as_str(), notes.as_str(), timing.as_deref()))
                .collect(),
            step_details: step_details
                .iter()
                .map(|(num, html)| (num.as_str(), html.0.as_str()))
                .collect(),
        }
        .render()
        .map_err(|e| {
            tracing::debug!("Calling weasyprint failed: {}", e);
            ReportError::ReportGenerationFailed
        })?,
        working_dir.path(),
        target,
    )
    .await
}

/// Generate a valid HTML representation of a [`Step`]. Used for step and
/// figure representation.
fn step_html(
    Step {
        foot,
        timing,
        weight_transfer,
        comment,
    }: &Step,
    hide_leader: bool,
    hide_follower: bool,
) -> Markup {
    let comment_str = comment
        .as_ref()
        .map(|c| format!(" ({c})"))
        .unwrap_or_default();
    let timing = match timing {
        StepTiming::A => "“a”".into(),
        StepTiming::And => "“and” (&amp;)".into(),
        StepTiming::Quick => "quick".into(),
        StepTiming::Slow => "slow".into(),
        StepTiming::Custom(semiquavers) => format!("{semiquavers}&nbsp;𝅘𝅥𝅯"),
    };
    let weight_transfer = match weight_transfer {
        WeightTransfer::None => "none".into(),
        WeightTransfer::Half => "half".into(),
        WeightTransfer::Full => "full".into(),
        WeightTransfer::Custom(percent) => format!("{percent}&nbsp;%"),
    };

    html! {
        p style="width: 100%;" {
            "Step"
            (comment_str)
        }
        div style="display: flex; justify-content: space-between; margin-top: -1rem;" {
            div {
                img src="fa-solid-clock.svg" style="height: 1ex; margin-right: 1rem;";
                (timing)
            }
            @if !hide_leader {
                div {
                    img src="fa-solid-person.svg" style="height: 1.5ex; margin-right: 1rem;";
                    @if *foot == Foot::Left {
                        "left"
                    } @else {
                        "right"
                    }
                    " foot"
                }
            }
            @if !hide_follower {
                div {
                    img src="fa-solid-person-dress.svg" style="height: 1.5ex; margin-right: 1rem;";
                    @if *foot == Foot::Left {
                        "left"
                    } @else {
                        "right"
                    }
                    " foot"
                }
            }
            div {
                img src="fa-solid-weight-hanging.svg" style="height: 1ex; margin-right: 1rem;";
                (weight_transfer)
            }
        }
    }
}

/// Generate a valid HTML snippet detailing the given element. Only considers
/// [`RoutineElement::Figure`] and [`RoutineElement::Step`].
fn routine_element_details(element: &RoutineElement) -> Markup {
    match element {
        RoutineElement::Figure(
            figure @ Figure {
                title,
                start,
                end,
                notes,
                level_requirement,
                ..
            },
        ) => {
            let (leaders_steps, followers_steps) = figure.steps();
            let timing_summary =
                timing_summary_for_routine(vec![RoutineElement::Figure(figure.clone())]);

            let leader_step_count = leaders_steps.len();
            let follower_step_count = followers_steps.len();

            html! {
                div style="display: flex; justify-content: space-between;" {
                    span style="margin-right: 0.5rem" {
                        (title)
                    }
                    @if *level_requirement != LevelRequirement::ToBeDetermined {
                        div {
                            img src="fa-solid-layer-group.svg" style="height: 1ex; margin-right: 1rem;";
                            @match level_requirement {
                                LevelRequirement::ToBeDetermined => "tbd",
                                LevelRequirement::WdsfDCSyllabus => "D/C class",
                                LevelRequirement::WdsfBSyllabus => "B class",
                                LevelRequirement::Open => "open class",
                            }
                        }
                    }
                    @if !timing_summary.is_empty() {
                        div {
                            img src="fa-solid-clock.svg" style="height: 1ex; margin-right: 1rem;";
                            span {
                                (timing_summary)
                            }
                        }
                    }
                    @if let Some(s) = start.as_ref() {
                        div {
                            img src="fa-solid-circle-chevron-up.svg" style="height: 1ex; margin-right: 1rem;";
                            (s)
                        }
                    }
                    @if let Some(e) = end.as_ref() {
                        div {
                            img src="fa-solid-circle-chevron-down.svg" style="height: 1ex; margin-right: 1rem;";
                            (e)
                        }
                    }
                }
                @if leader_step_count + follower_step_count > 0 {
                    div style="display: flex; justify-content: space-between; margin-top: 0.5rem;" {
                        div style="padding-right:7.5pt; flex-grow: 1;" {
                            span style="font-size: smaller; color: var(--faded-color);" {
                                "Leader's steps"
                            }
                            @if leader_step_count > 0 {
                                ul style="margin-left: 0cm; padding-left: 1rem; margin-top: -0.2rem;" {
                                    @for step in leaders_steps {
                                        li style="margin-top: -1rem; margin-bottom: -0.8rem;" {
                                            (step_html(step, true, false))
                                        }
                                    }
                                }
                            } @else {
                                p {
                                    "No steps for the leader."
                                }
                            }
                        }
                        div style="padding-left:7.5pt; flex-grow: 1;" {
                            span style="font-size: smaller; color: var(--faded-color);" {
                                "Follower's steps"
                            }
                            @if follower_step_count > 0 {
                                ul style="margin-left: 0cm; padding-left: 1rem; margin-top: -0.2rem;" {
                                    @for step in followers_steps {
                                        li style="margin-top: -1rem; margin-bottom: -0.8rem;" {
                                            (step_html(step, true, false))
                                        }
                                    }
                                }
                            } @else {
                                p {
                                    "No steps for the follower."
                                }
                            }
                        }
                    }
                }
                @if !notes.is_empty() {
                    div style="margin-top: 0.5rem" {
                        span { "Notes:" }
                        ul {
                            @for note in notes {
                                li { (note) }
                            }
                        }
                    }
                }
            }
        },
        RoutineElement::Step(step) => step_html(step, false, false),
        _ => Markup::default(),
    }
}
