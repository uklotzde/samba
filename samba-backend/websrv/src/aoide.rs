use std::path::PathBuf;

use aoide::{
    backend_embedded::storage::{provision_database, DatabaseConfig, DatabaseSchemaMigrationMode},
    storage_sqlite::connection::{
        pool::{gatekeeper::Config as DatabaseConnectionGatekeeperConfig, Config as PoolConfig},
        Config as ConnectionConfig, Storage,
    },
    CollectionUid, PlaylistUid,
};
use axum::{
    body::Body,
    http::StatusCode,
    response::{IntoResponse, Response},
};
use thiserror::Error;

use super::{AOIDE_SQLITE_FILE_ENV_VAR, SAMBA_SQLITE_FILE_ENV_VAR};

mod collections;
mod playlists;
mod tracks;

pub use aoide::storage_sqlite::connection::pool::gatekeeper::Gatekeeper;

pub use self::{
    collections::{
        create_collection, delete_collection, get_collection_by_id,
        synchronize_collection_with_file_system, update_collection,
    },
    playlists::{
        create_playlist_with_entries, delete_playlist, get_all_playlists, get_playlist_by_id,
        update_playlist_with_entries,
    },
    tracks::{get_track_by_id, get_tracks_for_collection},
};

#[derive(Error, Debug)]
pub enum AoideError {
    #[error("Connection to the aoide database failed.")]
    DatabaseConnectionFailed,
    #[error("Entity `{0}` has not been found.")]
    EntityNotFound(String),
    #[error("Environment variable `{0}` could not be retrieved.")]
    EnvironmentInvariantInvalid(String),
    #[error("Patching entity `{0}` failed.")]
    PatchingFailed(String),
    #[error("Finding playlists for a collection failed.")]
    PlaylistListingFailed,
    #[error("File system synchronization for collection `{0}` failed.")]
    SynchronizationFailed(String),
    #[error("Finding the requested tracks failed.")]
    TrackSearchFailed,
    #[error("Type conversion between aoide types failed.")]
    TypeConversionFailed,
}

impl IntoResponse for AoideError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .body(body)
            .unwrap()
    }
}

/// Decode a collection UID from string as needed by most consumers (e.g.
/// accessing from library which stores collection UID as text).
pub fn decode_collection_uid(collection_id: &str) -> Result<CollectionUid, AoideError> {
    collection_id.parse::<CollectionUid>().map_err(|e| {
        tracing::debug!("Failed to decode entity uid from {collection_id}: {e}");
        AoideError::TypeConversionFailed
    })
}

/// Decode a playlist UID from string as needed by most consumers (e.g.
/// accessing from session list which stores collection UID as text).
pub fn decode_playlist_uid(playlist_id: &str) -> Result<PlaylistUid, AoideError> {
    playlist_id.parse::<PlaylistUid>().map_err(|e| {
        tracing::debug!("Failed to decode entity uid from {playlist_id}: {e}");
        AoideError::TypeConversionFailed
    })
}

#[tracing::instrument]
pub async fn provision_aoide() -> Result<Gatekeeper, AoideError> {
    let file_path = match dotenvy::var(AOIDE_SQLITE_FILE_ENV_VAR).map(|url| url.parse::<PathBuf>())
    {
        Ok(Ok(path)) => path,
        Ok(Err(e)) => {
            tracing::debug!("Converting environment variable to path failed: {}.", e);
            return Err(AoideError::EnvironmentInvariantInvalid(
                AOIDE_SQLITE_FILE_ENV_VAR.to_string(),
            ));
        },
        Err(e) => {
            tracing::warn!(
                "Did not find valid {} in environment; defaulting to aoide.sqlite as sibling to \
                 samba database",
                AOIDE_SQLITE_FILE_ENV_VAR
            );
            tracing::debug!("Reason for environment resolution failure: {}", e);
            let db_url = dotenvy::var(SAMBA_SQLITE_FILE_ENV_VAR)
                .map(|url| url.parse::<PathBuf>())
                .map_err(|e| {
                    tracing::error!(
                        "Did not find a valid {} either; exiting due to: {}",
                        SAMBA_SQLITE_FILE_ENV_VAR,
                        e
                    );
                    AoideError::EnvironmentInvariantInvalid(SAMBA_SQLITE_FILE_ENV_VAR.to_string())
                })?
                .map_err(|e| {
                    tracing::debug!("Converting environment variable to path failed: {}.", e);
                    AoideError::TypeConversionFailed
                })?;
            db_url
                .parent()
                .ok_or(AoideError::TypeConversionFailed)?
                .join("aoide.sqlite")
        },
    };

    let storage = Storage::File { path: file_path };
    let pool = PoolConfig {
        max_size: 8
            .try_into()
            .map_err(|_conversion_failure| AoideError::TypeConversionFailed)?,
        gatekeeper: DatabaseConnectionGatekeeperConfig {
            acquire_read_timeout_millis: 10_000
                .try_into()
                .map_err(|_conversion_failure| AoideError::TypeConversionFailed)?,
            acquire_write_timeout_millis: 30_000
                .try_into()
                .map_err(|_conversion_failure| AoideError::TypeConversionFailed)?,
        },
    };

    provision_database(&DatabaseConfig {
        connection: ConnectionConfig { storage, pool },
        migrate_schema: Some(DatabaseSchemaMigrationMode::ApplyPending),
    })
    .map_err(|e| {
        tracing::debug!("Could not commission aoide database: {}", e);
        AoideError::DatabaseConnectionFailed
    })
}
