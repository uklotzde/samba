use std::{
    sync::{atomic::AtomicBool, Arc},
    time::Duration,
};

use aoide::{
    api::{collection::LoadScope as CollectionLoadScope, media::SyncMode},
    backend_embedded::{
        batch::synchronize_collection_vfs::{
            synchronize_collection_vfs, OrphanedMediaSources, UnsynchronizedTracks, UntrackedFiles,
            UntrackedMediaSources,
        },
        collection::{
            create as create_aoide_collection, load_one as load_collection,
            purge as purge_collection, update as update_aoide_collection,
        },
    },
    media::content::ContentMetadata,
    music::{beat::TimeSignature, tempo::TempoBpm},
    prelude::*,
    tag::{FacetKey, Label, PlainTag, Score, TagsMap},
    track::{
        actor::{Actors, Role as ActorRole},
        tag::{FACET_ID_COMMENT, FACET_ID_GENRE, FACET_ID_LANGUAGE},
    },
    Collection, CollectionEntity, CollectionUid, Track,
};
use samba_shared::{
    dancing::{Dance, DANCE_PREFERENCE_DELTA, FACET_ID_DANCE},
    tracks::{
        metadata::{
            get_primary_dance, FACET_ID_MOVIE, FACET_ID_MPM, FACET_ID_REMIX,
            TRACK_LANGUAGE_INSTRUMENTAL,
        },
        parse_artist_candidate, parse_title_candidate, ExtractedMetadata,
    },
};

use super::{AoideError, Gatekeeper};

#[tracing::instrument(skip(gatekeeper))]
pub async fn get_collection_by_id(
    gatekeeper: &Gatekeeper,
    collection_id: CollectionUid,
) -> Result<CollectionEntity, AoideError> {
    load_collection(
        gatekeeper,
        collection_id.clone(),
        CollectionLoadScope::Entity,
    )
    .await
    .map(|entity| entity.entity)
    .map_err(|e| {
        tracing::debug!("Failed to load collection for {collection_id}: {e}");
        AoideError::EntityNotFound(collection_id.to_string())
    })
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn create_collection(
    gatekeeper: &Gatekeeper,
    collection: Collection,
) -> Result<CollectionEntity, AoideError> {
    create_aoide_collection(gatekeeper, collection)
        .await
        .map_err(|e| {
            tracing::debug!("Creating collection failed: {}", e);
            AoideError::DatabaseConnectionFailed
        })
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn update_collection(
    gatekeeper: &Gatekeeper,
    collection: CollectionEntity,
) -> Result<CollectionEntity, AoideError> {
    update_aoide_collection(gatekeeper, collection.raw.hdr, collection.raw.body)
        .await
        .map_err(|e| {
            tracing::debug!("Updating collection failed: {}", e);
            AoideError::DatabaseConnectionFailed
        })
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn delete_collection(
    gatekeeper: &Gatekeeper,
    collection_id: CollectionUid,
) -> Result<(), AoideError> {
    purge_collection(gatekeeper, collection_id.clone())
        .await
        .map_err(|e| {
            tracing::debug!("Removing collection {collection_id} failed: {e}");
            AoideError::EntityNotFound(collection_id.to_string())
        })
}

/// Synchronize a collection with the file system by scanning for
/// new/modified/deleted files. This is a long-running operation. Do not call it
/// outside of background worker threads.
#[tracing::instrument(skip(gatekeeper))]
pub async fn synchronize_collection_with_file_system(
    gatekeeper: &Gatekeeper,
    collection_uid: &CollectionUid,
    dances: Arc<Vec<Dance>>,
) -> Result<(), AoideError> {
    let params = aoide::backend_embedded::batch::synchronize_collection_vfs::Params {
        // Use the root URL of the collection
        root_url: None,
        // Unlimited depth
        max_depth: None,
        // TODO: make user-configurable?
        // Ideally, SyncMode::Modified would used here if all track metadata
        // is synchronized bidirectionally (import/export) with file tags.
        sync_mode: SyncMode::Once,
        // Import config
        import_track_config: Default::default(),
        // Keep media sources even if the corresponding file has disappeared.
        // The file might be temporarily unavailable because a volume has not
        // been mounted. Otherwise we would also purge the corresponding tracks
        // and their metadata irreversibly without user confirmation!
        // TODO: Purge if explicitly requested
        untracked_media_sources: UntrackedMediaSources::Keep,
        // Media sources without a track entity are useless and can safely be deleted.
        orphaned_media_sources: OrphanedMediaSources::Purge,
        // Only for reporting unresolved differences
        untracked_files: UntrackedFiles::Skip,
        // Only for reporting unresolved differences
        unsynchronized_tracks: UnsynchronizedTracks::Skip,
    };

    let abort_flag = Arc::new(AtomicBool::new(false));
    synchronize_collection_vfs(
        gatekeeper,
        collection_uid.clone(),
        params,
        // Post-process tracks and update metadata from title or artist.
        move |track: Track| process_track(&dances, track),
        // TODO: Report progress
        |_| {},
        abort_flag,
    )
    .await
    .map(|outcome| {
        tracing::debug!("Synchronizing collection {collection_uid} completed: {outcome:?}");
    })
    .map_err(|err| {
        tracing::debug!("Synchronizing collection {collection_uid} failed: {err}");
        AoideError::SynchronizationFailed(collection_uid.to_string())
    })?;

    Ok(())
}

fn process_track(dances: &[Dance], mut track: Track) -> Track {
    let mut tags: TagsMap<'_> = std::mem::take(&mut track.tags).untie().into();

    if let Some(title) = track.track_title().map(ToOwned::to_owned) {
        update_track_from_extracted_metadata(&mut track, &mut tags, parse_title_candidate(&title));
    }

    let ContentMetadata::Audio(audio_content) = &track.media_source.content.metadata;
    if let Some(duration_ms) = audio_content.duration {
        let duration = Duration::from_millis(duration_ms.value().round() as u64);
        if duration >= Duration::from_secs(90) && duration <= Duration::from_secs(120) {
            tags.insert(FacetKey::new(None), PlainTag {
                label: Some(Label::from_unchecked("competition-length")),
                score: Default::default(),
            });
        }
    }

    if tags.count(FACET_ID_DANCE) == 0 {
        tags.replace_faceted_plain_tags(
            FACET_ID_DANCE.to_borrowed(),
            tags.get_faceted_plain_tags(FACET_ID_GENRE)
                .map(|plain_tags| plain_tags.iter())
                .into_iter()
                .flatten()
                .filter_map(|plain_tag| plain_tag.label.as_ref())
                .filter_map(|label| {
                    dances.iter().find(|dance| {
                        dance.name == label.as_str() || dance.abbreviation == label.as_str()
                    })
                })
                .enumerate()
                .map(|(i, dance)| {
                    let label = Label::from_unchecked(dance.abbreviation.clone());
                    debug_assert!(label.is_valid());
                    let score =
                        Score::new_unchecked((1.0 - i as f64 * DANCE_PREFERENCE_DELTA).max(0.0));
                    debug_assert!(score.is_valid());
                    PlainTag {
                        label: Some(label),
                        score,
                    }
                })
                .collect::<Vec<_>>(),
        );
    }

    debug_assert!(track.tags.is_empty());
    track.tags = tags.canonicalize_into();

    // After finishing tag editing above, update non-tag properties of the track.
    if track.metrics.tempo_bpm.is_none() {
        if let Some(dance) = get_primary_dance(&track).and_then(|primary_dance| {
            dances
                .iter()
                .find(|dance| dance.name == primary_dance || dance.abbreviation == primary_dance)
        }) {
            // TODO: check whether this assumption is sensible or whether not
            //       setting anything here would be better.
            let average_mpm =
                dance.mpm_range.start + (dance.mpm_range.end - dance.mpm_range.start) / 2;
            track.metrics.tempo_bpm = Some(TempoBpm::new(average_mpm as f64 * 4.0));
            track.metrics.time_signature = Some(TimeSignature::new(
                4,
                Some(dance.number_of_crotchets.into()),
            ));
        }
    }

    if track.metrics.time_signature.is_none() {
        // All ballroom and related dances are danced on music with 4 beats
        // per measure.
        track.metrics.time_signature = Some(TimeSignature::new(4, None));
    }

    let summary_track_artist = Actors::main_actor(track.actors.iter(), ActorRole::Artist);
    if let Some(summary_track_artist) = summary_track_artist {
        let parsed_track_artists = parse_artist_candidate(&summary_track_artist.name);

        let mut track_actors = std::mem::take(&mut track.actors).untie();
        track_actors.extend(parsed_track_artists);
        track.actors = track_actors.canonicalize_into();
    }

    track
}

fn update_track_from_extracted_metadata(
    track: &mut Track,
    tags: &mut TagsMap<'_>,
    ExtractedMetadata {
        title,
        remix,
        movie,
        comment,
        dance_abbreviations,
        measures_per_minute,
        ..
    }: ExtractedMetadata,
) {
    if let Some(title) = title {
        track.set_track_title(title);
    }

    if !dance_abbreviations.is_empty() {
        // In the light of having easier search in other applications as
        // well, the dances facet will be populated with the dance
        // abbreviations instead of full dance names.
        tags.replace_faceted_plain_tags(
            FACET_ID_DANCE.to_borrowed(),
            dance_abbreviations
                .into_iter()
                .filter_map(Label::clamp_from)
                .enumerate()
                .map(|(i, label)| {
                    let score =
                        Score::new_unchecked((1.0 - i as f64 * DANCE_PREFERENCE_DELTA).max(0.0));
                    debug_assert!(score.is_valid());
                    PlainTag {
                        label: Some(label),
                        score,
                    }
                })
                .collect::<Vec<_>>(),
        );
    };

    if let Some(label) = movie.and_then(Label::clamp_from) {
        tags.insert(FACET_ID_MOVIE, PlainTag {
            label: Some(label),
            score: Default::default(),
        });
    }

    if let Some(label) = remix.and_then(Label::clamp_from) {
        tags.insert(FACET_ID_REMIX, PlainTag {
            label: Some(label),
            score: Default::default(),
        });
    }

    // TODO: proper language detection using common shorthands e.g. inserted by Casa
    // Musica
    if let Some(comment) = comment
        .as_ref()
        .filter(|c| c.contains(TRACK_LANGUAGE_INSTRUMENTAL))
    {
        tags.insert(FACET_ID_LANGUAGE, PlainTag {
            label: Label::clamp_from("instrumental"),
            score: Default::default(),
        });
        let remaining_comment = Label::clamp_from(
            comment
                .replace(&format!("{TRACK_LANGUAGE_INSTRUMENTAL}, "), "")
                .replace(&format!(", {TRACK_LANGUAGE_INSTRUMENTAL}"), "")
                .replace(TRACK_LANGUAGE_INSTRUMENTAL, ""),
        );
        tags.insert(FACET_ID_COMMENT, PlainTag {
            label: remaining_comment,
            score: Default::default(),
        });
    } else if let Some(label) = comment.and_then(Label::clamp_from) {
        tags.insert(FACET_ID_COMMENT, PlainTag {
            label: Some(label),
            score: Default::default(),
        });
    }

    if let Some(mpm) = measures_per_minute {
        let score = Score::new_unchecked(mpm.get() as f64 / 100.0);
        debug_assert!(score.is_valid());
        tags.insert(FACET_ID_MPM, PlainTag {
            // No label, only the score that represents the value
            label: None,
            // No dance track has more than 100 MPM, so we use this as the
            // upper bound. It enables the use of a percentage based view
            // on the MPM.
            score,
        });

        if track.metrics.tempo_bpm.is_none() {
            track.metrics.tempo_bpm = Some(TempoBpm::new(mpm.get() as f64 * 4.0));
        }
    }
}
