use aoide::{
    api::{
        collection::LoadScope as CollectionLoadScope,
        sorting::SortDirection,
        track::search::{Params as SearchParams, SortField, SortOrder},
        Pagination,
    },
    backend_embedded::{
        collection::load_all as load_all_collections,
        track::{load_one as load_track, search as search_tracks},
    },
    repo::collection::KindFilter,
    CollectionUid, TrackEntity, TrackUid,
};

use super::{AoideError, Gatekeeper};
use crate::SAMBA_AOIDE_COLLECTION_KIND;

/// Fetch a track entity from aoide only by considering its ID. This is as
/// fast as a single query if the content URL is not required, but it is as
/// expensive as a search for the UID through all collections if the content
/// URL has to be resolved.
#[tracing::instrument(skip(gatekeeper))]
pub async fn get_track_by_id(
    gatekeeper: &Gatekeeper,
    track_id: &str,
    resolve_content_url: bool,
) -> Result<TrackEntity, AoideError> {
    let uid = track_id.parse::<TrackUid>().map_err(|e| {
        tracing::debug!("Failed to decode entity uid from {}: {}", track_id, e);
        AoideError::TypeConversionFailed
    })?;
    if resolve_content_url {
        let params = SearchParams {
            filter: Some(aoide::api::track::search::Filter::AnyTrackUid(vec![uid])),
            ordering: vec![],
            resolve_url_from_content_path: Some(Default::default()),
        };

        search_tracks_with_params(gatekeeper, params)
            .await?
            .first()
            .cloned()
            .ok_or_else(|| {
                tracing::debug!("Failed to load track for {} from any collection.", track_id);
                AoideError::EntityNotFound(track_id.to_string())
            })
    } else {
        load_track(gatekeeper, uid).await.map_err(|e| {
            tracing::debug!("Failed to load track for {}: {}", track_id, e);
            AoideError::EntityNotFound(track_id.to_string())
        })
    }
}

async fn search_tracks_with_params(
    gatekeeper: &Gatekeeper,
    params: SearchParams,
) -> Result<Vec<TrackEntity>, AoideError> {
    let collection_kind_filter = KindFilter::Equal(SAMBA_AOIDE_COLLECTION_KIND.into());
    let collections = load_all_collections(
        gatekeeper,
        Some(collection_kind_filter),
        None,
        CollectionLoadScope::Entity,
        None,
    )
    .await
    .map_err(|e| {
        tracing::debug!("Loading collections from aoide failed: {}", e);
        AoideError::DatabaseConnectionFailed
    })?;

    let mut track_result: Vec<TrackEntity> = vec![];
    for collection in collections {
        // Explicitly define an offset with no limit to prevent using
        // the default limit if no pagination is given!
        let no_pagination = Pagination {
            offset: Some(0),
            limit: None, // unlimited
        };

        track_result.extend(
            search_tracks(
                gatekeeper,
                collection.entity.hdr.uid.clone(),
                params.clone(),
                no_pagination,
            )
            .await
            .map_err(|e| {
                tracing::debug!(
                    "Track search in {} failed: {}",
                    collection.entity.hdr.uid,
                    e
                );
                AoideError::TrackSearchFailed
            })?,
        );
    }

    Ok(track_result)
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn get_tracks_for_collection(
    gatekeeper: &Gatekeeper,
    collection_id: CollectionUid,
) -> Result<Vec<TrackEntity>, AoideError> {
    // Explicitly define an offset with no limit to prevent using
    // the default limit if no pagination is given!
    let no_pagination = Pagination {
        offset: Some(0),
        limit: None, // unlimited
    };

    let params = SearchParams {
        filter: None,
        ordering: vec![SortOrder {
            field: SortField::ContentPath,
            direction: SortDirection::Ascending,
        }],
        // TODO: configurable?
        resolve_url_from_content_path: Some(Default::default()),
    };

    search_tracks(gatekeeper, collection_id.clone(), params, no_pagination)
        .await
        .map_err(|e| {
            tracing::debug!("Track search in {collection_id} failed: {e}");
            AoideError::TrackSearchFailed
        })
}
