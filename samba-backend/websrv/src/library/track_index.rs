use std::sync::Arc;

use aoide::{
    backend_embedded::batch::reindex_tracks::{reindex_tracks, IndexingMode},
    CollectionUid,
};
use color_eyre::eyre::eyre;
use samba_backend_database::{
    queries::{libraries::get_all as get_all_libraries, QueryError},
    Gatekeeper as DbGatekeeper,
};
use tantivy::TantivyError;
use thiserror::Error;

use crate::{
    aoide::Gatekeeper as AoideGatekeeper,
    server::data::{TantivyCache, TANTIVY_INDEX_WRITER_CAPACITY},
};

#[derive(Error, Debug)]
pub enum TrackIndexError {
    #[error("Querying the database failed: {0}")]
    AoideProcessing(color_eyre::eyre::Error),
    #[error("Querying the database failed: {0}")]
    DatabaseQueryError(#[from] QueryError),
    #[error("Invalid collection ID in database.")]
    InvalidCollectionId,
    #[error("Interacting with tantivy failed: {0}")]
    TantivyError(#[from] TantivyError),
}

#[tracing::instrument(skip(aoide_gatekeeper, tantivy_cache))]
pub async fn update_index_for_collection(
    aoide_gatekeeper: Arc<AoideGatekeeper>,
    tantivy_cache: Arc<TantivyCache>,
    collection_uid: CollectionUid,
) -> Result<usize, TrackIndexError> {
    let index_writer = tantivy_cache
        .index
        .index
        .writer(TANTIVY_INDEX_WRITER_CAPACITY)?;

    reindex_tracks(
        &aoide_gatekeeper,
        collection_uid,
        tantivy_cache.index.fields.clone(),
        1000.try_into().unwrap(),
        Some(IndexingMode::All),
        index_writer,
        |_| {},
    )
    .await
    .map_err(|e| TrackIndexError::AoideProcessing(eyre!(e.to_string())))
    .map(|count| count as usize)
}

#[tracing::instrument(skip(aoide_gatekeeper, database_gatekeeper, tantivy_cache))]
pub async fn update(
    aoide_gatekeeper: Arc<AoideGatekeeper>,
    database_gatekeeper: Arc<DbGatekeeper>,
    tantivy_cache: Arc<TantivyCache>,
) -> Result<(), TrackIndexError> {
    for collection in get_all_libraries(&database_gatekeeper)
        .await?
        .into_iter()
        .map(|lib| lib.aoide_collection)
    {
        let updated_tracks = update_index_for_collection(
            Arc::clone(&aoide_gatekeeper),
            Arc::clone(&tantivy_cache),
            collection
                .parse()
                .map_err(|_invalid_id| TrackIndexError::InvalidCollectionId)?,
        )
        .await
        .map_err(|e| TrackIndexError::AoideProcessing(eyre!(e.to_string())))?;
        tracing::debug!("Updated {updated_tracks} tracks in index for collection {collection}.");
    }

    Ok(())
}
