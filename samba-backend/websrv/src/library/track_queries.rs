use aoide::track::Entity as TrackEntity;
use rand::Rng;
use tantivy::{
    collector::TopDocs,
    query::{AllQuery, Query as TantivyQuery, QueryParserError},
    TantivyError,
};
use thiserror::Error;

use crate::{
    aoide::{get_track_by_id, Gatekeeper as AoideGatekeeper},
    server::data::TantivyCache,
};

#[derive(Debug, Error)]
pub enum TrackQueryError {
    #[error("Invalid track query: {0}")]
    InvalidTrackQuery(#[from] QueryParserError),
    #[error("Interaction with the file system failed: {0}")]
    IO(#[from] std::io::Error),
    #[error("Error while querying track index: {0}")]
    TantivyError(#[from] TantivyError),
    #[error("Fetching specific track {0} failed.")]
    TrackIdentification(String),
}

#[derive(Debug, Default)]
pub struct TrackQueryParameters {
    pub offset: usize,
    pub limit: Option<usize>,
    pub randomize: bool,
}

#[tracing::instrument(skip(tantivy_cache))]
pub async fn count_tracks_matching_query(
    tantivy_cache: &TantivyCache,
    query: &str,
) -> Result<usize, TrackQueryError> {
    let query: Box<dyn TantivyQuery> = if !query.is_empty() {
        Box::new(tantivy_cache.parser.parse_query(query)?)
    } else {
        Box::new(AllQuery)
    };
    let reader = tantivy_cache.index.index.reader()?;
    let searcher = reader.searcher();

    query.count(&searcher).map_err(Into::into)
}

#[tracing::instrument(skip(aoide_gatekeeper, tantivy_cache))]
pub async fn get_tracks_matching_query(
    aoide_gatekeeper: &AoideGatekeeper,
    tantivy_cache: &TantivyCache,
    query: &str,
    parameters: TrackQueryParameters,
) -> Result<Vec<TrackEntity>, TrackQueryError> {
    let query: Box<dyn TantivyQuery> = if !query.is_empty() {
        Box::new(tantivy_cache.parser.parse_query(query)?)
    } else {
        Box::new(AllQuery)
    };
    let reader = tantivy_cache.index.index.reader()?;
    let searcher = reader.searcher();

    let limit = if let Some(limit) = parameters.limit {
        limit
    } else {
        query.count(&searcher)?
    };
    let top_docs = if parameters.randomize {
        let collector = TopDocs::with_limit(limit)
            .and_offset(parameters.offset)
            .custom_score(move |_: &_| move |_| rand::thread_rng().gen::<f32>());
        searcher.search(&query, &collector)?
    } else {
        let collector = TopDocs::with_limit(limit).and_offset(parameters.offset);
        searcher.search(&query, &collector)?
    };
    tracing::debug!("Identified {} docs for query {query:?}", top_docs.len());

    let mut track_result: Vec<TrackEntity> = Vec::with_capacity(top_docs.len());
    for (_, doc_addr) in &top_docs {
        if let Ok(Some(track_uid)) = searcher
            .doc(*doc_addr)
            .map(|doc| tantivy_cache.index.fields.read_uid(&doc))
        {
            let track_uid_string = track_uid.to_string();
            track_result.push(
                get_track_by_id(aoide_gatekeeper, &track_uid_string, false)
                    .await
                    .map_err(|e| {
                        tracing::error!(
                            "Failed to reach aoide to resolve {track_uid_string}: {}",
                            e
                        );
                        TrackQueryError::TrackIdentification(track_uid_string)
                    })?,
            );
        } else {
            tracing::debug!("Failed to resolve doc address {doc_addr:?} in track index.");
        }
    }

    Ok(track_result)
}
