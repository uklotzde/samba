use axum::{
    body::Body,
    extract::State,
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use samba_backend_database::{
    queries::{dances::get_all as get_all_dances, QueryError},
    GatekeeperError,
};
use samba_shared::dancing::Dance as SharedDance;
use thiserror::Error;

use crate::SharedData;

#[derive(Error, Debug)]
pub enum DanceError {
    #[error("Connection to the database failed: {0}")]
    DatabaseConnection(#[from] GatekeeperError),
    #[error("Querying the database failed: {0}")]
    DatabaseQuery(#[from] QueryError),
}

impl IntoResponse for DanceError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(match self {
                DanceError::DatabaseConnection(_) => StatusCode::SERVICE_UNAVAILABLE,
                DanceError::DatabaseQuery(QueryError::RowIdentification(_)) => {
                    StatusCode::NOT_FOUND
                },
                DanceError::DatabaseQuery(_) => StatusCode::INTERNAL_SERVER_ERROR,
            })
            .body(body)
            .unwrap()
    }
}

#[tracing::instrument]
pub async fn index(State(state): State<SharedData>) -> Result<Json<Vec<SharedDance>>, DanceError> {
    get_all_dances(&state.database_gatekeeper)
        .await
        .map(Json)
        .map_err(Into::into)
}
