use axum::{
    body::Body,
    extract::{Path, State},
    http::{header, StatusCode},
    response::{IntoResponse, Response},
    Json,
};
use axum_extra::headers::HeaderValue;
use samba_backend_database::{
    queries::{
        events::{
            create as create_event, delete as delete_event, get_all as get_all_events,
            get_by_id as get_event_by_id, update as update_event,
        },
        QueryError,
    },
    GatekeeperError,
};
use samba_backend_reports::generate_event_report_at;
use samba_shared::events::Event;
use tempfile::NamedTempFile;
use thiserror::Error;
use tokio_util::io::ReaderStream;

use crate::SharedData;

#[derive(Error, Debug)]
pub enum EventError {
    #[error("Connection to the database failed: {0}")]
    DatabaseConnection(#[from] GatekeeperError),
    #[error("Querying the database failed: {0}")]
    DatabaseQuery(#[from] QueryError),
    #[error("Failed to commit I/O transaction.")]
    IOError,
    #[error("Failed to create a report for event {0}")]
    ReportGeneration(i32),
}

impl IntoResponse for EventError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(match self {
                EventError::DatabaseConnection(_) => StatusCode::SERVICE_UNAVAILABLE,
                EventError::DatabaseQuery(QueryError::RowIdentification(_)) => {
                    StatusCode::NOT_FOUND
                },
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            })
            .body(body)
            .unwrap()
    }
}

#[tracing::instrument]
pub async fn index(State(state): State<SharedData>) -> Result<impl IntoResponse, EventError> {
    get_all_events(&state.database_gatekeeper)
        .await
        .map(Json)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn create(
    State(state): State<SharedData>,
    Json(new): Json<Event>,
) -> Result<impl IntoResponse, EventError> {
    create_event(&state.database_gatekeeper, new)
        .await
        .map(|event_id| (StatusCode::CREATED, Json(event_id)))
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn get_by_id(
    State(state): State<SharedData>,
    Path(event_id): Path<i32>,
) -> Result<impl IntoResponse, EventError> {
    get_event_by_id(&state.database_gatekeeper, event_id)
        .await
        .map(Json)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn update(
    State(state): State<SharedData>,
    Path(event_id): Path<i32>,
    Json(updated): Json<Event>,
) -> Result<impl IntoResponse, EventError> {
    update_event(&state.database_gatekeeper, event_id, updated)
        .await
        .map(|_ok| StatusCode::ACCEPTED)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn delete(
    State(state): State<SharedData>,
    Path(event_id): Path<i32>,
) -> Result<impl IntoResponse, EventError> {
    delete_event(&state.database_gatekeeper, event_id)
        .await
        .map(|_ok| StatusCode::OK)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn build_report(
    State(state): State<SharedData>,
    Path(event_id): Path<i32>,
) -> Result<impl IntoResponse, EventError> {
    let event = get_event_by_id(&state.database_gatekeeper, event_id).await?;

    let pdf_file = NamedTempFile::new().map_err(|e| {
        tracing::debug!("failed to create new temp file: {}", e);
        EventError::IOError
    })?;
    generate_event_report_at(&event.value, pdf_file.path())
        .await
        .map_err(|e| {
            tracing::debug!("failed to generate report for {}: {}", event_id, e);
            EventError::ReportGeneration(event_id)
        })?;

    let tokio_file = tokio::fs::File::open(pdf_file.path()).await.map_err(|e| {
        tracing::debug!("failed to open PDF report: {}", e);
        EventError::IOError
    })?;
    let stream = ReaderStream::new(tokio_file);
    let body = Body::from_stream(stream);

    Ok((
        StatusCode::OK,
        [
            (
                header::CONTENT_TYPE,
                HeaderValue::from_static("application/pdf"),
            ),
            (
                header::CONTENT_DISPOSITION,
                format!("attachment; filename=\"{}.pdf\"", event.value.title)
                    .try_into()
                    .unwrap_or_else(|_| {
                        HeaderValue::from_static("attachment; filename=\"event.pdf\"")
                    }),
            ),
        ],
        body,
    ))
}
