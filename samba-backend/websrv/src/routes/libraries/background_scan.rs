use std::sync::Arc;

use aoide::CollectionUid;
use samba_backend_database::Gatekeeper as DbGatekeeper;

use super::LibraryError;
use crate::{
    aoide::Gatekeeper as AoideGatekeeper,
    library::{
        track_cache::update_collection_track_cache, track_index::update_index_for_collection,
    },
    server::{background_tasks::BackgroundTask, data::TantivyCache},
};

pub async fn create_postupdate_task(
    aoide_gatekeeper: Arc<AoideGatekeeper>,
    database_gatekeeper: Arc<DbGatekeeper>,
    tantivy_cache: Arc<TantivyCache>,
    collection_id: CollectionUid,
) -> Result<BackgroundTask, LibraryError> {
    Ok(BackgroundTask::new(tokio::task::spawn(async move {
        update_index_for_collection(
            Arc::clone(&aoide_gatekeeper),
            Arc::clone(&tantivy_cache),
            collection_id.clone(),
        )
        .await?;
        update_collection_track_cache(aoide_gatekeeper, database_gatekeeper, collection_id).await?;

        Ok(None)
    })))
}
