use std::sync::Arc;

use axum::{
    body::Body,
    extract::{Extension, State},
    http::{header, StatusCode},
    response::{IntoResponse, Response},
    Json,
};
use axum_extra::headers::HeaderValue;
use jsonwebtoken::Header as JwtHeader;
use samba_backend_database::queries::{
    members::{
        get_by_id as get_user_by_id, get_by_name as get_user_by_name, update as update_user,
    },
    QueryError,
};
use samba_shared::SambaBackendVersion;
use secrecy::{ExposeSecret, SecretString};
use serde::Deserialize;
use thiserror::Error;

use crate::server::data::{Claims, SharedData};

#[derive(Error, Debug)]
pub enum ManagementError {
    #[error("Failed to create authorized token on login.")]
    AuthorizationError(#[from] jsonwebtoken::errors::Error),
    #[error("Querying the database failed: {0}")]
    DatabaseQuery(#[from] QueryError),
    #[error("Invalid username or password.")]
    InvalidUsernameOrPassword,
    #[error("Invalid JSON syntax in body of preference store request.")]
    InvalidPreferencesJson,
    #[error("Shutting down the service failed.")]
    ShutdownFailed,
}

impl IntoResponse for ManagementError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(match self {
                ManagementError::InvalidUsernameOrPassword
                | ManagementError::InvalidPreferencesJson => StatusCode::BAD_REQUEST,
                ManagementError::DatabaseQuery(QueryError::RowIdentification(_)) => {
                    StatusCode::NOT_FOUND
                },
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            })
            .body(body)
            .unwrap()
    }
}

/// Return version information.
#[tracing::instrument]
pub async fn version() -> Json<SambaBackendVersion> {
    Json(SambaBackendVersion {
        version: env!("CARGO_PKG_VERSION").to_string(),
    })
}

#[derive(Debug, Clone, Deserialize)]
pub struct LoginRequest {
    pub user_id: String,
    pub password: SecretString,
}

/// Try to authenticate a user.
#[tracing::instrument]
pub async fn login(
    State(state): State<SharedData>,
    Json(login_request): Json<LoginRequest>,
) -> Result<impl IntoResponse, ManagementError> {
    if login_request.user_id.is_empty() || login_request.password.expose_secret().is_empty() {
        return Err(ManagementError::InvalidUsernameOrPassword);
    }

    let db_user = get_user_by_name(&state.database_gatekeeper, &login_request.user_id).await?;
    if !db_user.password_matches(&login_request.password) {
        return Err(ManagementError::InvalidUsernameOrPassword);
    }

    Ok((
        StatusCode::OK,
        jsonwebtoken::encode(
            &JwtHeader::default(),
            &Claims::new_for_user(db_user.id),
            &state.jwt_session_keys.encoding,
        )?,
    )
        .into_response())
}

/// Get the user's preferences as JSON from the database and return it plain
/// if existent at all. Return an empty JSON object if the user has no
/// preferences so that he gets the defaults.
#[tracing::instrument]
pub async fn preferences(
    State(state): State<SharedData>,
    Extension(claims): Extension<Claims>,
) -> Result<impl IntoResponse, ManagementError> {
    let db_user = get_user_by_id(&state.database_gatekeeper, claims.user).await?;

    Ok((
        StatusCode::OK,
        [(
            header::CONTENT_TYPE,
            HeaderValue::from_static("application/json"),
        )],
        db_user.preferences.unwrap_or("{}".into()),
    ))
}

/// Save the user's preferences JSON into the database.
#[tracing::instrument]
pub async fn store_preferences(
    State(state): State<SharedData>,
    Extension(claims): Extension<Claims>,
    body: String,
) -> Result<impl IntoResponse, ManagementError> {
    if let Err(e) = serde_json::from_str::<serde::de::IgnoredAny>(&body) {
        tracing::error!("Invalid JSON passed, error: {e}");
        return Err(ManagementError::InvalidPreferencesJson);
    }

    let mut db_user = get_user_by_id(&state.database_gatekeeper, claims.user).await?;
    db_user.preferences = Some(body);

    update_user(&state.database_gatekeeper, db_user)
        .await
        .map(|_| StatusCode::OK)
        .map_err(Into::into)
}

/// Destroy a specific session of a user.
#[tracing::instrument]
pub async fn logout() -> Result<impl IntoResponse, ManagementError> {
    // TODO: destroy session

    Ok(())
}

/// Shut down the server.
#[tracing::instrument]
pub async fn shutdown(
    State(state): State<SharedData>,
) -> Result<impl IntoResponse, ManagementError> {
    Arc::try_unwrap(state.shutdown_channel)
        .map(|channel| {
            let _ = channel.send(());
            StatusCode::OK
        })
        .map_err(|_sender| {
            tracing::error!("Shutting down backend failed.");
            ManagementError::ShutdownFailed
        })
}
