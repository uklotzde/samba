use std::{io::Cursor, path::Path as FsPath};

use aoide::{
    media::artwork::{thumbnail_image, Artwork, EmbeddedArtwork, LinkedArtwork},
    media_file::io::import::{load_artwork_image_data, LoadedArtworkImageData},
    util::color::Color,
};
use axum::{
    body::{Body, Bytes},
    http::header,
    response::IntoResponse,
};
use axum_extra::headers::HeaderValue;
use image::{ImageBuffer, Rgb};
use ring::digest::{Context as HashContext, SHA256};

use super::TrackError;

#[tracing::instrument(level = "debug")]
pub async fn convert_color_to_artwork(color: &Color) -> Result<impl IntoResponse, TrackError> {
    let pixel_color = match color {
        Color::Rgb(color) => {
            let code = color.code();
            let red: u8 = ((code >> 16) & 0xFF) as u8;
            let green: u8 = ((code >> 8) & 0xFF) as u8;
            let blue: u8 = (code & 0xFF) as u8;
            Rgb([red, green, blue])
        },
        // TODO: use index of some color palette
        Color::Index(_) => Rgb([0, 0, 0]),
    };

    let image = ImageBuffer::<Rgb<u8>, _>::from_fn(128, 128, |_, _| pixel_color);
    let mut bytes: Vec<u8> = Vec::new();
    image
        .write_to(&mut Cursor::new(&mut bytes), image::ImageFormat::Png)
        .map_err(TrackError::ArtworkError)?;

    get_file_from_bytes(&bytes, "image/png").await
}

#[tracing::instrument(level = "debug")]
pub async fn extract_artwork(
    track_path: &FsPath,
    artwork: &Artwork,
) -> Result<impl IntoResponse, TrackError> {
    // Each branch tries to extract the actual image but ignores errors on
    // purpose to fall through to getting a fallback image.

    let loaded = tokio::task::block_in_place(|| load_artwork_image_data(track_path, artwork))
        .inspect_err(|err| tracing::warn!("Failed to load artwork image data: {err}"))
        .ok()
        .flatten();
    if let Some(LoadedArtworkImageData {
        image_data,
        media_type,
        ..
    }) = loaded
    {
        if let Ok(return_file) = get_file_from_bytes(&image_data, media_type.essence_str()).await {
            return Ok(return_file.into_response());
        }
    }

    tracing::debug!("Failed to load artwork from file, switching to fallback.");
    if let Ok(return_value) = extract_fallback_artwork(artwork).await {
        return Ok(return_value.into_response());
    }

    Err(TrackError::TypeConversion)
}

#[tracing::instrument(level = "debug")]
async fn extract_fallback_artwork(artwork: &Artwork) -> Result<impl IntoResponse, TrackError> {
    let artwork_image = match artwork {
        Artwork::Embedded(EmbeddedArtwork { image })
        | Artwork::Linked(LinkedArtwork { image, .. }) => image,
        // ignore other results so that a color will be
        // chosen if available
        _ => {
            return Err(TrackError::TypeConversion);
        },
    };

    let Some(thumbnail) = artwork_image.thumbnail else {
        return Err(TrackError::TypeConversion);
    };

    let image = image::imageops::resize(
        &thumbnail_image(&thumbnail),
        128,
        128,
        image::imageops::FilterType::Triangle,
    );

    let mut bytes: Vec<u8> = Vec::new();
    image
        .write_to(&mut Cursor::new(&mut bytes), image::ImageFormat::Png)
        .map_err(TrackError::ArtworkError)?;

    get_file_from_bytes(&bytes, "image/png").await
}

async fn get_file_from_bytes(
    bytes: &[u8],
    mime_type: &str,
) -> Result<impl IntoResponse, TrackError> {
    Ok((
        [
            // Album artwork is considered non-sensitive, therefore allowing
            // public caching does seem to make sense. Authentication might
            // interfere with that intention anyway but for non-authenticated
            // use this will help.
            (
                header::CACHE_CONTROL,
                HeaderValue::from_str("public")
                    .map_err(|_invalid_header| TrackError::TypeConversion)?,
            ),
            (
                header::ETAG,
                HeaderValue::from_str(&etag_for(bytes))
                    .map_err(|_invalid_header| TrackError::TypeConversion)?,
            ),
            (
                header::CONTENT_DISPOSITION,
                HeaderValue::from_str("inline; filename=\"album image\"")
                    .map_err(|_invalid_header| TrackError::TypeConversion)?,
            ),
            (
                header::CONTENT_TYPE,
                HeaderValue::from_str(mime_type)
                    .map_err(|_invalid_header| TrackError::TypeConversion)?,
            ),
        ],
        Body::from(Bytes::copy_from_slice(bytes)),
    ))
}

/// Generate an [etag][mdn-etag] (entity tag) for a given byte array. This can
/// be used as identification for the content and can be a simple hash.
///
/// [mdn-etag]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag
fn etag_for(bytes: &[u8]) -> String {
    let mut context = HashContext::new(&SHA256);
    context.update(bytes);

    format!("{:x?}", context.finish().as_ref())
}
