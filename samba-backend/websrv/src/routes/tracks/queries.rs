use lazy_regex::regex_replace_all;
use samba_backend_database::{
    queries::dances::get_all as get_all_dances, Gatekeeper as DatabaseGatekeeper,
};
use samba_shared::{dancing::FACET_ID_DANCE, libraries::Library};

use super::TrackError;

pub fn surround_query_with_collections_ids(libraries: &[Library], query: &str) -> String {
    if libraries.is_empty() {
        // If there are no libraries, we must ensure that the user does not get
        // to see results from libraries that do not belong to him. This
        // collection UID is guaranteed to be invalid, so we trust tantivy not
        // to find anything for this query.
        "collection_uid:INVALID".into()
    } else {
        let library_query = libraries
            .iter()
            .map(|lib| format!("collection_uid:{}", lib.aoide_collection))
            .intersperse(" OR ".to_string())
            .collect::<String>();
        if !query.is_empty() {
            format!("({library_query}) AND ({query})")
        } else {
            // The above syntax would be a tantivy syntax error with empty parentheses, so
            // we just omit the conditional and only restrict to libraries the user has
            // access to. The calling site already guards against this but in the sense of
            // defensive programming, it does make sense to repeat this cheap check.
            library_query
        }
    }
}

pub async fn preprocess_track_query(
    database_gatekeeper: &DatabaseGatekeeper,
    libraries: &[Library],
    query: &str,
) -> Result<String, TrackError> {
    // TODO: process MPM dependent on dance (if there are dances with other
    //       than 4 beats per bar)
    let query = regex_replace_all!(
        r"mpm:([\[\{])(\d+)\s+TO\s+(\d+)([\]\}])",
        query,
        |_, open: &str, mpm_from_string: &str, mpm_to_string: &str, close: &str| {
            // No track should have 0 BPM, so it's a safe fallback that should not
            // change any weights in the top-k collection.
            let mpm_from = mpm_from_string.parse::<usize>().unwrap_or_default() * 4;
            let mpm_to = mpm_to_string.parse::<usize>().unwrap_or_default() * 4;
            format!("tempo_bpm:{open}{mpm_from} TO {mpm_to}{close}")
        }
    );
    let query = regex_replace_all!(r"mpm:(\d+)", &query, |_, mpm_string: &str| {
        let mpm = mpm_string.parse::<usize>().unwrap_or_default() * 4;
        format!("tempo_bpm:{mpm}")
    });

    let dances = get_all_dances(database_gatekeeper).await?;
    let dance_query_processor = |dance_query: &str| {
        let dance_abbreviation = dances
            .iter()
            .find(|dance| {
                dance.abbreviation.to_lowercase() == dance_query.to_lowercase()
                    || dance.name.to_lowercase() == dance_query.to_lowercase()
            })
            .map_or_else(
                || dance_query.to_string(),
                |dance| dance.abbreviation.clone(),
            );
        format!("tag:\"{FACET_ID_DANCE}#{dance_abbreviation}\"")
    };
    let query = regex_replace_all!(r#"dance:"([^"]+)""#, &query, |_, dance_query| {
        dance_query_processor(dance_query)
    });
    let query = regex_replace_all!(r"dance:(\w+)", &query, |_, dance_query| {
        dance_query_processor(dance_query)
    });

    let library_query_processor = |library_query: &str| {
        format!(
            "collection_uid:{}",
            libraries
                .iter()
                .find(|library| library.id.to_lowercase() == library_query.to_lowercase())
                .map_or_else(
                    || library_query.to_string(),
                    |library| library.aoide_collection.clone()
                )
        )
    };
    let query = regex_replace_all!(r#"lib:"([^"]+)""#, &query, |_, library_query: &str| {
        library_query_processor(library_query)
    });
    let query = regex_replace_all!(r"lib:(\w+)", &query, |_, library_query: &str| {
        library_query_processor(library_query)
    });

    Ok(query.to_string())
}
