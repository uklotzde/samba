use std::{collections::HashMap, sync::Arc};

use aoide::search_index_tantivy::TrackIndex;
use jsonwebtoken::{DecodingKey, EncodingKey};
use samba_backend_database::Gatekeeper as DbGatekeeper;
use samba_shared::BackgroundTaskStatus;
use serde::{Deserialize, Serialize};
use tantivy::query::QueryParser;
use tokio::sync::{oneshot::Sender as OneshotSender, RwLock};
use ulid::Ulid;

use super::background_tasks::BackgroundTaskSender;
use crate::aoide::Gatekeeper as AoideGatekeeper;

pub struct Keys {
    pub encoding: EncodingKey,
    pub decoding: DecodingKey,
}

impl Keys {
    #[must_use]
    pub fn new(secret: &[u8]) -> Self {
        Self {
            encoding: EncodingKey::from_secret(secret),
            decoding: DecodingKey::from_secret(secret),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Claims {
    pub user: i32,
    /// Expiry date as unix timestamp (seconds since epoch). Required by
    /// `jsonwebtoken` verification.
    pub exp: u64,
}

impl Claims {
    /// Create a default claim for a user, valid for 8 hours from now.
    pub fn new_for_user(user: i32) -> Self {
        Self {
            user,
            exp: (std::time::SystemTime::now()
                .duration_since(std::time::UNIX_EPOCH)
                .expect("Time does not go backward")
                + std::time::Duration::from_secs(60 * 60 * 8))
            .as_secs(),
        }
    }
}

/// Number of bytes used for tantivy writer memory arenas.
pub const TANTIVY_INDEX_WRITER_CAPACITY: usize = 50_000_000;

pub struct TantivyCache {
    pub index: TrackIndex,
    pub parser: QueryParser,
}

/// The shared state of the server.
#[derive(Clone)]
pub struct SharedData {
    pub aoide_gatekeeper: Arc<AoideGatekeeper>,
    pub database_gatekeeper: Arc<DbGatekeeper>,
    pub tantivy_cache: Arc<TantivyCache>,
    pub background_channel: Arc<BackgroundTaskSender>,
    pub background_tasks: Arc<RwLock<HashMap<Ulid, BackgroundTaskStatus>>>,
    pub shutdown_channel: Arc<OneshotSender<()>>,
    pub jwt_session_keys: Arc<Keys>,
}

impl std::fmt::Debug for SharedData {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SharedData")
            .field(
                "aoide_gatekeeper",
                &format!(
                    "<opaque struct, {} pointers>",
                    Arc::strong_count(&self.aoide_gatekeeper)
                        + Arc::weak_count(&self.aoide_gatekeeper)
                ),
            )
            .field(
                "database_gatekeeper",
                &format!(
                    "<opaque struct, {} pointers>",
                    Arc::strong_count(&self.database_gatekeeper)
                        + Arc::weak_count(&self.database_gatekeeper)
                ),
            )
            .field(
                "tantivy_cache",
                &format!(
                    "<opaque struct, {} pointers>",
                    Arc::strong_count(&self.tantivy_cache) + Arc::weak_count(&self.tantivy_cache)
                ),
            )
            .field(
                "background_channel",
                &format!(
                    "<opaque struct, {} pointers>",
                    Arc::strong_count(&self.background_channel)
                        + Arc::weak_count(&self.background_channel)
                ),
            )
            .field(
                "background_tasks",
                &format!(
                    "<list of tasks, {} pointers>",
                    Arc::strong_count(&self.background_tasks)
                        + Arc::weak_count(&self.background_tasks)
                ),
            )
            .field(
                "shutdown_channel",
                &format!(
                    "<opaque struct, {} pointers>",
                    Arc::strong_count(&self.shutdown_channel)
                        + Arc::weak_count(&self.shutdown_channel)
                ),
            )
            .field(
                "jwt_session_keys",
                &format!(
                    "<opaque struct, {} pointers>",
                    Arc::strong_count(&self.jwt_session_keys)
                        + Arc::weak_count(&self.jwt_session_keys)
                ),
            )
            .finish()
    }
}
