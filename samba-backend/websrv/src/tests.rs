use std::{collections::HashMap, sync::Arc};

use aoide::{
    backend_embedded::storage::{
        provision_database as provision_aoide_database, DatabaseConfig, DatabaseSchemaMigrationMode,
    },
    search_index_tantivy::{IndexStorage, TrackIndex},
    storage_sqlite::connection::{
        pool::{
            gatekeeper::{Config as DatabaseConnectionGatekeeperConfig, Gatekeeper},
            Config as PoolConfig,
        },
        Config as ConnectionConfig, Storage,
    },
};
use axum::{
    body::Body,
    http::{Request, StatusCode},
    Router,
};
use diesel::{
    prelude::*,
    r2d2::{ConnectionManager as DieselConnectionManager, Pool},
};
use samba_backend_database::migrate_database;
use tantivy::query::QueryParser;
use tokio::sync::RwLock;
use tower::ServiceExt;

use crate::server::{
    background_tasks::BackgroundTask,
    data::{Keys, SharedData, TantivyCache},
};

async fn provision_inmemory_aoide() -> Gatekeeper {
    let storage = Storage::InMemory;
    let pool = PoolConfig {
        max_size: 8.try_into().unwrap(),
        gatekeeper: DatabaseConnectionGatekeeperConfig {
            acquire_read_timeout_millis: 10_000.try_into().unwrap(),
            acquire_write_timeout_millis: 30_000.try_into().unwrap(),
        },
    };

    provision_aoide_database(&DatabaseConfig {
        connection: ConnectionConfig { storage, pool },
        migrate_schema: Some(DatabaseSchemaMigrationMode::ApplyPending),
    })
    .unwrap()
}

async fn initialize_and_migrate_database() -> Gatekeeper {
    let manager = DieselConnectionManager::<SqliteConnection>::new(":memory:");
    let pool = Pool::builder().max_size(8).build(manager).unwrap();
    diesel::sql_query(
        r#"
      PRAGMA foreign_keys = 1;          -- check foreign key constraints
      PRAGMA defer_foreign_keys = 1;    -- delay enforcement of foreign key constraints until commit
      PRAGMA recursive_triggers = 1;    -- for recursive ON CASCADE DELETE actions
      PRAGMA encoding = 'UTF-8';
    "#,
    )
    .execute(&mut *pool.get().unwrap())
    .unwrap();
    migrate_database(&mut *pool.get().unwrap()).unwrap();

    Gatekeeper::new(pool, DatabaseConnectionGatekeeperConfig {
        acquire_read_timeout_millis: 10_000.try_into().unwrap(),
        acquire_write_timeout_millis: 30_000.try_into().unwrap(),
    })
}

async fn provide_app() -> Router {
    let aoide_gatekeeper = Arc::new(provision_inmemory_aoide().await);
    let database_gatekeeper = Arc::new(initialize_and_migrate_database().await);

    let track_index = TrackIndex::open_or_recreate(IndexStorage::InMemory).unwrap();
    let query_parser = QueryParser::for_index(&track_index.index, vec![
        track_index.fields.track_title,
        track_index.fields.track_artist,
        track_index.fields.album_title,
        track_index.fields.album_artist,
        track_index.fields.genre,
    ]);
    let tantivy_cache = Arc::new(TantivyCache {
        index: track_index,
        parser: query_parser,
    });

    let jwt_session_keys = Arc::new(Keys::new(b"1234"));
    let (shutdown_channel_sender, _) = tokio::sync::oneshot::channel::<()>();
    let (background_channel_sender, _) = tokio::sync::mpsc::unbounded_channel::<BackgroundTask>();
    let shutdown_channel_sender = Arc::new(shutdown_channel_sender);
    let background_channel = Arc::new(background_channel_sender);
    let background_tasks = Arc::new(RwLock::new(HashMap::new()));

    let shared_state = SharedData {
        aoide_gatekeeper: Arc::clone(&aoide_gatekeeper),
        database_gatekeeper: Arc::clone(&database_gatekeeper),
        tantivy_cache: Arc::clone(&tantivy_cache),
        background_channel: Arc::clone(&background_channel),
        background_tasks: Arc::clone(&background_tasks),
        shutdown_channel: shutdown_channel_sender,
        jwt_session_keys: Arc::clone(&jwt_session_keys),
    };

    super::routes::get_application_router(&jwt_session_keys).with_state(shared_state)
}

#[tokio::test]
async fn unauthorized_without_token() {
    let app = provide_app().await;

    let response = app
        .oneshot(
            Request::builder()
                .uri("/tracks")
                .body(Body::empty())
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);
}

#[tokio::test]
async fn not_found() {
    let app = provide_app().await;

    let response = app
        .oneshot(
            Request::builder()
                .uri("/does-not-exist")
                .body(Body::empty())
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(response.status(), StatusCode::NOT_FOUND);
    let body = axum::body::to_bytes(response.into_body(), usize::MAX)
        .await
        .unwrap();
    assert!(body.is_empty());
}
