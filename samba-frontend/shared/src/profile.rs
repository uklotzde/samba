use serde::{Deserialize, Serialize};

pub const LOCAL_STORAGE_PROFILE_KEY: &str = "samba-user-profile";

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct UserProfile {
    pub jwt: String,
    pub user_name: String,
    pub avatar_url: Option<String>,
    pub is_admin: bool,
}

impl UserProfile {
    #[must_use]
    pub fn guest() -> Self {
        Self {
            jwt: String::new(),
            user_name: String::from("Guest"),
            avatar_url: None,
            is_admin: false,
        }
    }

    #[must_use]
    pub fn is_guest(&self) -> bool {
        self.jwt.is_empty()
            && self.user_name == "Guest"
            && self.avatar_url.is_none()
            && !self.is_admin
    }
}

impl Default for UserProfile {
    fn default() -> Self {
        Self::guest()
    }
}
