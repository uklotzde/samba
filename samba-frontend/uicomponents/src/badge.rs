use leptos::*;

#[derive(Copy, Clone, Debug, Default)]
pub enum BadgeLevel {
    Neutral,
    Primary,
    Secondary,
    Accent,
    Ghost,
    #[default]
    Info,
    Success,
    Warning,
    Error,
    Outline,
}

impl BadgeLevel {
    #[must_use]
    pub const fn to_tailwind_class(&self) -> &'static str {
        match self {
            BadgeLevel::Neutral => "badge-neutral",
            BadgeLevel::Primary => "badge-primary",
            BadgeLevel::Secondary => "badge-secondary",
            BadgeLevel::Accent => "badge-accent",
            BadgeLevel::Ghost => "badge-ghost",
            BadgeLevel::Info => "badge-info",
            BadgeLevel::Success => "badge-success",
            BadgeLevel::Warning => "badge-warning",
            BadgeLevel::Error => "badge-error",
            BadgeLevel::Outline => "badge-outline",
        }
    }
}

#[must_use]
#[component]
pub fn Badge(
    #[prop(optional)] level: BadgeLevel,
    title: String,
    description: String,
    #[prop(optional)] classes: Option<String>,
) -> impl IntoView {
    view! {
        <div class=format!(
            "inline-block join {}",
            classes.clone().unwrap_or_default(),
        )>
            {(!title.is_empty())
                .then(|| {
                    view! {
                        <span class=format!(
                            "badge {} badge-outline join-item",
                            level.to_tailwind_class(),
                        )>{title}</span>
                    }
                })}
            {(!description.is_empty())
                .then(|| {
                    view! {
                        <span class=format!(
                            "badge join-item {}",
                            level.to_tailwind_class(),
                        )>{description}</span>
                    }
                })}
        </div>
    }
}
