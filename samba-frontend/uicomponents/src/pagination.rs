use std::num::NonZeroUsize;

use leptos::*;

/// Create a pagination with border which marks an active page in the range
/// `1` to `total_pages` (inclusive). Each pagination item is linked to the
/// page `i` where `i` represents the input to `pageLink` to produce the
/// actual link that will be written. Next to `page` three items will be
/// displayed before applying ellipsis (if ellipsis is applied, the first and
/// last page will always be linked).
#[must_use]
#[component]
pub fn Pagination(
    current_page: RwSignal<usize>,
    total_pages: ReadSignal<usize>,
    ellipsis_distance: usize,
) -> impl IntoView {
    // generate a page change action to be used for on click events
    let page_link = move |i| {
        move |_| {
            current_page.update(|page| *page = i);
        }
    };

    let lower = move || {
        current_page()
            .checked_sub(ellipsis_distance)
            .and_then(NonZeroUsize::new)
            .unwrap_or_else(|| NonZeroUsize::new(1).expect("1 > 0"))
            .get()
    };
    let upper = move || total_pages().min(current_page() + ellipsis_distance);

    let is_first_page = move || current_page() == 1;
    let previous_page = move || {
        is_first_page()
            .then_some(1)
            .unwrap_or_else(|| current_page() - 1)
    };

    let is_last_page = move || current_page() == total_pages();
    let next_page = move || {
        is_last_page()
            .then_some(1)
            .unwrap_or_else(|| current_page() + 1)
    };

    let pages_inbetween = move || {
        (lower()..=upper())
            .map(|i| {
                view! {
                    <button
                        class="join-item btn btn-outline"
                        class:btn-active=move || i == current_page()
                        on:click=page_link(i)
                        type="button"
                    >
                        {i}
                    </button>
                }
            })
            .collect::<Vec<_>>()
    };

    view! {
        <div class="join">
            {move || {
                (!is_first_page())
                    .then(|| {
                        view! {
                            <button
                                class="join-item btn btn-outline tooltip"
                                on:click=page_link(1)
                                disabled=is_first_page
                                data-tip="First"
                                type="button"
                            >
                                <span class="fa-solid fa-angles-left" />
                            </button>
                            <button
                                class="join-item btn btn-outline tooltip"
                                on:click=page_link(previous_page())
                                disabled=is_first_page
                                data-tip="Previous"
                                type="button"
                            >
                                <span class="fa-solid fa-angle-left" />
                            </button>
                        }
                    })
            }}
            {move || {
                (lower() != 1)
                    .then(|| {
                        view! {
                            <button
                                class="join-item btn btn-outline"
                                on:click=page_link(1)
                                type="button"
                            >
                                "1"
                            </button>
                            <button
                                class="join-item btn btn-outline btn-disabled"
                                on:click=page_link(current_page() - 1)
                                type="button"
                            >
                                "…"
                            </button>
                        }
                    })
            }} {pages_inbetween}
            {move || {
                (upper() != total_pages())
                    .then(|| {
                        view! {
                            <button
                                class="join-item btn btn-outline btn-disabled"
                                on:click=page_link(current_page() + 1)
                                type="button"
                            >
                                "…"
                            </button>
                            <button
                                class="join-item btn btn-outline"
                                on:click=page_link(total_pages())
                                type="button"
                            >
                                {total_pages}
                            </button>
                        }
                    })
            }}
            {move || {
                (!is_last_page())
                    .then(|| {
                        view! {
                            <button
                                class="join-item btn btn-outline tooltip"
                                on:click=page_link(next_page())
                                disabled=is_last_page
                                data-tip="Next"
                                type="button"
                            >
                                <span class="fa-solid fa-angle-right" />
                            </button>
                            <button
                                class="join-item btn btn-outline tooltip"
                                on:click=page_link(total_pages())
                                disabled=is_last_page
                                data-tip="Last"
                                type="button"
                            >
                                <span class="fa-solid fa-angles-right" />
                            </button>
                        }
                    })
            }}
        </div>
    }
}
