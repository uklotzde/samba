use leptos::*;

#[must_use]
#[component]
pub fn TagInput(
    bind_signal: RwSignal<String>,
    #[prop(optional)] valid_tags: Option<Vec<String>>,
) -> impl IntoView {
    // TODO: take inspiration from frameworks like https://metroui.org.ua/tag-input.html
    //       to overlay an input element in a div

    let invalid_class = move || {
        !valid_tags.as_ref().map_or(true, |valid| {
            bind_signal()
                .split(',')
                .all(|tag| valid.contains(&tag.trim().to_string()))
        })
    };

    view! {
        <div class="input-control">
            <input
                type="text"
                class="input-contains-icon"
                class:text-danger=invalid_class.clone()
                class:input-error=invalid_class
                placeholder="tag1, tag2, tag3"
                on:input=move |ev| {
                    bind_signal.set(event_target_value(&ev));
                }
                prop:value=bind_signal
            />
            <span class="icon">
                <span class="fa-solid fa-tags"></span>
            </span>
        </div>
    }
}
