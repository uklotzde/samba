use leptos::*;

#[derive(Copy, Clone, Debug, Default)]
pub enum ToastType {
    Success,
    Warning,
    Error,
    #[default]
    Info,
    Link,
    Primary,
    Gray,
    Dark,
}
impl ToastType {
    #[must_use]
    pub const fn to_cirrus_class(&self) -> &'static str {
        match self {
            ToastType::Success => "toast--success",
            ToastType::Warning => "toast--warning",
            ToastType::Error => "toast--error",
            ToastType::Info => "toast--info",
            ToastType::Link => "toast--link",
            ToastType::Primary => "toast--primary",
            ToastType::Gray => "toast--gray",
            ToastType::Dark => "toast--dark",
        }
    }
}

#[must_use]
#[component]
pub fn Toast(
    #[prop(optional)] kind: ToastType,
    #[prop(optional)] title: Option<String>,
    message: String,
) -> impl IntoView {
    view! {
        <div class=format!(
            "toast {}",
            kind.to_cirrus_class(),
        )>
            {title.map(|title| view! { <h4 class="toast__title">{title}</h4> })}
            <p>{message}</p>
        </div>
    }
}
