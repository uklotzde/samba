use std::cmp::Ordering;

use aoide::{
    tag::{FacetId, Label, PlainTag},
    Track,
};

const TAG_SEPARATOR: &str = ", ";

fn plain_tag_max_score_cmp(lhs: &PlainTag<'_>, rhs: &PlainTag<'_>) -> Ordering {
    lhs.score
        .partial_cmp(&rhs.score)
        .unwrap_or(Ordering::Equal)
        .reverse()
}

fn sort_plain_tags_by_score_desc(plain_tags: &mut [&PlainTag<'_>]) {
    // We don't care to preserve the ordering between tags with equal score,
    // so using unstable sorting is fine (and faster).
    plain_tags.sort_unstable_by(|lhs, rhs| plain_tag_max_score_cmp(lhs, rhs));
}

/// Join all tags for a given facet to a comma-separated string.
pub fn join_tags_for_facet(track: &Track, facet_id: &FacetId<'_>) -> Option<String> {
    let mut tags = track
        .tags
        .facets
        .iter()
        .filter_map(|faceted_tags| {
            (faceted_tags.facet_id == *facet_id).then_some(faceted_tags.tags.iter())
        })
        .flatten()
        .collect::<Vec<_>>();
    if tags.is_empty() {
        return None;
    }
    sort_plain_tags_by_score_desc(tags.as_mut_slice());

    Some(
        tags.iter()
            .filter_map(|tag| tag.label.as_ref().map(Label::as_str))
            .intersperse(TAG_SEPARATOR)
            .collect(),
    )
}

/// Get all tags for `track` excluding the facets identified by the keys in
/// `exclusions`. Returns a vector of remaining facets and their tags (the
/// tags are joined to string after ordering them by significance).
pub fn get_tags_for_track_excluding_facets(
    track: &Track,
    exclusions: &[&FacetId<'_>],
) -> Vec<(String, String)> {
    let mut plain_tags = track.tags.plain.iter().collect::<Vec<_>>();
    sort_plain_tags_by_score_desc(plain_tags.as_mut_slice());
    let plain_iter = plain_tags
        .iter()
        .filter_map(|tag| tag.label.as_ref().map(ToString::to_string))
        .map(|label| (String::new(), label));
    let faceted_iter = track
        .tags
        .facets
        .iter()
        .filter(|faceted| !exclusions.contains(&&faceted.facet_id))
        .map(|faceted| {
            let mut tags = faceted.tags.iter().collect::<Vec<_>>();
            debug_assert!(!tags.is_empty());
            sort_plain_tags_by_score_desc(tags.as_mut_slice());

            (
                faceted.facet_id.to_string(),
                tags.iter()
                    .filter_map(|tag| tag.label.as_ref().map(Label::as_str))
                    .intersperse(TAG_SEPARATOR)
                    .collect::<String>(),
            )
        });
    plain_iter.chain(faceted_iter).collect()
}
