use leptos::*;
use leptos_router::*;

pub enum EventAction {
    Create,
    Edit(i32),
    View(i32),
    List,
}

#[derive(Params, Clone, Copy, PartialEq)]
pub struct EventId {
    pub id: i32,
}

impl std::fmt::Display for EventAction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&match self {
            EventAction::Create => "/create".into(),
            EventAction::Edit(id) => format!("/edit/{id}"),
            EventAction::View(id) => format!("/view/{id}"),
            EventAction::List => "".into(),
        })
    }
}
