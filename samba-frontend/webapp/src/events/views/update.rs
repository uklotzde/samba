use chrono::NaiveDateTime;
use leptos::*;
use leptos_router::{use_navigate, use_params};
use samba_frontend_uicomponents::{DropdownSelect, Heading, Space, TagInput};
use samba_shared::events::{Event, StoredEvent};

use super::super::{
    backend::{create_event, fetch_event, update_event},
    navigation::{EventAction, EventId},
};
use crate::{views::PleaseWaitView, AppRoutes};

#[component]
pub fn EventUpdateView() -> impl IntoView {
    // TODO: proper error handling
    let event_id = use_params::<EventId>();
    let event = Resource::new(event_id, move |_| async move {
        fetch_event(event_id.get().ok()?.id).await.ok()
    });

    view! {
        <section>
            <Suspense fallback=|| {
                view! { <PleaseWaitView /> }
            }>{move || { event().map(|evt| view! { <EventUpdateHandler event=evt /> }) }}</Suspense>
        </section>
    }
}

#[component]
fn EventUpdateHandler(event: Option<StoredEvent>) -> impl IntoView {
    let event_id = event.as_ref().map_or(0, |e| e.identifier);
    let (event_title, set_event_title) =
        create_signal(event.as_ref().map(|e| e.title.clone()).unwrap_or_default());
    let (event_description, set_event_description) = create_signal(
        event
            .as_ref()
            .map(|e| e.description.clone())
            .unwrap_or_default(),
    );
    let (event_location, set_event_location) = create_signal(
        event
            .as_ref()
            .map(|e| e.location.clone())
            .unwrap_or_default(),
    );
    let (event_date, set_event_date) = create_signal(
        event
            .as_ref()
            .map(|e| e.date.format("%Y-%m-%d %H:%M:%S").to_string())
            .unwrap_or_default(),
    );

    // TODO: initialize from event
    let event_type = RwSignal::new(
        event
            .as_ref()
            .map(|e| {
                e.competition_details
                    .as_ref()
                    .map_or_else(|| "Prom".into(), |_| "Competition".to_owned())
            })
            .unwrap_or_default(),
    );
    let valid_event_types = vec!["Competition".to_string(), "Prom".to_string()];

    let event_tags = RwSignal::new(
        event
            .as_ref()
            .map(|e| e.tags.join(", "))
            .unwrap_or_default(),
    );

    let submit_action = move |_| {
        let date_time =
            if let Ok(date_time) = NaiveDateTime::parse_from_str(&event_date(), "%Y-%m-%dT%H:%M") {
                date_time
            } else {
                return;
            };

        let updated_event = Event {
            date: date_time,
            title: event_title().clone(),
            description: event_description().clone(),
            location: event_location().clone(),
            organizers: vec![],
            tags: event_tags()
                .split(',')
                .map(|x| x.trim().to_owned())
                .collect::<Vec<String>>(),
            competition_details: None,
            helpers: vec![],
        };

        if event_id == 0 {
            spawn_local(async move {
                let _ = create_event(&updated_event).await;
            });
        } else {
            spawn_local(async move {
                let _ = update_event(event_id, &updated_event).await;
            });
        };

        // TODO: reintroduce error handling
        // if request_errored {
        //     app_state.add_message(Message::new_error(
        //         "Saving event failed".to_owned(),
        //         "Please make sure the backend is running or try again in a few
        // seconds.".to_owned(),     ));
        // }

        // TODO: maybe view newly created event instead?
        use_navigate()(
            &AppRoutes::Events(EventAction::List).to_string(),
            Default::default(),
        );
    };

    view! {
        <Heading>
            (if let Some(evt) = (*event_ref).get().as_ref() {
                format!("Updating event “{}”", evt.title)
            } else {
                "Creating new event".to_owned()
            })
        </Heading>
        <form action="javascript:void(0);" method="post" on:submit=submit_action>
            <div class="row">
                <div class="col">
                    <div class="input-control">
                        <input
                            type="text"
                            class="input-contains-icon input-contains-icon-right"
                            required=true
                            placeholder="Your event title"
                            on:input=move |ev| {
                                set_event_title(event_target_value(&ev));
                            }
                            prop:value=event_title
                        />
                        <span class="icon">
                            <span class="fa-solid fa-pen"></span>
                        </span>
                        // TODO: automate marking required fields
                        <span class="icon icon-right">
                            <span class="fa-solid fa-asterisk tooltip" data-tip="required"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="input-control">
                        <input
                            type="datetime-local"
                            class="input-contains-icon input-contains-icon-right"
                            required=true
                            placeholder="When?"
                            on:input=move |ev| {
                                set_event_date(event_target_value(&ev));
                            }
                            prop:value=event_date
                        />
                        <span class="icon">
                            <span class="fa-solid fa-calendar"></span>
                        </span>
                        // TODO: automate marking required fields
                        <span class="icon icon-right">
                            <span class="fa-solid fa-asterisk tooltip" data-tip="required"></span>
                        </span>
                    </div>
                </div>
                <div class="col">
                    <div class="input-control">
                        <input
                            type="text"
                            class="input-contains-icon"
                            placeholder="Where?"
                            on:input=move |ev| {
                                set_event_location(event_target_value(&ev));
                            }
                            prop:value=event_location
                        />
                        <span class="icon">
                            <span class="fa-solid fa-location-dot"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="input-control">
                        <DropdownSelect
                            bind_signal=event_type
                            options=valid_event_types
                            required=true
                        />
                    </div>
                </div>
                <div class="col">
                    <TagInput bind_signal=event_tags />
                </div>
            </div>
            <div class="form-section">
                <label for="event-description">"Description"</label>
                <div class="row">
                    <textarea
                        id="event-description"
                        placeholder="Event decription here"
                        on:input=move |ev| {
                            set_event_description(event_target_value(&ev));
                        }
                        prop:value=event_description
                    ></textarea>
                </div>
            </div>
            // TODO: organizers
            <Space />
            <div class="form-section">
                <button type="submit" class="btn-primary u-pull-right ml-1">
                    "Submit"
                </button>
                <a class="btn u-pull-right" href=AppRoutes::Events(EventAction::List)>
                    "Cancel"
                </a>
            </div>
        </form>
    }
}
