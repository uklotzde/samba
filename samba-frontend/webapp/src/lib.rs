#![feature(iter_intersperse)]
#![allow(clippy::wildcard_imports)]

mod aoide;
mod events;
mod library;
mod management;
mod practice;
mod routines;
mod settings;
mod util;
mod views;

use leptos::*;
pub use views::App;

enum AppRoutes {
    Home,
    About,
    Events(events::EventAction),
    Library(library::LibraryAction),
    Practice(practice::PracticeAction),
    Routines(routines::RoutineAction),
    Settings(settings::SettingsComponent),
}

impl std::fmt::Display for AppRoutes {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&match self {
            AppRoutes::Home => "/dashboard".into(),
            AppRoutes::About => "/about".into(),
            AppRoutes::Events(action) => format!("/events{action}"),
            AppRoutes::Library(action) => format!("/library{action}"),
            AppRoutes::Practice(kind) => format!("/practice{kind}"),
            AppRoutes::Routines(action) => format!("/routines{action}"),
            AppRoutes::Settings(component) => format!("/settings{component}"),
        })
    }
}

impl IntoAttribute for AppRoutes {
    #[inline(always)]
    fn into_attribute(self) -> Attribute {
        Attribute::String(Oco::Owned(self.to_string()))
    }

    #[inline(always)]
    fn into_attribute_boxed(self: Box<Self>) -> Attribute {
        Attribute::String(Oco::Owned(self.to_string()))
    }
}

#[cfg(feature = "ssr")]
pub mod fallback;

#[cfg(feature = "hydrate")]
#[wasm_bindgen::prelude::wasm_bindgen]
pub fn hydrate() {
    use samba_frontend_shared::{AppState, UserProfile};
    use views::App;

    _ = console_log::init_with_level(log::Level::Debug);
    console_error_panic_hook::set_once();

    leptos::mount_to_body(move || {
        provide_context(AppState::new("http://localhost:8081".into(), false));

        view! { <App /> }
    });
}
