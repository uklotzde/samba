use aoide::Track;
use leptos::*;
use samba_shared::tracks::lint::{apply_lints, predefined_lints, LintSeverity};

#[component]
pub fn LintRow(track: Signal<Track>) -> impl IntoView {
    // TODO: fetch dances from backend

    // TODO: make library lints configurable in preferences
    let lint_results = move || {
        apply_lints(
            &[
                Box::new(predefined_lints::BadArtist),
                // Box::new(predefined_lints::BadGenre {
                //     valid_dance_names: &dances::DANCE_NAMES,
                // }),
                Box::new(predefined_lints::MpmMismatch),
                Box::new(predefined_lints::NoAlbum),
                Box::new(predefined_lints::NonEmptyComment),
                // Box::new(predefined_lints::PropertiesFromFilename {
                //     dance_abbreviations: &dances::DANCES,
                // })
            ],
            &track(),
        )
    };

    view! {
        <Show
            when=move || !lint_results().is_empty()
            fallback=|| {
                view! {
                    <div role="alert" class="alert alert-success w-full my-8 flex justify-center">
                        <p>"This track does not trigger any metadata lints."</p>
                    </div>
                }
            }
        >
            <div role="alert" class="alert block w-full my-8">
                <p class="pb-2">"This track triggered the following metadata lints:"</p>
                <For each=lint_results key=|l| l.message.clone() let:result>
                    <div class="flex gap-4">
                        <span
                            class="badge"
                            class=(
                                "badge-info",
                                move || matches!(result.severity, LintSeverity::Info),
                            )
                            class=(
                                "badge-warning",
                                move || matches!(result.severity, LintSeverity::Warning),
                            )
                            class=(
                                "badge-error",
                                move || matches!(result.severity, LintSeverity::Critical),
                            )
                        >
                            {result.severity.to_string()}
                        </span>
                        <div class="grow">
                            <p>{result.message}</p>
                            <p class="text-sm">"Suggested fix: " {result.suggested_fix}</p>
                        </div>
                        // TODO: allow fixing of lint if fix operation available
                        <div class="h-[10%]">
                            <a class="btn">
                                <span class="fa-solid fa-hammer"></span>
                            </a>
                        </div>
                    </div>
                </For>
            </div>
        </Show>
    }
}
