use aoide::{
    media::{
        artwork::{thumbnail_png_data_uri, Artwork, ArtworkImage, EmbeddedArtwork},
        content::ContentMetadata,
        Source,
    },
    util::color::Color,
    Track, TrackEntity,
};
use leptos::*;
use samba_frontend_shared::AppState;
use samba_shared::{
    dancing::{Dance, FACET_ID_DANCE},
    preferences::TrackRowIndicator,
    tracks::metadata::{get_mpm, get_primary_dance},
    utils::format_duration_millis,
};

use super::detailsrow::TrackDetailsRow;

/// Display the duration of a source.
fn display_duration(source: &Source) -> String {
    let ContentMetadata::Audio(audio_content) = &source.content.metadata;
    audio_content.duration.map_or_else(
        || "--:--:--".into(),
        |duration| {
            let duration = duration.value() as u128;
            format_duration_millis(duration)
        },
    )
}

fn display_dance(dance_abbreviations: &[String], known_dances: &[Dance]) -> String {
    if dance_abbreviations.is_empty() {
        return "–".to_owned();
    }

    dance_abbreviations
        .iter()
        .find_map(|abbreviation| {
            known_dances
                .iter()
                .find(|dance| &dance.abbreviation == abbreviation)
                .map(|dance| dance.name.clone())
        })
        .unwrap_or_else(|| {
            let lc_str = dance_abbreviations
                .first()
                .expect("There must be at least one element.")
                .to_lowercase();
            let mut lc = lc_str.chars();
            match lc.next() {
                None => String::new(),
                Some(f) => f.to_uppercase().chain(lc).collect(),
            }
        })
}

fn display_mpm(track: &Track, known_dances: &[Dance]) -> impl IntoView {
    get_mpm(track).map_or_else(
        || {
            let Some(dance) = get_primary_dance(track).and_then(|primary_dance| {
                known_dances
                    .iter()
                    .find(|dance| {
                        dance.abbreviation == primary_dance || dance.name == primary_dance
                    })
                    .cloned()
            }) else {
                return view! { "–" }.into_view();
            };

            if let Some(bpm) = track.metrics.tempo_bpm {
                return ((bpm.value() / dance.number_of_crotchets as f64).round() as u64)
                    .into_view();
            }

            let dance_mpm_max = dance.mpm_range.end.saturating_sub(1);
            let mpm_string = if dance_mpm_max <= dance.mpm_range.start {
                format!("{}", dance.mpm_range.start)
            } else {
                format!("{}–{}", dance.mpm_range.start, dance.mpm_range.end - 1)
            };
            view! {
                <span class="text-gray-600 tooltip" data-tip="MPM extracted from dance">
                    {mpm_string}
                </span>
            }
            .into_view()
        },
        |mpm| mpm.into_view(),
    )
}

const DEFAULT_TRACK_TITLE: &str = "Unknown";
const DEFAULT_TRACK_ARTIST: &str = "Unknown";
const DEFAULT_ALBUM_TITLE: &str = "Unknown";

#[component]
pub fn TrackRow(track: TrackEntity, dances: Signal<Vec<Dance>>) -> impl IntoView {
    let dance_abbreviations = track
        .body
        .track
        .tags
        .as_ref()
        .facets
        .iter()
        .find(|facet| facet.facet_id == *FACET_ID_DANCE)
        .map(|faceted| {
            // TODO: check that they are sorted by priority?
            faceted
                .tags
                .iter()
                .filter_map(|tag| tag.label.as_ref().map(ToString::to_string))
                .collect::<Vec<_>>()
        })
        .unwrap_or_default();
    let mpm = display_mpm(&track.body.track, &dances.get());

    let (track_entity, _) = create_signal(track);
    let track_signal = Signal::derive(move || track_entity().body.track.clone());

    let show_details = RwSignal::new(false);
    let show_preview = RwSignal::new(Some(false));
    let show_lint = RwSignal::new(Some(false));

    // TODO: background color of row as function of track.body.extra?.color
    //       else zebra look
    let track_title = move || {
        track_signal()
            .track_title()
            .unwrap_or(DEFAULT_TRACK_TITLE)
            .to_owned()
    };
    let track_artist = move || {
        track_signal()
            .track_artist()
            .unwrap_or(DEFAULT_TRACK_ARTIST)
            .to_owned()
    };
    let album_title = move || {
        track_signal()
            .album_title()
            .unwrap_or(DEFAULT_ALBUM_TITLE)
            .to_owned()
    };
    let classes = move || {
        if show_details() {
            "track-row bg-base-200"
        } else {
            "track-row"
        }
    };

    view! {
        <tr
            class=classes
            on:click=move |_| {
                show_details.set(!show_details());
            }
        >
            <TrackRowIndicatorCell track=track_signal />
            <td>{track_title}</td>
            <td>{track_artist}</td>
            <td>{album_title}</td>
            <td>{display_dance(&dance_abbreviations, &dances())}</td>
            <td class="justify-center">{mpm}</td>
            <td class="justify-right">{display_duration(&track_signal().media_source)}</td>
        </tr>
        <Show when=show_details>
            <tr>
                <TrackDetailsRow
                    track=track_entity
                    show_preview_row=show_preview
                    show_lint_row=show_lint
                />
            </tr>
        </Show>
    }
}

#[component]
fn TrackRowIndicatorCell(track: Signal<Track>) -> impl IntoView {
    let app_state =
        use_context::<AppState>().expect("Tracks are only shown when logged in and initialized");
    let has_track_row_indicator = move || {
        !matches!(
            (app_state.user_preferences)().track_row_indicator,
            TrackRowIndicator::None
        )
    };

    view! {
        <Show
            when=has_track_row_indicator
            fallback=|| view! { <td class="track-artwork-preview"></td> }
        >
            {move || {
                let track_color = Memo::new(move |_| {
                    track()
                        .color
                        .and_then(|color| {
                            if let Color::Rgb(rgb) = color { Some(rgb.to_string()) } else { None }
                        })
                        .unwrap_or_else(|| {
                            // Use the predominant artwork color as a fallback.
                            track()
                                .media_source
                                .artwork
                                .as_ref()
                                .and_then(|artwork| match artwork {
                                    Artwork::Embedded(
                                        EmbeddedArtwork {
                                            image: ArtworkImage { color: Some(color), .. },
                                        },
                                    ) => Some(color.to_string()),
                                    _ => None,
                                })
                                .unwrap_or_else(|| "transparent".to_string())
                        })
                });
                // TODO: respect cover art setting if user wants full-sized cover art
                let thumbnail_image = Memo::new(move |_| {
                    track()
                        .media_source
                        .artwork
                        .as_ref()
                        .and_then(|artwork| match artwork {
                            Artwork::Embedded(
                                EmbeddedArtwork { image: ArtworkImage { thumbnail, .. } },
                            ) => *thumbnail,
                            _ => None,
                        })
                        .map(|thumbnail| {
                            let data_uri = thumbnail_png_data_uri(&thumbnail);
                            view! {
                                <img src=data_uri
                                     class="absolute top-0 h-full max-w-full" />
                            }.into_view()
                        })
                });
                view! {
                    <td class="track-artwork-preview relative"
                        style=("--track-color", track_color)>
                        {thumbnail_image}
                    </td>
                }
            }}
        </Show>
    }
}
