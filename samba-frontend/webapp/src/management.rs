use codee::string::JsonSerdeWasmCodec as JsonCodec;
use leptos::*;
use leptos_use::storage::use_local_storage;
use samba_frontend_shared::{
    profile::{UserProfile, LOCAL_STORAGE_PROFILE_KEY},
    AppState, Message,
};
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::JsCast;

use crate::util::{refresh_page, REQWEST_CLIENT};

async fn shutdown_request() -> bool {
    let Some(app_state) = use_context::<AppState>() else {
        return false;
    };

    let shutdown = REQWEST_CLIENT
        .get(&format!("{}/shutdown", &app_state.backend_url))
        .bearer_auth(app_state.get_jwt())
        .send()
        .await;
    if let Ok(v) = shutdown {
        v.text().await.is_ok()
    } else {
        false
    }
}

pub async fn logout() {
    let Some(app_state) = use_context::<AppState>() else {
        return;
    };
    if REQWEST_CLIENT
        .get(format!("{}/user/logout", &app_state.backend_url))
        .bearer_auth(app_state.get_jwt())
        .send()
        .await
        .is_err()
    {
        app_state.add_message(Message::new_error(
            "Failed to log you out".into(),
            "The backend could be reached or could not destroy your session. Please try again."
                .into(),
        ));
    }

    refresh_page();

    // Clear the fallback-cookie which contains the session token. As clearing
    // is non-trivial, we simply overwrite it and force expiry.
    #[cfg(target_arch = "wasm32")]
    {
        let html_doc = document().dyn_into::<web_sys::HtmlDocument>().unwrap();
        html_doc
            .set_cookie("jwt=;path=/;Max-Age=-99999999;")
            .unwrap();
    }

    // Clearing local storage is mostly non-critical. As the server session
    // will be destroyed, it is okay if this fails. Still, it determines the
    // sucess of the logout operation as a whole.
    let (_, _, delete_profile) =
        use_local_storage::<UserProfile, JsonCodec>(LOCAL_STORAGE_PROFILE_KEY);
    delete_profile();
}

pub async fn shutdown() -> bool {
    let Some(app_state) = use_context::<AppState>() else {
        return false;
    };

    let message = if shutdown_request().await {
        Message::new_info(
            "Samba has been shut down.".into(),
            "No further interaction is possible. If you want to resume working with samba, please \
             restart all necessary components."
                .into(),
        )
    } else {
        Message::new_error(
            "Samba has been unable to shut down.".into(),
            "Please make sure the server is still running and check its logs.".into(),
        )
    };
    app_state.add_message(message);

    false
}
