use aoide::{playlist::PlaylistWithEntries, util::clock::OffsetDateTimeMs};

mod edit;
mod history;
mod list;
mod play;

pub use self::{
    edit::{EditPracticeMode, EditPracticeView},
    history::PracticeHistoryView,
    list::PracticeListView,
    play::PracticePlayView,
};

fn get_last_modification_date(playlist: &PlaylistWithEntries) -> Option<OffsetDateTimeMs> {
    playlist.entries.iter().fold(None, |acc, entry| {
        Some(
            acc.and_then(|a| (a < entry.added_at).then_some(entry.added_at.clone()))
                .unwrap_or(entry.added_at.clone()),
        )
    })
}
