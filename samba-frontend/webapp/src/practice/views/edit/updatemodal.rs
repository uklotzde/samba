use std::time::Duration;

use leptos::*;
use samba_frontend_uicomponents::DropdownSelect;
use samba_shared::playlists::{AudioPlayback, AudioSource, PlaybackProperties, PlaylistItem};
use uuid::Uuid;

use super::super::super::SessionItem;

#[component]
pub fn SessionItemUpdateModal(
    modal_visibility: RwSignal<bool>,
    session_items: RwSignal<Vec<SessionItem>>,
    #[prop(optional)] current_session_item: Option<SessionItem>,
) -> impl IntoView {
    let is_update = current_session_item.is_some();

    let (item_to_update, _) = create_signal(current_session_item.as_ref().map(|item| item.uuid));
    let submit_value = RwSignal::new(current_session_item.clone());
    let submit_action = move |_| {
        // TODO: invalidity warning?
        if let Some(value) = submit_value.get().as_ref() {
            session_items.update(|items| {
                if let Some(pos) = items
                    .iter()
                    .position(|element| element.uuid == item_to_update().unwrap_or(value.uuid))
                {
                    items[pos] = value.clone();
                } else {
                    items.push(value.clone());
                }
            });
        }

        // Update the submit value to allow for multiple insertions of the same
        // value consecutively.
        submit_value.update(|session_item| {
            if let Some(item) = session_item.as_mut() {
                item.uuid = Uuid::new_v4();
            }
        });
        modal_visibility.set(false);
    };

    // TODO: factor out into ToString implementation?
    let item_type = RwSignal::new(
        current_session_item
            .as_ref()
            .map(|item| match &item.item {
                PlaylistItem::Playback(AudioPlayback {
                    audio_source: AudioSource::TextToSpeech(_),
                    ..
                }) => "Text to speech (TTS)",
                PlaylistItem::Playback(_) => "Track",
                PlaylistItem::Pause(_) => "Pause",
                PlaylistItem::Visual(_) => "Visual separator",
                PlaylistItem::Break => "Break",
                PlaylistItem::Stop => "Stop",
            })
            .map(ToString::to_string)
            .unwrap_or_default(),
    );
    let (valid_item_types, _) = create_signal(vec![
        "Text to speech (TTS)".into(),
        "Track".into(),
        "Pause".into(),
        "Break".into(),
        "Stop".into(),
        "Visual separator".into(),
    ]);

    let type_specific_input = RwSignal::new(View::default().into_view());
    Effect::new(move |_| {
        type_specific_input.set(match item_type().as_str() {
            "Text to speech (TTS)" => {
                // TODO: handle playback properties
                let (text_signal, set_text_signal) = create_signal(
                    current_session_item
                        .as_ref()
                        .map(|item| {
                            if let PlaylistItem::Playback(AudioPlayback {
                                audio_source: AudioSource::TextToSpeech(ref text),
                                ..
                            }) = item.item
                            {
                                text.clone()
                            } else {
                                String::new()
                            }
                        })
                        .unwrap_or_default(),
                );

                Effect::new(move |_| {
                    submit_value.set(Some(SessionItem::new(PlaylistItem::Playback(
                        AudioPlayback {
                            audio_source: AudioSource::TextToSpeech(text_signal().clone()),
                            playback_properties: Default::default(),
                        },
                    ))));
                });

                view! {
                    <div class="flex gap-2 items-center">
                        <label for="item-track-tts">"Text to speak:"</label>
                        <input
                            class="input grow"
                            id="item-track-tts"
                            placeholder="Heat 1"
                            type="text"
                            on:input=move |ev| {
                                set_text_signal(event_target_value(&ev));
                            }
                            prop:value=text_signal
                        />
                    </div>
                }
                .into_view()
            },
            "Track" => {
                let playback_props = RwSignal::new(
                    current_session_item
                        .as_ref()
                        .map(|item| {
                            if let PlaylistItem::Playback(AudioPlayback {
                                ref playback_properties,
                                ..
                            }) = item.item
                            {
                                playback_properties.clone()
                            } else {
                                Default::default()
                            }
                        })
                        .unwrap_or_default(),
                );

                let (query_signal, set_query_signal) = create_signal(
                    current_session_item
                        .as_ref()
                        .map(|item| {
                            if let PlaylistItem::Playback(AudioPlayback {
                                audio_source: AudioSource::TrackQuery(ref query),
                                ..
                            }) = item.item
                            {
                                query.to_string()
                            } else if let PlaylistItem::Playback(AudioPlayback {
                                audio_source: AudioSource::TrackUid(ref uid),
                                ..
                            }) = item.item
                            {
                                format!("uid:{uid}")
                            } else {
                                String::new()
                            }
                        })
                        .unwrap_or_default(),
                );

                Effect::new(move |_| {
                    submit_value.set(Some(SessionItem::new(PlaylistItem::Playback(
                        AudioPlayback {
                            audio_source: AudioSource::TrackQuery(query_signal().clone()),
                            playback_properties: playback_props().clone(),
                        },
                    ))));
                });

                view! {
                    <div class="flex gap-2 items-center">
                        <label for="item-track-query">"Track query (blank = random track):"</label>
                        <input
                            class="input grow"
                            id="item-track-query"
                            placeholder="dance:RB"
                            type="text"
                            on:input=move |ev| {
                                set_query_signal(event_target_value(&ev));
                            }
                            prop:value=query_signal
                        />
                    </div>
                    <PlaybackPropertiesInput playback_properties=playback_props />
                }
                .into_view()
            },
            "Pause" => {
                let (duration_signal, set_duration_signal) =
                    create_signal(current_session_item.as_ref().map_or(20, |item| {
                        if let PlaylistItem::Pause(ref duration) = item.item {
                            duration.as_secs()
                        } else {
                            20
                        }
                    }));
                Effect::new(move |_| {
                    submit_value.set(Some(SessionItem::new(PlaylistItem::Pause(
                        Duration::from_secs(duration_signal()),
                    ))));
                });

                view! {
                    <div class="flex gap-2 items-center">
                        <label for="item-duration">"Duration:"</label>
                        <input
                            class="input grow"
                            id="item-duration"
                            type="number"
                            on:input=move |ev| {
                                if let Ok(val) = event_target_value(&ev).parse::<u64>() {
                                    set_duration_signal(val);
                                }
                            }
                            prop:value=duration_signal
                        />
                    </div>
                }
                .into_view()
            },
            "Break" => {
                Effect::new(move |_| {
                    submit_value.set(Some(SessionItem::new(PlaylistItem::Break)));
                });
                View::default().into_view()
            },
            "Stop" => {
                Effect::new(move |_| {
                    submit_value.set(Some(SessionItem::new(PlaylistItem::Stop)));
                });
                View::default().into_view()
            },
            "Visual separator" => {
                let (label_signal, set_label_signal) = create_signal(
                    current_session_item
                        .as_ref()
                        .map_or_else(String::new, |item| {
                            if let PlaylistItem::Visual(ref label) = item.item {
                                label.clone().unwrap_or_default()
                            } else {
                                String::new()
                            }
                        }),
                );
                Effect::new(move |_| {
                    submit_value.set(Some(SessionItem::new(PlaylistItem::Visual(
                        (!label_signal().is_empty()).then(|| label_signal().clone()),
                    ))));
                });

                view! {
                    <div class="flex gap-2 items-center">
                        <label for="separator-label">"Label:"</label>
                        <input
                            class="input grow"
                            id="separator-label"
                            placeholder="leave empty for separator without text"
                            type="text"
                            on:input=move |ev| {
                                set_label_signal(event_target_value(&ev));
                            }
                            prop:value=label_signal
                        />
                    </div>
                }
                .into_view()
            },
            _ => View::default().into_view(),
        });
    });

    view! {
        <dialog class="modal modal-bottom sm:modal-middle" open=modal_visibility>
            <div
                class="modal-box md:flex md:flex-col md:justify-between md:h-[50%] md:min-h-[50%]"
                role="document"
            >
                <div class="flex items-center">
                    <h2 class="grow font-bold text-xl">
                        {if is_update {
                            "Edit practice session item"
                        } else {
                            "Add practice session item"
                        }}
                    </h2>
                    <a
                        class="btn btn-circle btn-ghost"
                        aria-label="Close"
                        on:click=move |_| {
                            modal_visibility.set(false);
                        }
                    >
                        <span class="fa-wrapper fa fa-times"></span>
                    </a>
                </div>
                <div class="flex gap-2 items-center">
                    <label for="item-type">"Type:"</label>
                    // TODO: assign ID item-type
                    <DropdownSelect
                        bind_signal=item_type
                        options=valid_item_types()
                        required=false
                        classes="grow".into()
                    />
                </div>
                <div class="my-2">{type_specific_input}</div>
                <div class="mt-auto flex gap-2 items-center justify-end">
                    <button
                        class="btn"
                        on:click=move |_| {
                            modal_visibility.set(false);
                        }
                        type="button"
                    >
                        "Cancel"
                    </button>
                    <button
                        class="btn btn-primary"
                        disabled=move || item_type.get().is_empty()
                        on:click=submit_action
                    >
                        {if is_update { "Update" } else { "Add" }}
                    </button>
                </div>
            </div>
        </dialog>
    }
}

#[component]
fn PlaybackPropertiesInput(playback_properties: RwSignal<PlaybackProperties>) -> impl IntoView {
    let (start_offset, set_start_offset) = create_signal(playback_properties().start_at.as_secs());
    let (duration, set_duration) = create_signal(playback_properties().duration.as_secs());
    let (fade_in, set_fade_in) = create_signal(playback_properties().fade_in.as_secs());
    let (fade_out, set_fade_out) = create_signal(playback_properties().fade_out.as_secs());

    Effect::new(move |_| {
        playback_properties.set(PlaybackProperties {
            start_at: Duration::from_secs(start_offset()),
            duration: Duration::from_secs(duration()),
            fade_in: Duration::from_secs(fade_in()),
            fade_out: Duration::from_secs(fade_out()),
        });
    });

    view! {
        <div class="my-4 divider" />
        <div class="flex gap-2 items-center">
            <label for="playback-props-start">"Start at (seconds):"</label>
            <input
                class="input grow"
                id="playback-props-start"
                type="number"
                on:input=move |ev| {
                    if let Ok(val) = event_target_value(&ev).parse::<u64>() {
                        set_start_offset(val);
                    }
                }
                prop:value=start_offset
            />
        </div>
        <div class="flex gap-2 items-center">
            <label for="playback-props-duration">"Play for (seconds):"</label>
            <input
                class="input grow"
                id="playback-props-duration"
                type="number"
                on:input=move |ev| {
                    if let Ok(val) = event_target_value(&ev).parse::<u64>() {
                        set_duration(val);
                    }
                }
                prop:value=duration
            />
        </div>
        <div class="flex gap-2 items-center">
            <label for="playback-props-fadein">"Fade in (seconds):"</label>
            <input
                class="input grow"
                id="playback-props-fadein"
                type="number"
                on:input=move |ev| {
                    if let Ok(val) = event_target_value(&ev).parse::<u64>() {
                        set_fade_in(val);
                    }
                }
                prop:value=fade_in
            />
        </div>
        <div class="flex gap-2 items-center">
            <label for="playback-props-fadeout">"Fade out (seconds):"</label>
            <input
                class="input grow"
                id="playback-props-fadeout"
                type="number"
                on:input=move |ev| {
                    if let Ok(val) = event_target_value(&ev).parse::<u64>() {
                        set_fade_out(val);
                    }
                }
                prop:value=fade_out
            />
        </div>
    }
}
