use aoide::playlist::EntityWithEntries as PlaylistEntityWithEntries;
use leptos::*;
use samba_frontend_shared::AppState;
use samba_frontend_uicomponents::{ConfirmationModal, DialogButtons, Heading, Placeholder};
use samba_shared::{playlists::Playlist, utils::format_duration_millis};

use super::super::{
    backend::{delete_session, fetch_practice_sessions},
    navigation::PracticeAction,
};
use crate::{util::refresh_page, views::PleaseWaitView, AppRoutes};

#[component]
pub fn PracticeListView() -> impl IntoView {
    // TODO: use Resource::new with serial types?
    let sessions = create_local_resource(
        || (),
        move |_| async move { fetch_practice_sessions().await.unwrap_or_default() },
    );

    view! {
        <section>
            <Suspense fallback=|| {
                view! { <PleaseWaitView /> }
            }>
                {move || {
                    sessions().map(|sessions| view! { <PracticeListHandler sessions=sessions /> })
                }}
            </Suspense>
        </section>
    }
}

#[component]
fn PracticeListHandler(sessions: Vec<PlaylistEntityWithEntries>) -> impl IntoView {
    let (sessions, _) = create_signal(sessions);

    view! {
        <section>
            <Show
                when=move || !sessions().is_empty()
                fallback=|| {
                    view! {
                        <Heading>"Your practice sessions"</Heading>
                        <Placeholder
                            icon=view! {
                                <span
                                    class="fa-solid fa-stop-circle"
                                    style="font-size: 500%;"
                                ></span>
                            }
                                .into_view()
                            title="There are no practice sessions".into_view()
                            subtitle="Up to now you did not create or save any sessions."
                                .into_view()
                            commands=vec![
                                view! {
                                    <div class="btn-container">
                                        <a
                                            class="btn btn-primary u-inline-block"
                                            href=AppRoutes::Practice(PracticeAction::Edit(None))
                                        >
                                            "Create a new practice session"
                                        </a>
                                    </div>
                                }
                                    .into_view(),
                            ]
                        />
                    }
                }
            >
                <div class="flex gap-2 mb-4">
                    <Heading classes="grow".into()>"Your practice sessions"</Heading>
                    <a class="btn" href=AppRoutes::Practice(PracticeAction::History)>
                        "History"
                    </a>
                    <a class="btn btn-primary" href=AppRoutes::Practice(PracticeAction::Edit(None))>
                        "New"
                    </a>
                </div>
                <table class="table borderless striped u-text-left">
                    <thead>
                        <tr>
                            <th>"Title"</th>
                            <th>"Tags"</th>
                            <th>"Creator"</th>
                            <th>"Length"</th>
                            <th>"Actions"</th>
                        </tr>
                    </thead>
                    <tbody>
                        <For each=sessions key=|x| x.hdr.uid.clone() let:pwe>
                            <SessionRow session=pwe />
                        </For>
                    </tbody>
                </table>
            // TODO: pagination
            </Show>
        </section>
    }
}

#[component]
fn SessionRow(session: PlaylistEntityWithEntries) -> impl IntoView {
    let app_state =
        use_context::<AppState>().expect("Sessions are only shown when logged in and initialized.");

    let title = session.body.playlist.title.clone();
    let (session_id, _) = create_signal(session.hdr.uid.to_string());
    let playlist: Playlist = session.body.clone().into();

    let playlist_length_estimate = playlist.estimate_length();
    let length_estimate = format!(
        "{}{}",
        if playlist_length_estimate.is_estimate {
            "≥ "
        } else {
            ""
        },
        format_duration_millis(playlist_length_estimate.duration.as_millis())
    );

    let delete_dialog_open = RwSignal::new(false);
    let (deletion_confirmed, set_deletion_confirmed) = create_signal(None);
    let delete_action = move |_| {
        delete_dialog_open.set(true);
    };
    Effect::new(move |_| {
        if deletion_confirmed() == Some(true) {
            // TODO: error handling
            spawn_local(async move {
                delete_session(&session_id()).await.unwrap();
                refresh_page();
            });
        }
    });

    view! {
        <tr>
            <td>
                <a href=AppRoutes::Practice(
                    PracticeAction::Saved(session_id().clone(), None),
                )>{title.clone()}</a>
            </td>
            <td>"TODO: Tags"</td>
            <td>"TODO: Creator"</td>
            <td>{length_estimate}</td>
            <td class="flex gap-2">
                <span class="tooltip" data-tip="Play">
                    <a
                        class="btn btn-primary"
                        href=AppRoutes::Practice(PracticeAction::Saved(session_id().clone(), None))
                            .to_string()
                        role="button"
                    >
                        <span class="fa-regular fa-circle-play"></span>
                    </a>
                </span>
                <span class="tooltip" data-tip="Edit">
                    <a
                        class="btn"
                        href=AppRoutes::Practice(PracticeAction::Edit(Some(session_id().clone())))
                        role="button"
                    >
                        <span class="fa-solid fa-pen"></span>
                    </a>
                </span>
                <span class="tooltip" data-tip="Export PDF">
                    <a
                        class="btn"
                        download=format!("{title}.pdf")
                        href=format!("{}/playlists/{}/export", app_state.backend_url, session_id())
                        rel="external"
                        role="button"
                    >
                        <span class="fa-regular fa-file-pdf"></span>
                    </a>
                </span>
                <button
                    class="btn btn-error tooltip"
                    data-tip="Delete"
                    on:click=delete_action
                    type="button"
                >
                    <span class="fa-regular fa-trash-can"></span>
                </button>
            </td>
        </tr>
        <ConfirmationModal
            open=delete_dialog_open
            confirmation=set_deletion_confirmed
            buttons=DialogButtons::YesNo
        >
            "Do you really want to delete the session “"
            {title}
            "”?"
        </ConfirmationModal>
    }
}
