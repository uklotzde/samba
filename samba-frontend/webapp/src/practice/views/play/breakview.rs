use leptos::*;
use samba_frontend_uicomponents::LRTileCard;

use super::PlayState;

#[component]
pub fn BreakView(active: RwSignal<PlayState>) -> impl IntoView {
    let break_text = move || {
        if active() != PlayState::Finished {
            "Break (waiting for your signal)"
        } else {
            "Break (continuation confirmed)"
        }
    };

    view! {
        <LRTileCard
            left=view! { <span class="font-bold">{break_text}</span> }.into_view()
            right=(active() != PlayState::Finished)
                .then(|| {
                    view! {
                        <button
                            class="btn"
                            disabled=move || active() != PlayState::Playing
                            on:click=move |_| {
                                active.set(PlayState::Finished);
                            }
                            type="button"
                        >
                            <span class="fa-solid fa-forward" />
                        </button>
                    }
                })
                .into_view()
        />
    }
}
