use leptos::*;

use super::super::super::PracticeAction;
use crate::AppRoutes;

pub const PRACTICE_SESSION_LINK_NOTE: &str = "practice-session";

#[component]
pub fn NotesView(notes: String) -> impl IntoView {
    // Find a potential backreference to a playlist and sanitize notes from
    // it (simply remove the reference; it will be presented separately).
    let original_playlist = notes
        .find(PRACTICE_SESSION_LINK_NOTE)
        .and_then(|index| notes[index..].find(':'))
        .map(|index| {
            let id = notes[index..]
                .find(char::is_whitespace)
                .map_or(&notes[index + 1..], |end| &notes[index + 1..end]);
            (
                id.to_string(),
                notes.replace(&format!("{PRACTICE_SESSION_LINK_NOTE}:{id}"), ""),
            )
        });

    if let Some((playlist_id, remaining_notes)) = original_playlist {
        view! {
            <p>
                <small>
                    "Click "
                    <a
                        class="link"
                        href=AppRoutes::Practice(PracticeAction::Saved(playlist_id.clone(), None))
                    >
                        "here"
                    </a> " to play a new session of this playlist."
                </small>
                <br />
                <small>{remaining_notes}</small>
            </p>
        }
        .into_view()
    } else {
        view! {
            <p>
                <small>{notes}</small>
            </p>
        }
        .into_view()
    }
}
