use std::time::Duration;

use gloo_timers::future::TimeoutFuture;
use leptos::*;
use samba_frontend_uicomponents::LRTileCard;

use super::PlayState;

#[component]
pub fn PauseView(active: RwSignal<PlayState>, duration: RwSignal<Duration>) -> impl IntoView {
    let counting = RwSignal::new(false);
    Effect::new(move |_| {
        if active() == PlayState::Playing {
            counting.set(true);
        } else {
            counting.set(false);
        }
    });
    Effect::new(move |_| {
        counting.track();
        spawn_local(async move {
            while counting() && duration().as_secs() > 0 {
                TimeoutFuture::new(1000).await;
                duration.set(duration().saturating_sub(Duration::from_secs(1)));
            }
            if counting() {
                TimeoutFuture::new(duration().as_millis() as u32).await;
                duration.set(Duration::ZERO);
                active.set(PlayState::Finished);
            }
        });
    });

    let duration_representation = move || {
        let duration = duration().as_secs();
        format!(
            "Pause ({})",
            if duration > 0 {
                format!("{duration} seconds remaining")
            } else {
                "time is up".to_string()
            }
        )
    };

    view! {
        <LRTileCard
            left=view! { <span class="font-bold">{duration_representation}</span> }.into_view()
            right=(active() != PlayState::Finished)
                .then(|| {
                    view! {
                        <div class="join">
                            <button
                                class="btn join-item"
                                disabled=move || active() != PlayState::Playing
                                on:click=move |_| {
                                    counting.set(!counting());
                                }
                                type="button"
                            >
                                <span
                                    class="fa-solid"
                                    class:fa-pause=counting
                                    class:fa-play=move || !counting()
                                />
                            </button>
                            <button
                                class="btn join-item"
                                disabled=move || active() != PlayState::Playing
                                on:click=move |_| {
                                    counting.set(false);
                                    duration.set(Duration::ZERO);
                                    active.set(PlayState::Finished);
                                }

                                type="button"
                            >
                                <span class="fa-solid fa-forward" />
                            </button>
                        </div>
                    }
                })
                .into_view()
        />
    }
}
