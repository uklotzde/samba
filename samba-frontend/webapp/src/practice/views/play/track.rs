use std::time::Duration;

use aoide::{
    playlist::{
        Entry as AoidePlaylistEntry, Item as AoidePlaylistItem, TrackItem as AoideTrackItem,
    },
    track::tag::FACET_ID_GENRE,
    TrackEntity,
};
use gloo_timers::future::TimeoutFuture;
use leptos::*;
use samba_frontend_shared::AppState;
use samba_frontend_uicomponents::LRTileCard;
use samba_shared::{
    dancing::FACET_ID_DANCE,
    playlists::{AudioPlayback, AudioSource, PlaybackProperties, PlaylistItem},
};
use wasm_bindgen::{closure::Closure, JsCast, JsValue};
use web_sys::{Event, HtmlAudioElement};

use super::{super::super::backend::fetch_random_track_for_query, PlayState};
use crate::aoide::join_tags_for_facet;

#[derive(Clone)]
pub struct TrackContext {
    pub query: String,
    pub properties: PlaybackProperties,
}

#[component]
pub fn TrackView(
    active: RwSignal<PlayState>,
    history_entry: RwSignal<AoidePlaylistEntry>,
) -> impl IntoView {
    let TrackContext { query, .. } = use_context::<TrackContext>()
        .expect("Track view can only be called with appropriate context.");
    // TODO: non-local with serial types?
    let track = create_local_resource(
        || (),
        move |_| {
            let query = query.clone();
            async move {
                fetch_random_track_for_query(&query)
                    .await
                    .inspect_err(|e| log::error!("Could not fetch random track: {e:?}"))
                    .map_err(|_ignore| ())
            }
        },
    );

    view! {
        <Suspense fallback=|| {
            view! {
                <LRTileCard
                    left=view! {
                        <div class="flex gap-4 items-center">
                            <div class="skeleton w-16 h-16 rounded-full shrink-0"></div>
                            <div class="flex flex-col gap-4">
                                <div class="skeleton h-4 w-20"></div>
                                <div class="skeleton h-4 w-28"></div>
                            </div>
                        </div>
                    }
                        .into_view()
                    right=view! { <div class="loading loading-spinner loading-lg"></div> }
                        .into_view()
                />
            }
        }>
            {move || {
                track()
                    .map(|track| {
                        track
                            .map_or_else(
                                |_| {
                                    Effect::new(move |_| {
                                        if active() == PlayState::Playing {
                                            active.set(PlayState::Finished);
                                        }
                                    });

                                    let TrackContext { query, .. } = use_context::<TrackContext>()
                                        .expect("Track view can only be called with appropriate context.");
                                    view! {
                                        // There is nothing to play, so we are simply skipping this entry.
                                        <LRTileCard left=view! {
                                            <span class="font-bold">
                                                "No track matched your query '"
                                                {query}
                                                "', so this entry will be skipped."
                                            </span>
                                        }.into_view()/>
                                    }
                                        .into_view()
                                },
                                |entity| {
                                    if matches!(
                                        history_entry().item.clone().into(),
                                        PlaylistItem::Playback(
                                            AudioPlayback {
                                                audio_source: AudioSource::TrackQuery(_),
                                                ..
                                            },
                                        )
                                    ) {
                                        history_entry
                                            .update(|entry| {
                                                entry
                                                    .item = AoidePlaylistItem::Track(AoideTrackItem {
                                                    uid: entity.hdr.uid.clone(),
                                                });
                                            });
                                    }
                                    view! {
                                        <TrackPlayerView
                                            active=active
                                            entity=entity
                                        />
                                    }
                                        .into_view()
                                },
                            )
                    })
            }}

        </Suspense>
    }
}

#[component]
fn TrackPlayerView(active: RwSignal<PlayState>, entity: TrackEntity) -> impl IntoView {
    let TrackContext { query, properties } = use_context::<TrackContext>()
        .expect("Track player can only be called with appropriate context.");
    let app_state =
        use_context::<AppState>().expect("Tracks are only fetched when logged in and initialized.");
    let (file_url, _) = create_signal(format!(
        "{}/tracks/{}/file",
        &app_state.backend_url, entity.hdr.uid
    ));

    let audio_node = create_node_ref::<leptos::html::Audio>();
    let playing = RwSignal::new(false);
    let playback_finished = RwSignal::new(false);

    // On finishing the playback, the next track shall trigger and this track
    // shall be guaranteed to be silent.
    Effect::new(move |_| {
        if playback_finished() {
            playing.set(false);
            active.set(PlayState::Finished);
        }
    });

    // After loading (the track waveform) finished, the playback must be
    // triggered manually because WaveSurfer is not blocking and has no
    // way to interact with signals.
    Effect::new(move |_| {
        if active() == PlayState::Playing {
            playing.set(true);
        }
    });

    // Process the playback properties by setting appropriate offsets and
    // starting timers for fading and timing out.
    Effect::new(move |_| {
        // This will only trigger once.
        if active() == PlayState::Playing {
            let Some(audio_element) = audio_node.get() else {
                return;
            };

            if properties.fade_in != Duration::ZERO {
                let (duration, set_duration) = create_signal(properties.fade_in);
                let volume_step = 1.0 / (properties.fade_in.as_secs_f64() * 100.0);

                audio_element.set_volume(0.0);
                spawn_local(async move {
                    while playing() && duration().as_millis() > 0 {
                        TimeoutFuture::new(1).await;
                        set_duration(duration().saturating_sub(Duration::from_millis(1)));
                        audio_element.set_volume(audio_element.volume() + volume_step);
                    }
                    audio_element.set_volume(1.0);
                });
            }
        }
    });

    // Start the playback on state change and pause the track (if it has not
    // ended) in finish state.
    Effect::new(move |_| {
        playing.track();
        let Some(audio_element) = audio_node.get() else {
            return;
        };

        if active() == PlayState::Playing && !audio_element.paused() {
            audio_element.pause().expect("Cannot fail in JS.");
        } else if active() == PlayState::Playing && !audio_element.ended() {
            audio_element.set_current_time(
                properties
                    .start_at
                    .as_secs_f64()
                    .max(audio_element.current_time()),
            );
            let _ = audio_element.play().expect("Cannot fail in JS.");
        } else if active() == PlayState::Finished && !audio_element.paused() {
            audio_element.pause().expect("Cannot fail in JS.");
        }
    });

    let dances = join_tags_for_facet(&entity.body.track, FACET_ID_DANCE).unwrap_or_else(|| {
        join_tags_for_facet(&entity.body.track, FACET_ID_GENRE)
            .unwrap_or_else(|| "no dance specified".into())
    });

    let title_segment = format!(
        "{} ({}; {}{})",
        entity.body.track.track_title().unwrap_or("No Title"),
        dances,
        entity.body.track.track_artist().unwrap_or_else(|| entity
            .body
            .track
            .album_artist()
            .unwrap_or("no artist")),
        entity
            .body
            .track
            .album_title()
            .map(|title| format!("; {title}"))
            .unwrap_or_default()
    );

    // On registering the HTML element for the audio node, register event
    // listeners. Usually a job for on_mount but that does not work here
    // because the custom element does not return a HTMLElement.
    audio_node.on_load(move |audio_element| {
        // Add the appropriate event listeners to the audio element and load
        // the track.
        let (finish_closure, _) = create_signal(Closure::once_into_js(move || {
            playback_finished.set(true);
        }));
        audio_element.set_onended(Some(finish_closure.get().unchecked_ref()));

        let (on_time_update, _) = create_signal(
            Closure::wrap(Box::new(move |ev: &JsValue| {
                let Some(audio_element) = ev
                    .clone()
                    .unchecked_into::<Event>()
                    .target()
                    .map(|node| node.unchecked_into::<HtmlAudioElement>())
                else {
                    return;
                };

                // TODO: fade out
                if Duration::from_secs_f64(audio_element.duration())
                    .saturating_sub(properties.start_at + properties.duration)
                    == Duration::ZERO
                    && !playback_finished()
                {
                    playback_finished.set(true);
                }
            }) as Box<dyn Fn(&JsValue)>)
            .into_js_value(),
        );
        audio_element.set_ontimeupdate(Some(on_time_update.get().unchecked_ref()));
    });

    view! {
        <LRTileCard
            left=view! {
                <p class="font-bold mb-4">
                    {title_segment}
                    <span class="text-gray-300 ml-2">
                        <span
                            class="fa-regular fa-circle-question tooltip"
                            data-tip=format!("randomly selected from tracks matching '{query}'")
                        ></span>
                    </span>
                </p>
                <audio
                    autoplay=false
                    class="w-100p"
                    controls=true
                    preload="auto"
                    ref=audio_node
                    src=file_url
                    // Cirrus' classes do not work, so we have to force the style
                    // attributes.
                    style="height:2rem!important"
                >
                    <source src=file_url />
                    "samba requires a HTML5 compatible browser for playback"
                </audio>
            }
                .into_view()
            right=(active() != PlayState::Finished)
                .then(|| {
                    view! {
                        <div class="join">
                            <button
                                class="btn join-item"
                                disabled=move || active() != PlayState::Playing
                                on:click=move |_| {
                                    playing.set(!playing());
                                }
                                type="button"
                            >
                                <span
                                    class="fa-solid"
                                    class:fa-pause=playing
                                    class:fa-play=move || !playing()
                                />
                            </button>
                            <button
                                class="btn join-item"
                                disabled=move || active() != PlayState::Playing
                                on:click=move |_| {
                                    playing.set(false);
                                    active.set(PlayState::Finished);
                                }
                                type="button"
                            >
                                <span class="fa-solid fa-forward" />
                            </button>
                        </div>
                    }
                })
                .into_view()
        />
    }
}
