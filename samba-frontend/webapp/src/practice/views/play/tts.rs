use leptos::*;
use samba_frontend_shared::AppState;
use samba_frontend_uicomponents::LRTileCard;
use tts::Tts;

use super::PlayState;

#[component]
pub fn TtsView(active: RwSignal<PlayState>, text: String) -> impl IntoView {
    let app_state = use_context::<AppState>()
        .expect("Playlists are only played when logged in and initialized.");

    let mut tts_available = false;
    let mut tts = Tts::default().ok();
    if let Some(ref mut tts) = tts {
        let tts_voices = tts
            .voices()
            .expect("If the browser supports TTS, we assume it supports fetching voices.");
        if let Some(voice) = app_state
            .user_preferences
            .get()
            .preferred_tts_voice
            .as_ref()
            .and_then(|voice_name| tts_voices.iter().find(|voice| &voice.name() == voice_name))
            .or_else(|| tts_voices.first())
        {
            if tts.set_voice(voice).is_ok() {
                tts_available = true;
            }
        }
    }
    let (tts, _) = create_signal(if tts_available { tts } else { None });

    let text_to_speak = text.clone();
    Effect::new(move |_| {
        if active() == PlayState::Playing {
            if let Some(ref mut tts) = tts() {
                // If there is a device to speak text, speak, otherwise simply
                // skip everything.
                tts.speak(&text_to_speak, false).unwrap();
            }
            active.set(PlayState::Finished);
        } else if active() == PlayState::Finished && tts_available {
            // Abort current speaking if there is some skip or stop happening.
            if let Some(ref mut tts) = tts() {
                tts.stop().unwrap();
            }
        }
    });

    view! {
        <LRTileCard left=view! {
            <p>
                <span class="font-bold">"samba speaking:"</span>
                " “"
                {text}
                "”"
                {(!tts_available)
                    .then(|| {
                        view! {
                            <span class="text-gray-300 ml-2">
                                <span
                                    class="fa-regular fa-circle-question tooltip"
                                    data-tip=format!("no TTS engine (will be skipped)")
                                ></span>
                            </span>
                        }
                    })}

            </p>
        }
            .into_view() />
    }
}
