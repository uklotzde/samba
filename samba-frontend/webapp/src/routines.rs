mod backend;
mod navigation;
mod views;

use samba_shared::routines::RoutineElement;
use uuid::Uuid;

pub use self::{navigation::RoutineAction, views::*};

#[derive(Clone, Debug, PartialEq)]
struct RoutineItem {
    uuid: Uuid,
    item: RoutineElement,
}

impl RoutineItem {
    fn new(item: RoutineElement) -> Self {
        Self {
            uuid: Uuid::new_v4(),
            item,
        }
    }
}
