mod details;
mod list;
mod update;

use samba_shared::routines::StepTiming;

pub use self::{details::RoutineDetailsView, list::RoutineListView, update::RoutineUpdateView};

/// String representation of a step timing, to be used for step timings
/// displayed on their own (not grouped).
fn step_timing_to_string(timing: StepTiming) -> String {
    match timing {
        StepTiming::A => "“a”".into(),
        StepTiming::And => "“and” (&)".into(),
        StepTiming::Quick => "quick".into(),
        StepTiming::Slow => "slow".into(),
        StepTiming::Custom(semiquavers) => format!("{semiquavers} 𝅘𝅥𝅯"),
    }
}
