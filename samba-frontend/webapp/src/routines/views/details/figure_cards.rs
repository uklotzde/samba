use leptos::*;
use samba_shared::routines::{
    timing_summary_for_routine, Figure, LevelRequirement, RoutineElement,
};

use super::step_cards::StepCardContent;

#[component]
pub fn FigureCardContent(figure: Figure) -> impl IntoView {
    let (leaders_steps, followers_steps) = figure.steps();
    let timing_summary = timing_summary_for_routine(vec![RoutineElement::Figure(figure.clone())]);
    let Figure {
        title,
        start,
        end,
        notes,
        level_requirement,
        ..
    } = figure.clone();

    let leader_step_count = leaders_steps.len();
    let follower_step_count = followers_steps.len();
    let (leaders_step_view, _) = create_signal(
        leaders_steps
            .iter()
            .cloned()
            .map(|step| {
                view! {
                    <li class="mt-2 mb-1">
                        <StepCardContent step=step hide_follower=true />
                    </li>
                }
            })
            .collect::<Vec<_>>(),
    );
    let (followers_step_view, _) = create_signal(
        followers_steps
            .iter()
            .cloned()
            .map(|step| {
                view! {
                    <li class="mt-2 mb-1">
                        <StepCardContent step=step hide_leader=true />
                    </li>
                }
            })
            .collect::<Vec<_>>(),
    );

    let (collapsed, set_collapsed) = create_signal(true);
    let expandable = leader_step_count + follower_step_count > 0 || !notes.is_empty();

    view! {
        <div
            class="flex gap-4 cursor-pointer"
            on:click=move |_| {
                set_collapsed(!collapsed());
            }
        >
            <span class="font-bold mr-2">{title}</span>
            <Show when=move || { level_requirement != LevelRequirement::ToBeDetermined }>
                <div>
                    <span class="fa-solid fa-layer-group mr-2" />
                    {match level_requirement {
                        LevelRequirement::ToBeDetermined => "tbd",
                        LevelRequirement::WdsfDCSyllabus => "D/C class",
                        LevelRequirement::WdsfBSyllabus => "B class",
                        LevelRequirement::Open => "open class",
                    }}
                </div>
            </Show>
            {(!timing_summary.is_empty())
                .then(|| {
                    view! {
                        <div>
                            <span class="fa-solid fa-clock mr-2" />
                            <span class="tooltip" data-tip="Leader's timing">
                                {timing_summary}
                            </span>
                        </div>
                    }
                })}
            {start
                .map(|s| {
                    view! {
                        <div>
                            <span class="fa-solid fa-circle-chevron-up mr-2" />
                            {s}
                        </div>
                    }
                })}
            {end
                .map(|e| {
                    view! {
                        <div>
                            <span class="fa-solid fa-circle-chevron-down mr-2" />
                            {e}
                        </div>
                    }
                })}
            <Show when=move || expandable>
                <span class="grow" />
                <span
                    class="mr-2 fa-solid"
                    class:fa-angle-down=collapsed
                    class:fa-angle-up=move || !collapsed()
                />
            </Show>
        </div>
        <Show when=move || !collapsed()>
            <Show when=move || { leader_step_count + follower_step_count > 0 }>
                <div class="flex gap-4">
                    <div class="grow">
                        <span class="font-light">"Leader's steps"</span>
                        <Show
                            when=move || { leader_step_count > 0 }
                            fallback=|| view! { <p>"No steps for the leader."</p> }
                        >
                            <ul class="ml-4 list-decimal">{leaders_step_view}</ul>
                        </Show>
                    </div>
                    <div class="grow">
                        <span class="font-light">"Follower's steps"</span>
                        <Show
                            when=move || { follower_step_count > 0 }
                            fallback=|| view! { <p>"No steps for the follower."</p> }
                        >
                            <ul class="ml-4 list-decimal">{followers_step_view}</ul>
                        </Show>
                    </div>
                </div>
            </Show>
            {(!notes.is_empty())
                .then(|| {
                    view! {
                        <div class="mt-2">
                            <span class="font-bold">"Notes:"</span>
                            <ul>
                                {notes
                                    .iter()
                                    .map(|note| view! { <li>{note}</li> })
                                    .collect::<Vec<_>>()}
                            </ul>
                        </div>
                    }
                })}
        </Show>
    }
}
