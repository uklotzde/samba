use leptos::*;
use samba_shared::routines::{Foot, Step, WeightTransfer};

use super::super::step_timing_to_string;

#[component]
pub fn StepCardContent(
    step: Step,
    #[prop(optional)] bold_face: bool,
    #[prop(optional)] hide_leader: bool,
    #[prop(optional)] hide_follower: bool,
) -> impl IntoView {
    let Step {
        foot,
        timing,
        weight_transfer,
        comment,
    } = step;

    view! {
        <span>
            <span class:font-bold=bold_face>"Step"</span>
            {comment.map(|c| format!(" ({c})"))}
        </span>
        <div class="flex gap-4">
            <div>
                <span class="fa-solid fa-clock mr-2" />
                {step_timing_to_string(timing)}
            </div>
            <Show when=move || !hide_leader>
                <div>
                    <span class="fa-solid fa-person mr-2" />
                    {if foot == Foot::Left { "left" } else { "right" }}
                    " foot"
                </div>
            </Show>
            <Show when=move || !hide_follower>
                <div>
                    <span class="fa-solid fa-person-dress mr-2" />
                    {if foot == Foot::Left { "right" } else { "left" }}
                    " foot"
                </div>
            </Show>
            <div>
                <span class="fa-solid fa-weight-hanging mr-2" />
                {match weight_transfer {
                    WeightTransfer::None => "none".into(),
                    WeightTransfer::Half => "half".into(),
                    WeightTransfer::Full => "full".into(),
                    WeightTransfer::Custom(percent) => format!("{percent} %"),
                }}
            </div>
        </div>
    }
}
