use leptos::*;
use leptos_router::*;
use samba_frontend_uicomponents::{Heading, Placeholder, Space};
use samba_shared::routines::{Routine, StoredRoutine};

use super::super::{
    backend::{create_routine, fetch_routine, update_routine},
    navigation::{RoutineAction, RoutineId},
    RoutineItem,
};
use crate::{views::PleaseWaitView, AppRoutes};

mod routineitem;
use routineitem::RoutineItemView;
mod updatemodal;
use updatemodal::RoutineItemUpdateModal;

#[component]
pub fn RoutineUpdateView() -> impl IntoView {
    let routine_id = use_params::<RoutineId>();
    // TODO: error handling if routine not found
    let routine = Resource::new(routine_id, move |_| async move {
        fetch_routine(routine_id.get().ok()?.id).await.ok()
    });

    view! {
        <section>
            <Suspense fallback=|| {
                view! { <PleaseWaitView /> }
            }>
                {move || { routine().map(|rtn| view! { <RoutineUpdateHandler routine=rtn /> }) }}
            </Suspense>
        </section>
    }
}

#[component]
fn RoutineUpdateHandler(routine: Option<StoredRoutine>) -> impl IntoView {
    let routine_id = routine.as_ref().map_or(0, |r| r.identifier);
    let (routine, _) = create_signal(routine.map(|r| r.value));
    let is_update = move || routine().is_some();

    let routine_items = RwSignal::new(
        routine()
            .map(|rtn| {
                rtn.steps
                    .iter()
                    .map(|elem| RoutineItem::new(elem.clone()))
                    .collect::<Vec<_>>()
            })
            .unwrap_or_default(),
    );

    let (title, set_title) = create_signal(
        routine()
            .as_ref()
            .and_then(|rtn| rtn.title.clone())
            .unwrap_or_default(),
    );
    let (choreographer, set_choreographer) = create_signal(
        routine()
            .as_ref()
            .and_then(|rtn| rtn.choreographer.clone())
            .unwrap_or_default(),
    );
    let (description, set_description) = create_signal(
        routine()
            .as_ref()
            .and_then(|rtn| rtn.description.clone())
            .unwrap_or_default(),
    );
    let (notes, set_notes) = create_signal(
        routine()
            .as_ref()
            .and_then(|rtn| rtn.notes.clone())
            .unwrap_or_default(),
    );

    let (performers, set_performers) = create_signal(
        routine()
            .as_ref()
            .map(|rtn| rtn.performers.clone())
            .unwrap_or_default(),
    );
    let performer_display = move || performers().join(", ");

    let submit_action = move |_| {
        let mut routine = routine().clone().unwrap_or_else(Routine::empty);
        routine.title = (!title().is_empty()).then(title);
        routine.choreographer = (!choreographer().is_empty()).then(choreographer);
        routine.description = (!description().is_empty()).then(description);
        routine.notes = (!notes().is_empty()).then(notes);
        routine.performers = performers()
            .iter()
            .map(|p| p.trim())
            .map(ToOwned::to_owned)
            .collect::<Vec<_>>();
        routine.steps = routine_items()
            .iter()
            .map(|item| item.item.clone())
            .collect();

        spawn_local(async move {
            // TODO: error handling
            let id = if !is_update() {
                create_routine(routine).await.unwrap()
            } else {
                update_routine(routine_id, routine).await.unwrap();
                routine_id
            };

            use_navigate()(
                &AppRoutes::Routines(RoutineAction::View(id)).to_string(),
                Default::default(),
            );
        });
    };

    let show_add_modal = RwSignal::new(false);
    let add_item = move |_| {
        if show_add_modal() {
            show_add_modal.set(false);
        }
        show_add_modal.set(true);
    };

    view! {
        <div class="flex gap-2 mb-8 items-center">
            <Heading classes="grow".into()>
                {move || {
                    if let Some(rtn) = routine().as_ref() {
                        if let Some(ref title) = rtn.title {
                            format!("Updating routine “{title}”")
                        } else {
                            "Updating routine".to_string()
                        }
                    } else {
                        "Creating new routine".to_string()
                    }
                }}
            </Heading>
            <div class="ml-2">
                <button type="button" class="btn btn-primary" on:click=add_item>
                    "Add item"
                </button>
            </div>
        </div>
        <form action="javascript:void(0);" method="post" on:submit=submit_action>
            // TODO: dance vs. track
            // TODO: small screen layout
            <div class="md:grid md:grid-cols-5 md:gap-y-6 items-center">
                <label class="m-0 md:col-span-1" for="rtn-title">
                    "Title:"
                </label>
                <input
                    class="input input-bordered py-3 px-4 w-full md:col-span-4 block"
                    id="rtn-title"
                    placeholder="My favorite routine"
                    type="text"
                    on:input=move |ev| {
                        set_title(event_target_value(&ev));
                    }
                    prop:value=title
                />
                <label class="m-0 md:col-span-1" for="rtn-choreographer">
                    "Choreographer:"
                </label>
                <input
                    class="input input-bordered py-3 px-4 w-full md:col-span-4 block"
                    id="rtn-choreographer"
                    placeholder="My coach"
                    type="text"
                    on:input=move |ev| {
                        set_choreographer(event_target_value(&ev));
                    }
                    prop:value=choreographer
                />
                <label class="m-0 md:col-span-1" for="rtn-desc">
                    "Description:"
                </label>
                <input
                    class="input input-bordered py-3 px-4 w-full md:col-span-4 block"
                    id="rtn-desc"
                    placeholder="Additional details"
                    type="text"
                    on:input=move |ev| {
                        set_description(event_target_value(&ev));
                    }
                    prop:value=description
                />
                <label class="m-0 md:col-span-1 md:self-start md:py-3" for="rtn-notes">
                    "Notes:"
                </label>
                <textarea
                    class="input input-bordered py-3 px-4 w-full md:col-span-4 block"
                    id="rtn-notes"
                    placeholder="Notes"
                    on:input=move |ev| {
                        set_notes(event_target_value(&ev));
                    }
                    prop:value=notes
                />
                <label class="m-0 md:col-span-1 md:self-start md:py-3" for="rtn-perf">
                    "Performers:"
                </label>
                <input
                    class="input input-bordered py-3 px-4 w-full md:col-span-4 block"
                    id="rtn-perf"
                    placeholder="Max and Maria, Anna and Anton, …"
                    on:input=move |ev| {
                        set_performers(event_target_value(&ev).split(',').map(|p| p.trim_start()).map(ToOwned::to_owned).collect::<Vec<_>>());
                    }
                    prop:value=performer_display
                />
                // TODO: demonstrations
            </div>
            <Space/>
            <Show
                when=move || !routine_items().is_empty()
                fallback=|| {
                    view! {
                        <Placeholder
                            icon=view! {
                                <span
                                    class="fa-solid fa-stop-circle"
                                    style="font-size: 500%;"
                                ></span>
                            }
                                .into_view()
                            title="There are no items yet".into_view()
                            subtitle="Up to now you did not add any item. Empty routines are not allowed."
                                .into_view()
                            commands=vec![
                                view! {
                                    <div class="btn-container">
                                        // TODO: on_click add_item
                                        <button type="button" class="btn btn-secondary">
                                            "Add a first item to the routine"
                                        </button>
                                    </div>
                                }
                                    .into_view(),
                            ]
                        />
                    }
                }
            >
                <For
                    each=routine_items
                    key=|x| x.uuid
                    children=move |item: RoutineItem| {
                        view! { <RoutineItemView item=item routine_items=routine_items /> }
                    }
                />
                <Space />
                <div class="flex justify-end">
                    <button type="submit" class="btn btn-secondary">
                        "Submit"
                    </button>
                </div>
            </Show>
        </form>
        <RoutineItemUpdateModal modal_visibility=show_add_modal routine_items=routine_items />
    }
}
