use leptos::*;
use samba_frontend_uicomponents::DropdownSelect;
use samba_shared::routines::{
    Figure, Foot, LevelRequirement, RoutineElement, Step, StepTiming, WeightTransfer,
};
use uuid::Uuid;

use super::super::{super::RoutineItem, step_timing_to_string};

#[component]
pub fn RoutineItemUpdateModal(
    modal_visibility: RwSignal<bool>,
    routine_items: RwSignal<Vec<RoutineItem>>,
    #[prop(optional)] current_routine_item: Option<RoutineItem>,
) -> impl IntoView {
    let is_update = current_routine_item.is_some();

    let (item_to_update, _) = create_signal(current_routine_item.as_ref().map(|item| item.uuid));
    let submit_value = RwSignal::new(current_routine_item.clone());
    let submit_action = move |_| {
        // TODO: invalidity warning?
        if let Some(value) = submit_value.get().as_ref() {
            routine_items.update(|items| {
                if let Some(pos) = items
                    .iter()
                    .position(|element| element.uuid == item_to_update().unwrap_or(value.uuid))
                {
                    items[pos] = value.clone();
                } else {
                    items.push(value.clone());
                }
            });
        }

        // Update the submit value to allow for multiple insertions of the same
        // value consecutively.
        submit_value.update(|session_item| {
            if let Some(item) = session_item.as_mut() {
                item.uuid = Uuid::new_v4();
            }
        });
        modal_visibility.set(false);
    };

    // TODO: factor out into ToString implementation?
    let item_type = RwSignal::new(
        current_routine_item
            .as_ref()
            .map(|item| match &item.item {
                RoutineElement::Figure(_) => "Figure",
                RoutineElement::Step(_) => "Step",
                RoutineElement::Pause(_) => "Pause",
                RoutineElement::Stop => "Stop",
                RoutineElement::Repeat(_) => "Repeat",
                RoutineElement::Separator(_) => "Separator",
            })
            .map(ToString::to_string)
            .unwrap_or_default(),
    );
    let (valid_item_types, _) = create_signal(vec![
        "Figure".into(),
        "Step".into(),
        "Separator".into(),
        "Pause".into(),
        "Repeat".into(),
        "Stop".into(),
    ]);

    let type_specific_input = RwSignal::new(View::default().into_view());
    Effect::new(move |_| {
        type_specific_input.set(match item_type().as_str() {
            "Figure" => {
                let initialization_figure = current_routine_item
                    .as_ref()
                    .and_then(|item| {
                        if let RoutineElement::Figure(ref fig) = item.item {
                            Some(fig.clone())
                        } else {
                            None
                        }
                    })
                    .unwrap_or_else(|| Figure::new(String::new()));
                let (init_leaders_steps, init_followers_steps) = initialization_figure.steps();

                let (title, set_title) = create_signal(initialization_figure.title.clone());
                let (start, set_start) =
                    create_signal(initialization_figure.start.clone().unwrap_or_default());
                let (end, set_end) =
                    create_signal(initialization_figure.end.clone().unwrap_or_default());
                let (notes, set_notes) = create_signal(initialization_figure.notes.clone());

                let level_req = RwSignal::new(initialization_figure.level_requirement);
                let leaders_steps = RwSignal::new(init_leaders_steps.to_vec());
                let followers_steps = RwSignal::new(init_followers_steps.to_vec());
                let step_timings_mismatch = Signal::derive(move || {
                    leaders_steps()
                        .iter()
                        .map(|step| step.timing.to_semiquavers())
                        .sum::<usize>()
                        != followers_steps()
                            .iter()
                            .map(|step| step.timing.to_semiquavers())
                            .sum::<usize>()
                });

                Effect::new(move |_| {
                    let mut figure = Figure::new(title()).with_level_requirement(level_req());
                    if !start().is_empty() {
                        figure = figure.with_start_comment(start());
                    }
                    if !end().is_empty() {
                        figure = figure.with_end_comment(end());
                    }
                    if !notes().is_empty() {
                        figure = figure.with_notes(notes());
                    }

                    let Some(figure) = figure.with_steps(leaders_steps(), followers_steps()) else {
                        return;
                    };
                    // While the step initialization of the figure should already check
                    // the absence of a timing mismatch, we check again here to avoid
                    // relying on implementation details.
                    if !step_timings_mismatch() {
                        submit_value.set(Some(RoutineItem::new(RoutineElement::Figure(figure))));
                    }
                });

                view! {
                    <div class="flex gap-2 items-center">
                        <label for="item-title">"Title:"</label>
                        <input
                            class="input input-bordered grow"
                            id="item-title"
                            placeholder="Natural Turn"
                            type="text"
                            on:input=move |ev| {
                                set_title(event_target_value(&ev));
                            }
                            prop:value=title
                        />
                    </div>
                    <div class="mt-2">
                        <LevelRequirementInput text="Required skills" signal=level_req />
                    </div>
                    <div class="flex gap-4 mt-6 mb-8 items-start">
                        <div class="grow">
                            <EditableSteps
                                heading="Leader's steps"
                                invalid_timing=step_timings_mismatch
                                invalid_timing_error="Timing does not match follower's"
                                signal=leaders_steps
                            />
                        </div>
                        <div class="grow">
                            <EditableSteps
                                heading="Follower's steps"
                                invalid_timing=step_timings_mismatch
                                invalid_timing_error="Timing does not match leader's"
                                signal=followers_steps
                            />
                        </div>
                    </div>
                    <div class="flex gap-2 items-center">
                        <label for="item-start">"Start:"</label>
                        <input
                            class="input input-bordered grow"
                            id="item-start"
                            placeholder="closed hold"
                            type="text"
                            on:input=move |ev| {
                                set_start(event_target_value(&ev));
                            }
                            prop:value=start
                        />
                    </div>
                    <div class="flex gap-2 my-4 items-center">
                        <label for="item-end">"End:"</label>
                        <input
                            class="input input-bordered grow"
                            id="item-end"
                            placeholder="outside partner"
                            type="text"
                            on:input=move |ev| {
                                set_end(event_target_value(&ev));
                            }
                            prop:value=end
                        />
                    </div>
                    <div class="flex gap-2 items-center">
                        <label for="item-notes" class="self-start pt-3">
                            "Notes:"
                        </label>
                        <textarea
                            class="input input-bordered grow"
                            id="item-notes"
                            placeholder="one note per line"
                            type="text"
                            on:input=move |ev| {
                                set_notes(
                                    event_target_value(&ev)
                                        .split(',')
                                        .map(ToString::to_string)
                                        .collect::<Vec<_>>(),
                                );
                            }
                            prop:value=move || notes().join(", ")
                        />
                    </div>
                }
                .into_view()
            },
            "Step" => {
                let step = RwSignal::new(
                    current_routine_item
                        .as_ref()
                        .and_then(|item| {
                            if let RoutineElement::Step(ref step) = item.item {
                                Some(step.clone())
                            } else {
                                None
                            }
                        })
                        .unwrap_or_else(|| {
                            Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::default())
                        }),
                );

                Effect::new(move |_| {
                    submit_value.set(Some(RoutineItem::new(RoutineElement::Step(step()))));
                });

                view! { <StepInput signal=step /> }.into_view()
            },
            "Separator" => {
                let (comment, set_comment) = create_signal(
                    current_routine_item
                        .as_ref()
                        .and_then(|item| {
                            if let RoutineElement::Separator(ref comment) = item.item {
                                comment.clone()
                            } else {
                                None
                            }
                        })
                        .unwrap_or_default(),
                );

                Effect::new(move |_| {
                    submit_value.set(Some(RoutineItem::new(RoutineElement::Separator(
                        (!comment().is_empty()).then(comment),
                    ))));
                });

                view! {
                    <div class="flex gap-2 items-center">
                        <label for="item-title">"Comment:"</label>
                        <input
                            class="input input-bordered grow"
                            id="item-title"
                            placeholder="…"
                            type="text"
                            on:input=move |ev| {
                                set_comment(event_target_value(&ev));
                            }
                            prop:value=comment
                        />
                    </div>
                }
                .into_view()
            },
            "Pause" => {
                let duration_signal = RwSignal::new(
                    current_routine_item
                        .as_ref()
                        .and_then(|item| {
                            if let RoutineElement::Pause(timing) = item.item {
                                Some(timing)
                            } else {
                                None
                            }
                        })
                        .unwrap_or(StepTiming::Quick),
                );

                Effect::new(move |_| {
                    submit_value.set(Some(RoutineItem::new(RoutineElement::Pause(
                        duration_signal(),
                    ))));
                });

                view! { <StepTimingInput text="Duration" signal=duration_signal /> }.into_view()
            },
            "Repeat" => {
                let duration_signal = RwSignal::new(
                    current_routine_item
                        .as_ref()
                        .and_then(|item| {
                            if let RoutineElement::Repeat(timing) = item.item {
                                Some(timing)
                            } else {
                                None
                            }
                        })
                        .unwrap_or(StepTiming::Custom(0)),
                );

                Effect::new(move |_| {
                    submit_value.set(Some(RoutineItem::new(RoutineElement::Repeat(
                        duration_signal(),
                    ))));
                });

                view! { <StepTimingInput text="Skip (from beginning)" signal=duration_signal /> }
                    .into_view()
            },
            "Stop" => {
                submit_value.set(Some(RoutineItem::new(RoutineElement::Stop)));

                View::default().into_view()
            },
            _ => View::default().into_view(),
        });
    });

    view! {
        <dialog class="modal modal-bottom sm:modal-middle" open=modal_visibility>
            <div
                class="modal-box md:flex md:flex-col md:justify-between md:h-[50%] md:min-h-[50%]"
                role="document"
            >
                <div class="flex items-center">
                    <h2 class="grow font-bold text-xl">
                        {if is_update { "Edit routine item" } else { "Add routine item" }}
                    </h2>
                    <a
                        class="btn btn-circle btn-ghost"
                        aria-label="Close"
                        on:click=move |_| {
                            modal_visibility.set(false);
                        }
                    >
                        <span class="fa-wrapper fa fa-times"></span>
                    </a>
                </div>
                <div class="flex gap-2 items-center">
                    <label for="item-type">"Type:"</label>
                    // TODO: assign ID item-type
                    <DropdownSelect
                        bind_signal=item_type
                        options=valid_item_types()
                        required=false
                        classes="grow".into()
                    />
                </div>
                <div class="my-2">{type_specific_input}</div>
                <div class="mt-auto flex gap-2 items-center justify-end">
                    <button
                        class="btn"
                        on:click=move |_| {
                            modal_visibility.set(false);
                        }
                        type="button"
                    >
                        "Cancel"
                    </button>
                    <button
                        class="btn btn-primary"
                        disabled=move || item_type.get().is_empty()
                        on:click=submit_action
                    >
                        {if is_update { "Update" } else { "Add" }}
                    </button>
                </div>
            </div>
        </dialog>
    }
}

#[component]
fn EditableSteps(
    heading: &'static str,
    invalid_timing: Signal<bool>,
    invalid_timing_error: &'static str,
    signal: RwSignal<Vec<Step>>,
) -> impl IntoView {
    view! {
        <label class="text-gray-600">{heading}</label>
        <Show when=invalid_timing>
            <div class="text-error">{invalid_timing_error}</div>
        </Show>
        <Show
            when=move || !signal().is_empty()
            fallback=|| {
                view! {
                    <br />
                    "No steps available"
                }
            }
        >
            <ul class="ml-4 list-disc">
                <For each=signal key=|x| x.clone() let:step>
                    // TODO: editable step representation
                    <li>
                        <span>"Step" {step.comment.map(|c| format!(" ({c})"))}</span>
                        <div class="flex gap-4">
                            <div>
                                <span class="fa-solid fa-clock mr-2" />
                                {step_timing_to_string(step.timing)}
                            </div>
                            <div>
                                <span class="fa-solid fa-shoe-prints mr-2" />
                                {if step.foot == Foot::Left { "left" } else { "right" }}
                            </div>
                            <div>
                                <span class="fa-solid fa-weight-hanging mr-2" />
                                {match step.weight_transfer {
                                    WeightTransfer::None => "none".into(),
                                    WeightTransfer::Half => "half".into(),
                                    WeightTransfer::Full => "full".into(),
                                    WeightTransfer::Custom(percent) => format!("{percent} %"),
                                }}
                            </div>
                        </div>
                    </li>
                </For>
            </ul>
        </Show>
        <div class="flex justify-end mt-2">
            <button
                class="btn btn-circle btn-sm"
                on:click=move |_| {
                    let mut steps = signal();
                    steps.push(Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::default()));
                    signal.set(steps);
                }
            >
                <span class="fa-solid fa-plus" />
            </button>
        </div>
    }
}

#[component]
fn StepInput(signal: RwSignal<Step>) -> impl IntoView {
    let foot = RwSignal::new(signal().foot);
    let timing = RwSignal::new(signal().timing);
    let weight_transfer = RwSignal::new(signal().weight_transfer);
    let (comment, set_comment) = create_signal(signal().comment.unwrap_or_default());

    view! {
        <div class="mt-2 mb-4">
            <FootInput text="Foot" signal=foot />
        </div>
        <StepTimingInput text="Timing" signal=timing />
        <div class="my-4">
            <WeightTransferInput text="Weight transfer" signal=weight_transfer />
        </div>
        <div class="flex gap-2 items-center">
            <label for="item-title">"Comment:"</label>
            <input
                class="input input-bordered grow"
                id="item-title"
                placeholder="…"
                type="text"
                on:input=move |ev| {
                    set_comment(event_target_value(&ev));
                }
                prop:value=comment
            />
        </div>
    }
}

#[component]
fn FootInput(text: &'static str, signal: RwSignal<Foot>) -> impl IntoView {
    let foot = RwSignal::new(
        match signal() {
            Foot::Left => "Left",
            Foot::Right => "Right",
        }
        .to_string(),
    );
    let (valid_feet, _) = create_signal(vec!["Left".into(), "Right".into()]);

    Effect::new(move |_| {
        signal.set(match foot().as_str() {
            "Left" => Foot::Left,
            "Right" => Foot::Right,
            // The following case cannot happen.
            _ => Foot::Left,
        });
    });

    view! {
        <div class="flex gap-2 items-center">
            <label for="item-foot">{text}":"</label>
            <DropdownSelect
                bind_signal=foot
                options=valid_feet()
                required=false
                classes="grow".into()
            />
        </div>
    }
}

#[component]
fn WeightTransferInput(text: &'static str, signal: RwSignal<WeightTransfer>) -> impl IntoView {
    let weight_transfer = RwSignal::new(
        match signal() {
            WeightTransfer::None => "None",
            WeightTransfer::Half => "Half",
            WeightTransfer::Full => "Full",
            WeightTransfer::Custom(_) => "Custom",
        }
        .to_string(),
    );
    let (valid_transfers, _) = create_signal(vec![
        "None".into(),
        "Half".into(),
        "Full".into(),
        "Custom".into(),
    ]);

    let needs_input = move || weight_transfer() == "Custom";
    let (percent, set_percent) = create_signal(0);

    Effect::new(move |_| {
        signal.set(match weight_transfer().as_str() {
            "None" => WeightTransfer::None,
            "Half" => WeightTransfer::Half,
            "Full" => WeightTransfer::Full,
            "Custom" => WeightTransfer::Custom(percent()),
            // The following case cannot happen.
            _ => WeightTransfer::Custom(0),
        });
    });

    view! {
        <div class="flex gap-2 items-center">
            <label for="item-weight">{text}":"</label>
            <DropdownSelect
                bind_signal=weight_transfer
                options=valid_transfers()
                required=false
                classes="grow".into()
            />
        </div>
        <Show when=needs_input>
            <div class="flex gap-2 items-center">
                <label for="item-weight-duration" class="invisible">
                    {text}
                    ":"
                </label>
                <input
                    class="input input-bordered mx-1 grow"
                    id="item-weight-duration"
                    type="number"
                    min="0"
                    max="100"
                    on:input=move |ev| {
                        if let Ok(weight) = event_target_value(&ev).parse::<u8>() {
                            if weight <= 100 {
                                set_percent(weight);
                            }
                        }
                    }
                    prop:value=percent
                />
                <span>"%"</span>
            </div>
        </Show>
    }
}

#[component]
fn StepTimingInput(text: &'static str, signal: RwSignal<StepTiming>) -> impl IntoView {
    let timing = RwSignal::new(
        match signal() {
            StepTiming::A => "A",
            StepTiming::And => "And",
            StepTiming::Quick => "Quick",
            StepTiming::Slow => "Slow",
            StepTiming::Custom(_) => "Custom duration",
        }
        .to_string(),
    );
    let (valid_timings, _) = create_signal(vec![
        "A".into(),
        "And".into(),
        "Quick".into(),
        "Slow".into(),
        "Custom duration".into(),
    ]);

    let needs_duration_input = move || timing() == "Custom duration";
    let (semiquavers, set_semiquavers) = create_signal(0);

    Effect::new(move |_| {
        signal.set(match timing().as_str() {
            "A" => StepTiming::A,
            "And" => StepTiming::And,
            "Quick" => StepTiming::Quick,
            "Slow" => StepTiming::Slow,
            "Custom duration" => StepTiming::Custom(semiquavers()),
            // The following case cannot happen.
            _ => StepTiming::Custom(0),
        });
    });

    view! {
        <div class="flex gap-2 items-center">
            <label for="item-timing">{text}":"</label>
            <DropdownSelect
                bind_signal=timing
                options=valid_timings()
                required=false
                classes="grow".into()
            />
        </div>
        <Show when=needs_duration_input>
            <div class="flex gap-2 items-center">
                <label for="item-timing-duration" class="invisible">
                    {text}
                    ":"
                </label>
                <input
                    class="input input-bordered mx-1 grow"
                    id="item-timing-duration"
                    type="number"
                    min="0"
                    on:input=move |ev| {
                        if let Ok(dur) = event_target_value(&ev).parse::<usize>() {
                            set_semiquavers(dur);
                        }
                    }
                    prop:value=semiquavers
                />
                <span>"𝅘𝅥𝅯"</span>
            </div>
        </Show>
    }
}

#[component]
fn LevelRequirementInput(text: &'static str, signal: RwSignal<LevelRequirement>) -> impl IntoView {
    let level = RwSignal::new(
        match signal() {
            LevelRequirement::ToBeDetermined => "Undefined",
            LevelRequirement::WdsfDCSyllabus => "WDSF D/C class",
            LevelRequirement::WdsfBSyllabus => "WDSF B class",
            LevelRequirement::Open => "Open",
        }
        .to_string(),
    );
    let (valid_levels, _) = create_signal(vec![
        "Undefined".into(),
        "WDSF D/C class".into(),
        "WDSF B class".into(),
        "Open".into(),
    ]);

    Effect::new(move |_| {
        signal.set(match level().as_str() {
            "Undefined" => LevelRequirement::ToBeDetermined,
            "WDSF D/C class" => LevelRequirement::WdsfDCSyllabus,
            "WDSF B class" => LevelRequirement::WdsfBSyllabus,
            "Open" => LevelRequirement::Open,
            // The following case cannot happen.
            _ => LevelRequirement::ToBeDetermined,
        });
    });

    view! {
        <div class="flex gap-2 items-center">
            <label for="item-foot">{text}":"</label>
            <DropdownSelect
                bind_signal=level
                options=valid_levels()
                required=false
                classes="grow".into()
            />
        </div>
    }
}
