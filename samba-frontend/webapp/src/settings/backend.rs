use anyhow::Result;
use samba_shared::{
    libraries::{CreateLibraryRequestBody, Library},
    preferences::UserPreferences,
    BackgroundTaskStatus,
};

use crate::util::{delete_from_remote, get_from_remote, post_to_remote, put_to_remote};

pub async fn get_background_task_status(background_task_id: &str) -> Result<BackgroundTaskStatus> {
    get_from_remote(&format!("tasks/{background_task_id}")).await
}

pub async fn fetch_libraries() -> Result<Vec<Library>> {
    get_from_remote("libraries").await
}

pub async fn create_library(library: Library) -> Result<String> {
    post_to_remote("libraries/create", CreateLibraryRequestBody {
        id: library.id,
        name: library.name,
        path: library.path,
        scan_subdirectories: library.scan_subdirectories,
        shared: library.shared,
    })
    .await
}

pub async fn delete_library(library_id: &str) -> Result<()> {
    delete_from_remote(&format!("libraries/{}", library_id)).await
}

pub async fn scan_library(library_id: &str) -> Result<String> {
    get_from_remote(&format!("libraries/{}/scan", library_id)).await
}

pub async fn update_library(library_id: &str, library: Library) -> Result<()> {
    put_to_remote(&format!("libraries/{}", library_id), library).await
}

pub async fn scan_all_libraries() -> Result<String> {
    get_from_remote("/libraries/scan").await
}

pub async fn submit_preference_update(preferences: &UserPreferences) -> Result<()> {
    put_to_remote("user/preferences", preferences).await
}
