pub enum SettingsComponent {
    Dashboard,
    Library,
    Profile,
    Users,
}

impl std::fmt::Display for SettingsComponent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            SettingsComponent::Dashboard => "",
            SettingsComponent::Library => "/library",
            SettingsComponent::Profile => "/profile",
            SettingsComponent::Users => "/users",
        })
    }
}
