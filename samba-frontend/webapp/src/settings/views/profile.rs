use std::collections::HashSet;

use codee::string::JsonSerdeWasmCodec as JsonCodec;
use leptos::*;
use leptos_use::storage::use_local_storage;
use samba_frontend_shared::{
    profile::{UserProfile, LOCAL_STORAGE_PROFILE_KEY},
    AppState,
};
use samba_frontend_uicomponents::{DropdownSelect, Heading, Placeholder, Space};
use samba_shared::preferences::{
    experimental::{DEBUG_FEATURE, EVENT_FEATURE},
    ExclusionMode, TrackRowIndicator, UserPreferences,
};
use tts::Tts;

use super::super::{backend::submit_preference_update, SettingsComponent};
use crate::{
    management::logout,
    util::{get_from_remote, refresh_page},
    AppRoutes,
};

#[component]
pub fn ProfileUpdateView() -> impl IntoView {
    let (profile, _, _) = use_local_storage::<UserProfile, JsonCodec>(LOCAL_STORAGE_PROFILE_KEY);

    view! {
        <Heading>"Your profile"</Heading>
        {move || {
            if !profile().is_guest() {
                view! { <ProfileUpdateHandler profile=profile() /> }.into_view()
            } else {
                view! { <NoProfileFound /> }.into_view()
            }
        }}
    }
}

#[component]
fn NoProfileFound() -> impl IntoView {
    let logout_fn = create_action(move |_: &()| async move {
        logout().await;
    });

    view! {
        <Placeholder
            icon=view! { <span class="fa-solid fa-stop-circle" style="font-size: 500%;"></span> }
                .into_view()
            title=view! { "No profile found in session" }.into_view()
            subtitle=view! {
                "Your session seems to have been corrupted. Please try logging \
                 out and logging back in. If this does not help, try cleaning \
                 your browser's cache."
            }
                .into_view()
            commands=vec![
                view! {
                    <div class="btn-container">
                        <a
                            class="btn btn-primary u-inline-block"
                            href=AppRoutes::Settings(SettingsComponent::Dashboard)
                        >
                            "Back to settings"
                        </a>
                    </div>
                }
                    .into_view(),
                view! {
                    <div class="btn-container">
                        <button class="btn-primary" on:click=move |_| logout_fn.dispatch(())>
                            "Logout"
                        </button>
                    </div>
                }
                    .into_view(),
            ]
        />
    }
}

#[component]
fn ProfileUpdateHandler(profile: UserProfile) -> impl IntoView {
    let app_state =
        use_context::<AppState>().expect("Updating profile requires login and thus state.");
    let is_debug_mode = app_state.is_experimental_feature_enabled(DEBUG_FEATURE);

    // TODO: make reactive (use setters)
    let (avatar_preview, _) = create_signal(profile.avatar_url);
    let (user_name, set_user_name) = create_signal(profile.user_name);
    let (jwt, _) = create_signal(profile.jwt);

    let (avatar_url, set_avatar_url) = create_signal(String::new());
    let avatar_view = Memo::new(move |_| {
        let preview_url = avatar_preview().clone();
        if let Some(avatar_url) = preview_url {
            view! {
                <figure class="avatar avatar--xl">
                    <img src=avatar_url.clone() />
                </figure>
            }
            .into_view()
        } else {
            view! {
                // TODO: positioning
                <p class="absolute text-white font-bold justify-middle">
                    "No profile image yet."
                </p>
                <p class="w-full pt-12 pb-12 pl-4 pr-4 bg-base-300"></p>
            }
            .into_view()
        }
    });

    let page_size = RwSignal::new(
        app_state
            .user_preferences
            .with(|pref| pref.pagination_default_page_size),
    );
    let ellipsis_distance = RwSignal::new(
        app_state
            .user_preferences
            .with(|pref| pref.pagination_ellipsis_distance),
    );
    let track_row_indicator = RwSignal::new(
        app_state
            .user_preferences
            .with(|pref| pref.track_row_indicator),
    );
    let track_exclusion_rules = RwSignal::new(
        app_state
            .user_preferences
            .with(|pref| pref.track_exclusion_rules.clone()),
    );
    let tts_voice = RwSignal::new(String::new());

    let debug_mode = RwSignal::new(app_state.is_experimental_feature_enabled(DEBUG_FEATURE));
    let events = RwSignal::new(app_state.is_experimental_feature_enabled(EVENT_FEATURE));

    let submit_action = move |_| {
        let app_state =
            use_context::<AppState>().expect("Updating profile requires login and thus state.");
        let mut experimental_features = HashSet::new();
        if debug_mode() {
            experimental_features.insert(DEBUG_FEATURE.into());
        }
        if events() {
            experimental_features.insert(EVENT_FEATURE.into());
        }

        let new_preferences = UserPreferences {
            pagination_default_page_size: page_size(),
            pagination_ellipsis_distance: ellipsis_distance(),
            preferred_tts_voice: tts_voice.get().is_empty().then(|| tts_voice().clone()),
            track_row_indicator: track_row_indicator(),
            experimental_feature_opt_in: experimental_features,
            ..Default::default()
        };

        spawn_local(async move {
            // TODO: error handling
            submit_preference_update(&new_preferences).await.unwrap();
            // We could actually set the current app user preferences to
            // `new_preferences` directly but it is a good idea to stay
            // consistent with backend state.
            if let Ok(preferences) = get_from_remote("user/preferences").await {
                app_state.replace_user_preferences(preferences);
            }
            refresh_page();
        });
    };

    view! {
        <form action="javascript:void(0);" method="post" on:submit=submit_action>
            <div class="grid grid-cols-5 items-center gap-y-6">
                <label class="col-span-1" for="user-id">
                    "User name:"
                </label>
                <input
                    class="input input-bordered col-span-4"
                    id="user-id"
                    placeholder="Your Name"
                    required=true
                    type="text"
                    on:input=move |ev| {
                        set_user_name(event_target_value(&ev));
                    }
                    prop:value=user_name
                />
                <label class="col-span-1" for="user-avatar">
                    "Profile image:"
                </label>
                <div class="col-span-3">
                    // TODO: change preview as soon as file is uploaded
                    <input
                        id="user-avatar"
                        type="file"
                        on:input=move |ev| {
                            set_avatar_url(event_target_value(&ev));
                        }
                        prop:value=avatar_url
                    />
                </div>
                <div class="col-span-1">
                    <div class="u-relative u-center">{avatar_view}</div>
                    <span class="text-xs text-gray-600">"Your current avatar"</span>
                </div>
            </div>
            <Space />
            <PaginationSection page_size=page_size ellipsis_distance=ellipsis_distance />
            <TtsSection voice=tts_voice />
            <Space />
            <LibrarySection
                track_row_indicator=track_row_indicator
                track_exclusion_rules=track_exclusion_rules
            />
            <Space />
            <ExperimentalSection debug_mode=debug_mode events=events />
            <Show when=move || is_debug_mode>
                <Space />
                <div class="row level text-gray-600">
                    <div class="col level-item">"Session token: " {jwt}</div>
                    <div class="col level-item">
                        <div class="form-ext-control form-ext-checkbox">
                            <input
                                checked=profile.is_admin
                                class="form-ext-input"
                                disabled=true
                                id="user-admin"
                                type="checkbox"
                            />
                            <label class="form-ext-label" for="user-admin">
                                "Admin account"
                            </label>
                        </div>
                    </div>
                </div>
            </Show>
            <Space />
            <div class="flex justify-end gap-4">
                <a class="btn btn-outline" href=AppRoutes::Settings(SettingsComponent::Dashboard)>
                    "Back to settings"
                </a>
                <button type="submit" class="btn btn-primary">
                    "Update profile"
                </button>
            </div>
        </form>
    }
}

#[component]
pub fn PaginationSection(
    page_size: RwSignal<usize>,
    ellipsis_distance: RwSignal<usize>,
) -> impl IntoView {
    view! {
        <h2 class="font-bold mb-4">"Pagination"</h2>
        <div class="flex gap-4 mb-6 items-center">
            <label for="page-size">
                "Page size "
                <div class="dropdown dropdown-end">
                    <label tabindex="0" class="btn btn-circle btn-ghost btn-xs text-info">
                        <span class="fa-solid fa-info" />
                    </label>
                    <div tabindex="0" class="card compact dropdown-content z-[1] shadow bg-base-100 rounded-box w-64">
                        <div class="card-body">
                            <h2 class="card-title">"Page size"</h2>
                            <p>
                                "How many items (tracks/events/…) to show on one page."
                            </p>
                        </div>
                    </div>
                </div>
            </label>
            <input
                class="input input-bordered grow"
                id="page-size"
                min="1"
                type="number"
                on:input=move |ev| {
                    if let Ok(size) = event_target_value(&ev).parse::<usize>() {
                        page_size.set(size);
                    }
                }
                prop:value=page_size
            />
            <label class="ml-8" for="ellipsis-distance">
                "Ellipsis distance "
                <div class="dropdown dropdown-end">
                    <label tabindex="0" class="btn btn-circle btn-ghost btn-xs text-info">
                        <span class="fa-solid fa-info" />
                    </label>
                    <div
                        tabindex="0"
                        class="card compact dropdown-content z-[1] shadow bg-base-100 rounded-box w-64"
                    >
                        <div class="card-body">
                            <h2 class="card-title">"Ellipsis distance"</h2>
                            <p>
                                "How many pages to show next to current page before replacing with ellipsis."
                            </p>
                        </div>
                    </div>
                </div>
            </label>
            <input
                class="input input-bordered grow"
                id="ellipsis-distance"
                min="0"
                type="number"
                on:input=move |ev| {
                    if let Ok(dist) = event_target_value(&ev).parse::<usize>() {
                        ellipsis_distance.set(dist);
                    }
                }
                prop:value=ellipsis_distance
            />
        </div>
    }
}

#[component]
pub fn TtsSection(voice: RwSignal<String>) -> impl IntoView {
    let (tts_voices, _) = create_signal(Tts::default().map_or_else(
        |_| Vec::new(),
        |tts| {
            tts.voices()
                .expect("If the browser supports TTS, we assume it supports fetching voices.")
                .iter()
                .map(|voice| voice.name())
                .collect::<Vec<_>>()
        },
    ));

    view! {
        <Show when=move || !tts_voices().is_empty()>
            <Space />
            <h2 class="font-bold mb-4">"Text-to-speech"</h2>
            <div class="flex gap-4 items-center">
                <label for="tts-voice">"Preferred voice:"</label>
                // TODO: set id
                <DropdownSelect
                    bind_signal=voice
                    options=tts_voices()
                    required=false
                    classes="grow".into()
                />
            </div>
        </Show>
    }
}

#[component]
pub fn LibrarySection(
    track_row_indicator: RwSignal<TrackRowIndicator>,
    track_exclusion_rules: RwSignal<HashSet<(String, ExclusionMode)>>,
) -> impl IntoView {
    let (possible_indicators, _) = create_signal(vec![
        "None".to_string(),
        "Track color".to_string(),
        "Track color and thumbnail".to_string(),
        "Track color and full cover art".to_string(),
    ]);
    let track_row_indicator_text = RwSignal::new(
        match track_row_indicator.get() {
            TrackRowIndicator::None => "None",
            TrackRowIndicator::ColorOnly => "Track color",
            TrackRowIndicator::ColorAndThumbnail => "Track color and thumbnail",
            TrackRowIndicator::ColorAndCoverArt => "Track color and full cover art",
        }
        .to_string(),
    );
    Effect::new(move |_| {
        if let Some(indicator) = match track_row_indicator_text().as_str() {
            "None" => Some(TrackRowIndicator::None),
            "Track color" => Some(TrackRowIndicator::ColorOnly),
            "Track color and thumbnail" => Some(TrackRowIndicator::ColorAndThumbnail),
            "Track color and full cover art" => Some(TrackRowIndicator::ColorAndCoverArt),
            _ => None,
        } {
            track_row_indicator.set(indicator);
        }
    });

    view! {
        <h2 class="font-bold mb-4">"Library"</h2>
        <div class="flex gap-4 items-center">
            <label>
                "Track row highlighting "
                <div class="dropdown dropdown-end">
                    <label tabindex="0" class="btn btn-circle btn-ghost btn-xs text-info">
                        <span class="fa-solid fa-info" />
                    </label>
                    <div tabindex="0" class="card compact dropdown-content z-[1] shadow bg-base-100 rounded-box w-64">
                        <div class="card-body">
                            <h2 class="card-title">"Track row highlighting"</h2>
                            <p>
                                "How to highlight the rows in the library (the section on the left). \
                                 By default, shows track color and album art thumbnail."
                            </p>
                        </div>
                    </div>
                </div>
            </label>
            <DropdownSelect
                bind_signal=track_row_indicator_text
                options=possible_indicators()
                required=false
                classes="grow".into()
            />
        // div(class="col") {
        // label { "Exclude from library" }
        // // TODO: track exclusion rules
        // }
        </div>
    }
}

#[component]
pub fn ExperimentalSection(debug_mode: RwSignal<bool>, events: RwSignal<bool>) -> impl IntoView {
    view! {
        <h2 class="font-bold mb-4">"Experimental features"</h2>
        <div class="flex gap-6 items-center">
            <div class="flex gap-2 items-center grow">
                <input
                    class="toggle"
                    id="debug"
                    type="checkbox"
                    prop:checked=debug_mode
                    on:input=move |ev| {
                        debug_mode.set(event_target_checked(&ev));
                    }
                />
                <label for="debug">"Debug mode"</label>
            </div>
            <div class="flex gap-2 items-center grow">
                <input
                    class="toggle"
                    id="events"
                    type="checkbox"
                    prop:checked=events
                    on:input=move |ev| {
                        events.set(event_target_checked(&ev));
                    }
                />
                <label for="events">"Event management"</label>
            </div>
        </div>
    }
}
