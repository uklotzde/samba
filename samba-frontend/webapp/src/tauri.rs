#[cfg(feature = "tauri")]
mod glue {
    use wasm_bindgen::{prelude::wasm_bindgen, JsValue};
    #[wasm_bindgen(module = "/static/js/tauri_glue.js")]
    extern "C" {
        #[wasm_bindgen(catch, js_name = getBackendPort)]
        pub async fn get_backend_port() -> Result<JsValue, JsValue>;
    }
}

pub const fn running_in_tauri() -> bool {
    cfg!(feature = "tauri")
}

#[cfg(feature = "tauri")]
pub fn get_backend_port() -> usize {
    let (port, set_port) = create_signal(8081);
    leptos::spawn_local(async move {
        set_port(
            glue::get_backend_port()
                .await
                .ok()
                .and_then(|js_val| serde_wasm_bindgen::from_value(js_val).ok())
                .expect("The function returns whole numbers"),
        );
    });
    port()
}
#[cfg(not(feature = "tauri"))]
pub const fn get_backend_port() -> usize {
    8081
}
