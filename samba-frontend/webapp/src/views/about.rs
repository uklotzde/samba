use anyhow::{Context, Result};
use leptos::*;
use samba_frontend_shared::AppState;
use samba_frontend_uicomponents::{Heading, HeadingLevel, Space};
use samba_shared::SambaBackendVersion;

async fn fetch_version(backend_url: &str) -> Result<SambaBackendVersion> {
    reqwest::get(&format!("{backend_url}/version"))
        .await
        .with_context(|| "did not get response")?
        .json()
        .await
        .with_context(|| "failed to fetch version from JSON")
}

#[component]
pub fn AboutView() -> impl IntoView {
    let app_state = use_context::<AppState>().expect("Application state must be available.");
    let (backend_url, _) = create_signal(app_state.backend_url.clone());

    let version = Resource::new(
        || (),
        move |_| async move {
            fetch_version(&backend_url())
                .await
                .ok()
                .map(|backend_version| backend_version.version)
        },
    );

    view! {
        <section>
            <Heading>"About samba v" {env!("CARGO_PKG_VERSION")}</Heading>
            <Heading level=HeadingLevel::Section>"Our mission"</Heading>
            <p>
                "samba aims to provide excellent support for operating on music \
                 libraries of dancers or dancing clubs. It is geared towards \
                 productively concentrating on your dancing and having your music \
                 simply available whenever you need it."
            </p>
            <Space />
            <Heading level=HeadingLevel::Section>"Acknowledgements"</Heading>
            <p>
                "This project makes use of the following incredible open-source software (among others):"
            </p>
            <ul class="list-disc ml-6 my-2">
                <li>
                    "the web framework "
                    <a class="link" href="https://github.com/leptos-rs/leptos" target="blank">
                        "leptos"
                    </a> ","
                </li>
                <li>
                    "the music library database "
                    <a class="link" href="https://gitlab.com/uklotzde/aoide-rs" target="blank">
                        "aoide"
                    </a> ","
                </li>
                // <li>
                // "the "
                // <a class="link" href = "https://wavesurfer-js.org" target = "blank">"wafesurver.js"</a>
                // " audio player interface,"
                // </li>
                <li>
                    "the "
                    <a class="link" href="https://tailwindcss.com/" target="blank">
                        "Tailwind"
                    </a>
                    " CSS framework, and"
                </li>
                <li>
                    "the "
                    <a class="link" href="https://daisyui.com/" target="blank">
                        "daisyUI"
                    </a>
                    " components framework."
                </li>
            </ul>
            <p>
                "Special thanks to Uwe Klotz, principal author of aoide, for his support in several \
                aspects of the application and adjustments to aoide."
            </p>
            <div class="divider"></div>
            <p>"The following components are currently running"</p>
            <ul class="list-disc ml-6 my-2">
                <li>
                    "samba backend version "
                    <Suspense fallback=|| {
                        view! {
                            <span class="loading loading-dots loading-xs translate-y-1/4"></span>
                        }
                    }>{move || { version().map(|v| view! { <span>{v}</span> }) }}</Suspense>
                </li>
                <li>"samba frontend version " {env!("CARGO_PKG_VERSION")}</li>
            </ul>
        </section>
    }
}
