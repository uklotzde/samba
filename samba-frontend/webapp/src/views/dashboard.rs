use leptos::*;
use samba_frontend_shared::AppState;
use samba_frontend_uicomponents::{Card, Heading};
use samba_shared::preferences::experimental::EVENT_FEATURE;

use crate::{
    events::EventAction, library::LibraryAction, practice::PracticeAction, routines::RoutineAction,
    AppRoutes,
};

#[component]
pub fn DashboardView() -> impl IntoView {
    let app_state = use_context::<AppState>().expect("AppState is provided by the application.");
    let events_opt_in = app_state.is_experimental_feature_enabled(EVENT_FEATURE);

    view! {
        <section>
            <Heading>"Start dancing"</Heading>
            <div class="grid grid-flow-row grid-cols-2 gap-4">
                <Card
                    title="Practice session".into()
                    action_bar=view! {
                        <a class="btn" href=AppRoutes::Practice(PracticeAction::AdHoc)>
                            "Ad-hoc"
                        </a>
                        <a class="btn" href=AppRoutes::Practice(PracticeAction::List)>
                            "List"
                        </a>
                    }
                        .into_view()
                >
                    <p>
                        "Start a practice session to play a sequence of dances without \
                         requiring your attention. Useful for practicing competitions."
                    </p>
                </Card>
                <Card
                    title="Play music".into()
                    action_bar=view! {
                        <a
                            class="btn"
                            href=AppRoutes::Library(LibraryAction::View(Default::default()))
                        >
                            "Browse library"
                        </a>
                        <a class="btn" href=AppRoutes::Library(LibraryAction::ViewByDance)>
                            "Browse by dance"
                        </a>
                    }
                        .into_view()
                >
                    <p>
                        "Search your library for specific songs and stumble upon the \
                         one you want to play next."
                    </p>
                </Card>
                <Card
                    title="Browse routines".into()
                    action_bar=view! {
                        <a class="btn" href=AppRoutes::Routines(RoutineAction::Create)>
                            "New"
                        </a>
                        <a class="btn" href=AppRoutes::Routines(RoutineAction::List)>
                            "List"
                        </a>
                    }
                        .into_view()
                >
                    <p>
                        "Seek inspiration in available routines and browse previous \
                            dancing sessions."
                    </p>
                </Card>
                <Show when=move || events_opt_in>
                    <Card
                        title="Prepare event".into()
                        action_bar=view! {
                            <a class="btn" href=AppRoutes::Events(EventAction::Create)>
                                "New"
                            </a>
                            <a class="btn" href=AppRoutes::Events(EventAction::List)>
                                "List"
                            </a>
                        }
                            .into_view()
                    >
                        <p>
                            "Plan your next event with playlists and organizational details \
                             to impress your guests."
                        </p>
                    </Card>
                </Show>
            </div>
        </section>
    }
}
