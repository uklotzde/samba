use codee::string::JsonSerdeWasmCodec as JsonCodec;
use leptos::*;
use leptos_use::storage::use_local_storage;
use samba_frontend_shared::profile::{UserProfile, LOCAL_STORAGE_PROFILE_KEY};

use crate::{
    management::{logout, shutdown},
    settings::SettingsComponent,
    AppRoutes,
};

#[component]
pub fn FooterView() -> impl IntoView {
    let (profile, _, _) = use_local_storage::<UserProfile, JsonCodec>(LOCAL_STORAGE_PROFILE_KEY);

    let logged_in = move || !profile().is_guest();
    let is_admin = move || profile().is_admin;

    let logout_fn = create_action(move |_: &()| async move {
        logout().await;
    });
    let shutdown_fn = create_action(move |_: &()| async move {
        shutdown().await;
    });

    view! {
        <footer class="footer footer-center p-10 text-slate-700 dark:text-slate-500">
            <Show when=move || { logged_in() || is_admin() }>
                <nav class="grid grid-flow-col gap-4">
                    <Show when=logged_in>
                        <a class="btn btn-ghost" on:click=move |_| logout_fn.dispatch(())>
                            <span class="fa-solid fa-sign-out-alt"></span>
                            <span class="icon-pad"></span>
                            <span class="level-item">"Logout"</span>
                        </a>
                        <a
                            class="btn btn-ghost"
                            href=AppRoutes::Settings(SettingsComponent::Dashboard)
                        >
                            <span class="fa-solid fa-cogs"></span>
                            <span class="icon-pad"></span>
                            <span class="level-item">"Settings"</span>
                        </a>
                    </Show>
                    <Show when=is_admin>
                        <a class="btn btn-ghost" on:click=move |_| shutdown_fn.dispatch(())>
                            <span class="fa-solid fa-power-off"></span>
                            <span class="icon-pad"></span>
                            <span class="level-item">"Shutdown"</span>
                        </a>
                    </Show>
                </nav>
            </Show>
            <aside>
                <p>"Proudly powered by samba – The smart and mechanic ballroom assistant."</p>
            </aside>
        </footer>
    }
}
