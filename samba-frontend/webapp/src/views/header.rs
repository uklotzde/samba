use codee::string::JsonSerdeWasmCodec as JsonCodec;
use leptos::*;
use leptos_use::{
    storage::use_local_storage, use_color_mode_with_options, ColorMode, UseColorModeOptions,
    UseColorModeReturn,
};
use samba_frontend_shared::{
    profile::{UserProfile, LOCAL_STORAGE_PROFILE_KEY},
    AppState,
};
use samba_shared::preferences::experimental::EVENT_FEATURE;

use crate::{
    events::EventAction,
    library::LibraryAction,
    management::{logout, shutdown},
    practice::PracticeAction,
    routines::RoutineAction,
    settings::SettingsComponent,
    AppRoutes,
};

#[component]
pub fn HeaderView() -> impl IntoView {
    let app_state = use_context::<AppState>().expect("AppState is provided by the application.");
    let events_opt_in = app_state.is_experimental_feature_enabled(EVENT_FEATURE);

    view! {
        <div class="navbar bg-base-100">
            <div class="navbar-start">
                <ul class="menu menu-horizontal items-center">
                    <li class="dropdown">
                        <label tabindex="0" class="btn btn-ghost min-h-min h-min lg:hidden">
                            <span class="fa-solid fa-bars"></span>
                        </label>
                        <ul
                            tabindex="0"
                            class="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
                        >
                            <li class="justify-start">
                                <a href=AppRoutes::Home>
                                    "Dashboard"
                                    <span class="icon-pad"></span>
                                    <span class="fa-solid fa-columns"></span>
                                </a>
                            </li>
                            <li>
                                <a href=AppRoutes::Practice(PracticeAction::List)>
                                    "Practice"
                                    <span class="icon-pad"></span>
                                    <span class="fa-solid fa-headphones"></span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href=AppRoutes::Library(LibraryAction::View(Default::default()))>
                                    "Library"
                                    <span class="icon-pad"></span>
                                    <span class="fa-solid fa-music"></span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href=AppRoutes::Routines(RoutineAction::List)>
                                    "Routines"
                                    <span class="icon-pad"></span>
                                    <span class="fa-solid fa-shoe-prints"></span>
                                </a>
                            </li>
                            <Show when=move || events_opt_in>
                                <li class="nav-item">
                                    <a href=AppRoutes::Events(EventAction::List)>
                                        "Events"
                                        <span class="icon-pad"></span>
                                        <span class="fa-solid fa-calendar-day"></span>
                                    </a>
                                </li>
                            </Show>
                        </ul>
                    </li>
                    <li class="nav-item no-hover">
                        <a href=AppRoutes::Home>
                            <h6 class="title">"samba"</h6>
                        </a>
                    </li>
                    <li class="nav-item hidden lg:flex">
                        <a href="https://gitlab.com/ballroom-dancing/samba">
                            <span class="fa-brands fa-gitlab"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <nav class="navbar-center hidden lg:flex">
                <ul class="menu menu-horizontal gap-2">
                    <li class="nav-item">
                        <a href=AppRoutes::Home>
                            <span class="fa-solid fa-columns"></span>
                            <span class="icon-pad"></span>
                            "Dashboard"
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href=AppRoutes::Practice(PracticeAction::List)>
                            <span class="fa-solid fa-headphones"></span>
                            <span class="icon-pad"></span>
                            "Practice"
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href=AppRoutes::Library(LibraryAction::View(Default::default()))>
                            <span class="fa-solid fa-music"></span>
                            <span class="icon-pad"></span>
                            "Library"
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href=AppRoutes::Routines(RoutineAction::List)>
                            <span class="fa-solid fa-shoe-prints"></span>
                            <span class="icon-pad"></span>
                            "Routines"
                        </a>
                    </li>
                    <Show when=move || events_opt_in>
                        <li class="nav-item">
                            <a href=AppRoutes::Events(EventAction::List)>
                                <span class="fa-solid fa-calendar-day"></span>
                                <span class="icon-pad"></span>
                                "Events"
                            </a>
                        </li>
                    </Show>
                </ul>
            </nav>
            <div class="navbar-end hidden lg:flex">
                <ul class="menu menu-horizontal items-center">
                    <li class="nav-item">
                        <a href=AppRoutes::About>
                            <span class="fa-solid fa-info-circle"></span>
                            <span class="icon-pad"></span>
                            "About"
                        </a>
                    </li>
                    <li>
                        <UserDropdown />
                    </li>
                </ul>
            </div>
        </div>
    }
}

#[component]
fn UserDropdown() -> impl IntoView {
    let (profile, _, _) = use_local_storage::<UserProfile, JsonCodec>(LOCAL_STORAGE_PROFILE_KEY);
    let logged_in = move || !profile().is_guest();
    let is_admin = move || profile().is_admin;

    let UseColorModeReturn { mode, set_mode, .. } =
        use_color_mode_with_options(UseColorModeOptions::default().attribute("data-theme"));
    let is_dark = move || mode() == ColorMode::Dark;

    let avatar_view = move || {
        view! {
            <div class="avatar m-0 p-1">
                <div class="h-min rounded-full">
                    {move || {
                        if let Some(url) = profile().avatar_url {
                            view! { <img class="h-2" src=url /> }.into_view()
                        } else {
                            view! { <span class="h-2 m-1 fa-solid fa-user"></span> }.into_view()
                        }
                    }}

                </div>
            </div>
        }
    };

    let logout_fn = create_action(move |_: &()| async move {
        logout().await;
    });
    let shutdown_fn = create_action(move |_: &()| async move {
        shutdown().await;
    });

    view! {
        <div class="dropdown dropdown-bottom dropdown-end p-0 mx-2">
            <label tabindex="0" class="btn btn-ghost min-h-min h-min">
                {avatar_view}
            </label>
            <Show when=logged_in>
                <ul tabindex="0" class="menu dropdown-content z-[1] w-52">
                    <li class="menu-item pointer-events-none">
                        <span class="text-sm text-slate-600 dark:text-slate-400">
                            {move || profile().user_name}
                        </span>
                    </li>
                    <li class="items-center">
                        <label class="flex cursor-pointer gap-2">
                            <span class="label-text fa-solid fa-sun"></span>
                            <input
                                type="checkbox"
                                class="toggle theme-controller"
                                on:input=move |_| {
                                    if is_dark() {
                                        set_mode(ColorMode::Light);
                                    } else {
                                        set_mode(ColorMode::Dark);
                                    }
                                }

                                prop:checked=is_dark
                            />
                            <span class="label-text fa-solid fa-moon"></span>
                        </label>
                    </li>
                    <div class="divider m-0"></div>
                    <li class="menu-item">
                        <a href=AppRoutes::Settings(SettingsComponent::Profile)>"Profile"</a>
                    </li>
                    <li class="menu-item">
                        <a href=AppRoutes::Settings(SettingsComponent::Dashboard)>"Settings"</a>
                    </li>
                    <div class="divider m-0"></div>
                    <li class="menu-item">
                        <a on:click=move |_| logout_fn.dispatch(())>"Logout"</a>
                    </li>
                    <Show when=is_admin>
                        <li class="menu-item">
                            <a on:click=move |_| shutdown_fn.dispatch(())>"Shutdown"</a>
                        </li>
                    </Show>
                </ul>
            </Show>
        </div>
    }
}
