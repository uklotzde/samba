use anyhow::{anyhow, Context, Result};
use codee::string::JsonSerdeWasmCodec as JsonCodec;
use leptos::*;
use leptos_use::storage::use_local_storage;
use reqwest::StatusCode;
use samba_frontend_shared::{
    profile::{UserProfile, LOCAL_STORAGE_PROFILE_KEY},
    AppState, Message,
};
use samba_shared::preferences::UserPreferences;
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::JsCast;

use crate::util::REQWEST_CLIENT;

async fn login(backend_url: &str, user_name: &str, password: &str) -> Result<String> {
    #[derive(serde::Serialize)]
    struct LoginRequest<'a> {
        user_id: &'a str,
        password: &'a str,
    }

    let response = REQWEST_CLIENT
        .post(format!("{backend_url}/login"))
        .json(&LoginRequest {
            user_id: user_name,
            password,
        })
        .send()
        .await
        .with_context(|| "transmitting credentials failed")?;

    if response.status() == StatusCode::OK {
        response
            .text()
            .await
            .with_context(|| "failed to receive JWT")
    } else {
        Err(anyhow!("Server error while logging in."))
    }
}

async fn get_preferences(backend_url: &str, token: &str) -> Result<UserPreferences> {
    REQWEST_CLIENT
        .get(format!("{backend_url}/user/preferences"))
        .bearer_auth(token)
        .send()
        .await
        .with_context(|| "transmitting preference request failed")?
        .json()
        .await
        .with_context(|| "receiving preference JSON failed")
}

#[component]
pub fn LoginView() -> impl IntoView {
    let (user_name, set_user_name) = create_signal(String::new());
    let (user_pass, set_user_pass) = create_signal(String::new());

    let (login_failed, set_login_failed) = create_signal(false);
    let input_classes = move || {
        format!(
            "input input-bordered py-3 px-4 w-full block pl-10 {}",
            if login_failed() {
                " text-error input-error"
            } else {
                ""
            }
        )
    };

    let submit_action = create_action(move |_: &()| async move {
        let Some(app_state) = use_context::<AppState>() else {
            // This should not happen but we cannot really print an error
            // message, so we just assume that it works.
            return;
        };
        let backend_url = app_state.backend_url.clone();

        let Ok(token) = login(&backend_url, &user_name.get(), &user_pass.get()).await else {
            set_login_failed(true);
            return;
        };
        if token.is_empty() {
            set_login_failed(true);
            return;
        }

        let user = UserProfile {
            jwt: token.clone(),
            user_name: "Guest".into(),
            avatar_url: None,
            is_admin: false,
        };
        let (profile, save_profile, _) =
            use_local_storage::<UserProfile, JsonCodec>(LOCAL_STORAGE_PROFILE_KEY);
        save_profile(user);
        if profile().jwt != token {
            app_state.add_message(Message::new_error(
                "Failed to save your login".into(),
                "Please check your browser's local storage and cookie settings.".into(),
            ));
            return;
        }

        // In addition to the user profile, we set a cookie with the token,
        // so that browser requests for img or from a tags will not fail.
        #[cfg(target_arch = "wasm32")]
        {
            let html_doc = document().dyn_into::<web_sys::HtmlDocument>().unwrap();
            html_doc.set_cookie(&format!("jwt={token};path=/")).unwrap();
        }

        // Get preferences from backend. This needs a separate function due
        // to timing complications: We would need the user profile to have
        // the JWT available for the request but once we set the user
        // profile, a redirection to the actual page view will happen, so
        // we cannot replace the user profile before fetching the user
        // preferences.
        if let Ok(preferences) = get_preferences(&backend_url, &token).await {
            app_state.replace_user_preferences(preferences);
        }
    });

    view! {
        <div class="hero min-h-screen">
            <div class="hero-content flex-col lg:flex-row-reverse">
                <div class="text-center lg:text-left">
                    <h1 class="text-5xl font-bold">samba login</h1>
                    <p class="py-6">
                        Play your dance music, organize your sessions, or plan the next practice.
                    </p>
                </div>
                <div class="card shrink-0 w-full max-w-sm">
                    <div class="card-body">
                        {move || {
                            (login_failed())
                                .then(|| {
                                    view! {
                                        <div class="toast toast--danger">
                                            <p class="toast__title">"Login failed"</p>
                                            <p>"Check user name and password for correctness."</p>
                                        </div>
                                    }
                                })
                        }}
                        <form
                            action="javascript:void(0);"
                            method="post"
                            on:submit=move |_| submit_action.dispatch(())
                        >
                            <div class="form-control mb-3">
                                <label class="relative block">
                                    <span class="fa-solid fa-user pointer-events-none py-2 w-8 h-8 absolute top-1/2 transform -translate-y-1/2 left-3"></span>
                                    <input
                                        autofocus=true
                                        class=input_classes
                                        placeholder="user_name"
                                        required=true
                                        type="text"
                                        on:input=move |ev| {
                                            set_user_name(event_target_value(&ev));
                                        }
                                        prop:value=user_name
                                    />
                                </label>
                            </div>
                            <div class="form-control mb-4">
                                <label class="relative block">
                                    <span class="fa-solid fa-key pointer-events-none py-2 w-8 h-8 absolute top-1/2 transform -translate-y-1/2 left-3"></span>
                                    <input
                                        class=input_classes
                                        placeholder="password"
                                        required=true
                                        type="password"
                                        on:input=move |ev| {
                                            set_user_pass(event_target_value(&ev));
                                        }
                                        prop:value=user_pass
                                    />
                                </label>
                            </div>
                            <div class="w-full">
                                <button type="submit" class="btn btn-primary w-full">
                                    "Login"
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    }
}
