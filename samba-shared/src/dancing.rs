use std::{borrow::Cow, collections::HashMap, ops::Range};

use aoide::tag::FacetId;
use serde::{Deserialize, Serialize};

/// The facet (in aoide terms) of the dance tags.
pub const FACET_ID_DANCE: &FacetId<'_> = &FacetId::new_unchecked(Cow::Borrowed("dance"));

/// The default difference between dance preferences within one facet.
pub const DANCE_PREFERENCE_DELTA: f64 = 0.2;

/// A general dance. Something like slow waltz intended to be represented
/// in the most general sense (wide range of MPM etc.).
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Dance {
    pub name: String,
    pub abbreviation: String,
    pub category: DanceCategory,
    pub mpm_range: Range<u8>,
    pub number_of_crotchets: u8,
}

/// A competitive dance. Most likely a restricted form of a general dance.
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct CompetitiveDance {
    pub name: String,
    pub abbreviation: String,
    pub category: DanceCategory,
    pub mpm_range: Range<u8>,
    pub number_of_crotchets: u8,
    pub from_class: CoupleClass,
    pub order_in_competition: u8,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub enum DanceCategory {
    Ballroom,
    Latin,
    Social,
    Unknown,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Couple {
    pub leader: Dancer,
    pub follower: Dancer,
    pub discipline: Discipline,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Dancer {
    pub name: String,
    pub couple_classes: HashMap<Discipline, CoupleClass>,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum CoupleClass {
    D,
    C,
    B,
    A,
    S,
}

impl std::fmt::Display for CoupleClass {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum CoupleGroup {
    ChildrenI,
    ChildrenII,
    JuniorsI,
    JuniorsII,
    Juveniles,
    AdultsI,
    AdultsII,
    SeniorsI,
    SeniorsII,
    SeniorsIII,
    SeniorsIV,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, Hash, PartialEq, Eq)]
pub enum Discipline {
    Ballroom,
    Latin,
    TenDance,
    ShowDance,
}
