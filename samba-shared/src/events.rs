use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

use super::Stored;
use crate::dancing::{Couple, CoupleClass, CoupleGroup};

pub type StoredEvent = Stored<i32, Event>;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Event {
    #[serde(with = "chrono::naive::serde::ts_seconds")]
    pub date: NaiveDateTime,
    pub title: String,
    pub description: String,
    pub location: String,
    pub organizers: Vec<String>,
    pub tags: Vec<String>,
    /// Field to represent competitions. `None` if the current event
    /// does not represent a competition.
    pub competition_details: Option<CompetitionDetails>,
    pub helpers: Vec<Helper>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Helper {
    pub name: String,
    pub role: String,
    #[serde(with = "chrono::naive::serde::ts_seconds")]
    pub start_of_shift: NaiveDateTime,
    #[serde(with = "chrono::naive::serde::ts_seconds")]
    pub end_of_shift: NaiveDateTime,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CompetitionDetails {
    pub timetable: Vec<Competition>,
    pub couples: Vec<CompetingCouple>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Competition {
    #[serde(with = "chrono::naive::serde::ts_seconds")]
    pub start_time: NaiveDateTime,
    #[serde(with = "chrono::naive::serde::ts_seconds")]
    pub end_time: NaiveDateTime,
    pub class: CoupleClass,
    pub group: CoupleGroup,
    pub floor: usize,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CompetingCouple {
    pub starter_id: usize,
    pub couple: Couple,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateEventRequestBody {
    #[serde(with = "chrono::naive::serde::ts_seconds")]
    pub date: NaiveDateTime,
    pub title: String,
    pub description: String,
    pub location: String,
    pub organizers: String,
    pub tags: String,
    pub competitions: String,
    pub couples: String,
    pub helpers: String,
}
