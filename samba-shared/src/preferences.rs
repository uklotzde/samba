use std::collections::HashSet;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum ExclusionMode {
    #[default]
    All,
    /// Exclude from all practice session playbacks or a specific session's
    /// playback.
    PracticeSession(Option<String>),
    /// Exclude from all event playbacks or a specific event's playback.
    Event(Option<i32>),
}

#[derive(Copy, Clone, Debug, Default, Serialize, Deserialize)]
pub enum TrackRowIndicator {
    None,
    ColorOnly,
    #[default]
    ColorAndThumbnail,
    ColorAndCoverArt,
}

/// A user's preferences. A serialized version of this will be stored by the
/// backend. The frontend makes use of them to adjust the end user experience
/// accordingly. At the moment, it is not versioned; changes are expected to
/// be additive, unknown values are removed, non-present values replaced by
/// defaults.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UserPreferences {
    /// How many items a page should contain by default.
    pub pagination_default_page_size: usize,
    /// How many items should be added to the left/right of the current page
    /// before using ellipsis and first/last page markers.
    pub pagination_ellipsis_distance: usize,
    /// The preferred voice for TTS output. If `None`, the browser's default
    /// will be used. Will be ignored if the preferred voice is not present
    /// on the target system.
    pub preferred_tts_voice: Option<String>,
    /// How to highlight track rows (potential first column of the library
    /// rows).
    pub track_row_indicator: TrackRowIndicator,
    /// Exclude certain tracks from query results.
    pub track_exclusion_rules: HashSet<(String, ExclusionMode)>,
    /// Allow to opt into experimental features. Available features will be
    /// documented in the `experimental` module below. Remember that they only
    /// affect the frontend. The backend will always ship with all features
    /// enabled.
    pub experimental_feature_opt_in: HashSet<String>,
}

impl Default for UserPreferences {
    fn default() -> Self {
        Self {
            pagination_default_page_size: 25,
            pagination_ellipsis_distance: 3,
            preferred_tts_voice: None,
            track_row_indicator: Default::default(),
            track_exclusion_rules: Default::default(),
            experimental_feature_opt_in: Default::default(),
        }
    }
}

/// Experimental feature flags to be used in preferences.
pub mod experimental {
    /// Not an experimental feature per se but useful for debugging purposes
    /// and should be opt-in for potential developers or users wishing to
    /// report bugs.
    pub const DEBUG_FEATURE: &str = "debug";

    /// Event management and planning
    pub const EVENT_FEATURE: &str = "events";
}
