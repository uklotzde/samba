use aoide::Track;

use super::TrackUpdate;

pub mod predefined_lints;

/// Severity of a lint, akin to log levels.
#[derive(Copy, Clone, Default, Debug, PartialEq, Eq)]
pub enum LintSeverity {
    #[default]
    Info,
    Warning,
    Critical,
}

impl std::fmt::Display for LintSeverity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            LintSeverity::Info => "INFO",
            LintSeverity::Warning => "WARN",
            LintSeverity::Critical => "CRIT",
        })
    }
}

/// The result of linting a music file.
#[derive(Clone, Debug, PartialEq)]
pub struct LintResult {
    pub severity: LintSeverity,
    pub message: String,
    pub suggested_fix: String,
    pub fix_operations: Vec<TrackUpdate>,
}

impl std::fmt::Display for LintResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "[{}]: {} (try: {}{})",
            self.severity,
            self.message,
            self.suggested_fix,
            (!self.fix_operations.is_empty())
                .then_some(", fix available")
                .unwrap_or_default()
        )
    }
}

/// A lint for music files.
pub trait Lint {
    /// Check a music file `track` according to this lint's properties.
    /// May produce multiple results.
    #[must_use]
    fn check(&self, track: &Track) -> Vec<LintResult>;
}

/// Apply the given set of `lints` to `track`. Lints are not guaranteed
/// to be applied in order of the `lints` array.
///
/// The `lints` are taken with `Send` and `Sync` for future parallelization
/// options.
#[must_use]
pub fn apply_lints(lints: &[Box<dyn Lint + Send + Sync>], track: &Track) -> Vec<LintResult> {
    lints.iter().flat_map(|lint| lint.check(track)).collect()
}
