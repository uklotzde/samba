use std::collections::{HashMap, HashSet};

use aoide::{
    tag::Label,
    track::{
        actor::Kind as ActorKind,
        tag::{FACET_ID_COMMENT, FACET_ID_GENRE},
    },
    Track,
};

use crate::tracks::{
    artist::parse as parse_artist,
    lint::{Lint, LintResult, LintSeverity},
    title::parse as parse_title,
    ExtractedMetadata, TrackUpdate, TrackUpdateOperation,
};

// Helper functions

/// Get the genre candidates for track if it has one.
fn get_potential_genre(track: &Track) -> Vec<&str> {
    track
        .tags
        .facets
        .iter()
        .find(|x| x.facet_id == *FACET_ID_GENRE)
        .map_or_else(Vec::new, |x| {
            x.tags
                .iter()
                .filter_map(|tag| tag.label.as_ref().map(Label::as_str))
                .collect()
        })
}

// Public API

/// Check if the artist tag is malformed, inconsistent or missing.
#[derive(Debug)]
pub struct BadArtist;
/// Check if the genre is set and a known dance.
#[derive(Debug)]
pub struct BadGenre<'a> {
    pub valid_dance_names: &'a HashSet<&'a str>,
}
/// Check if there is a mismatch between the MPM extracted from metadata and
/// what the music repository stores for time signature and BPM.
#[derive(Debug)]
pub struct MpmMismatch;
/// Check if there is an album tag present.
#[derive(Debug)]
pub struct NoAlbum;
/// Check if there is a non-empty comment which is considered an
/// error (should be in a structured tag).
#[derive(Debug)]
pub struct NonEmptyComment;
/// Check if the properties from the file name (dance abbreviations,
/// mpm, title) are consistent with the file's tags.
#[derive(Debug)]
pub struct PropertiesFromFilename<'a> {
    pub dance_abbreviations: &'a HashMap<&'a str, &'a str>,
}

impl Lint for BadArtist {
    fn check(&self, track: &Track) -> Vec<LintResult> {
        let mut lints = vec![];

        if let Some(artist) = track.track_artist() {
            if artist.contains("(orig ") || artist.contains(" ft ") || artist.contains(" vs ") {
                lints.push(LintResult {
                    severity: LintSeverity::Warning,
                    message: "Unfinished abbreviation in artist field (orig/ft/…).".to_owned(),
                    suggested_fix: "Add a period as in 'orig' → 'orig.'.".to_owned(),
                    fix_operations: vec![],
                });
            }

            let track_actor_count = track.actors.len().saturating_sub(
                track
                    .actors
                    .iter()
                    .filter(|actor| actor.kind == ActorKind::Summary)
                    .count(),
            );
            let artists = parse_artist(artist);
            if artists.len() != track_actor_count && !artists.is_empty() {
                lints.push(LintResult {
                    severity: LintSeverity::Warning,
                    message: "Different number of artists in database and artist field.".to_owned(),
                    suggested_fix: format!(
                        "Compare artists in database ({} found) with those which can be extracted \
                         from the artist field ({} extracted).",
                        track.actors.len(),
                        artists.len()
                    ),
                    fix_operations: vec![],
                });
            }
        } else {
            lints.push(LintResult {
                severity: LintSeverity::Critical,
                message: "No artist present.".to_owned(),
                suggested_fix: "Add an artist.".to_owned(),
                fix_operations: vec![],
            });
        }

        lints
    }
}

impl Lint for BadGenre<'_> {
    fn check(&self, track: &Track) -> Vec<LintResult> {
        let mut results = vec![];
        let genres = get_potential_genre(track);

        if genres.is_empty() {
            results.push(LintResult {
                severity: LintSeverity::Warning,
                message: "No genre set in file.".to_owned(),
                suggested_fix: "Add the genre tag.".to_owned(),
                fix_operations: vec![],
            });
        } else {
            for genre in genres {
                if !self.valid_dance_names.contains(&genre) {
                    results.push(LintResult {
                        severity: LintSeverity::Warning,
                        message: "Genre not known as a dance name.".to_owned(),
                        suggested_fix: format!("Check if {genre} is a known dance name."),
                        fix_operations: vec![],
                    });
                }
            }
        }

        results
    }
}

impl Lint for MpmMismatch {
    fn check(&self, track: &Track) -> Vec<LintResult> {
        // TODO: take list of dances and add fix operations if MPM can be
        //       extracted from title but are not there (to avoid whole
        //       reimport)
        let Some(ExtractedMetadata {
            measures_per_minute: Some(mpm),
            ..
        }) = track.track_title().map(parse_title)
        else {
            return vec![];
        };

        let Some(bpm) = track.metrics.tempo_bpm else {
            return vec![LintResult {
                severity: LintSeverity::Info,
                message: "MPM extracted from title but no BPM field in track.".to_owned(),
                suggested_fix: "Reimport the track's library (make sure the dance of the track is \
                                known at import time)"
                    .to_owned(),
                fix_operations: vec![],
            }];
        };
        let Some(signature) = track.metrics.time_signature else {
            return vec![LintResult {
                severity: LintSeverity::Warning,
                message: "Track contains BPM information and MPM information but no time \
                          signature."
                    .to_owned(),
                suggested_fix: "Reimport the track or reset the MPM information.".to_owned(),
                fix_operations: vec![TrackUpdate::MeasuresPerMinute(
                    TrackUpdateOperation::Replace(mpm.get(), mpm.get()),
                )],
            }];
        };

        if (bpm.value() / signature.beats_per_measure as f64).round() as u32 != mpm.get() as u32 {
            return vec![LintResult {
                severity: LintSeverity::Critical,
                message: "Track contains inconsistent BPM and MPM information.".to_owned(),
                suggested_fix: "Fix the MPM comment or the BPM tag.".to_owned(),
                fix_operations: vec![],
            }];
        }

        vec![]
    }
}

impl Lint for NoAlbum {
    fn check(&self, track: &Track) -> Vec<LintResult> {
        let album = &track.album;
        if album.actors.is_empty() || album.titles.is_empty() {
            vec![LintResult {
                severity: LintSeverity::Info,
                message: "No album field present.".to_owned(),
                suggested_fix: "Add album information (artist/title).".to_owned(),
                fix_operations: vec![],
            }]
        } else {
            vec![]
        }
    }
}

impl Lint for NonEmptyComment {
    fn check(&self, track: &Track) -> Vec<LintResult> {
        track
            .tags
            .facets
            .iter()
            .find(|x| x.facet_id == *FACET_ID_COMMENT)
            .filter(|c| !c.tags.is_empty())
            .map_or_else(Vec::new, |facets| {
                vec![LintResult {
                    severity: LintSeverity::Warning,
                    message: "Nonempty comment tag detected.".to_owned(),
                    suggested_fix: "Clear the comment tag.".to_owned(),
                    fix_operations: facets
                        .tags
                        .iter()
                        .filter_map(|tag| tag.label.as_ref().map(ToString::to_string))
                        .map(|comment| TrackUpdate::Comment(TrackUpdateOperation::Remove(comment)))
                        .collect(),
                }]
            })
    }
}

impl Lint for PropertiesFromFilename<'_> {
    fn check(&self, track: &Track) -> Vec<LintResult> {
        let mut lints = vec![];

        if let Some(file_title) = track.track_title() {
            let ExtractedMetadata {
                dance_abbreviations,
                ..
            } = parse_title(file_title);

            // TODO: check title (be aware that title from extracted metadata
            //       does not contain dance abbreviations but file_title does)

            if dance_abbreviations.is_empty() {
                lints.push(LintResult {
                    severity: LintSeverity::Warning,
                    message: "Dance attributes missing.".to_owned(),
                    suggested_fix: "Add dance abbreviations.".to_owned(),
                    fix_operations: vec![],
                });
            } else {
                let main_dance = dance_abbreviations
                    .first()
                    .map(|abbreviation| {
                        if let Some(dance) = self.dance_abbreviations.get(abbreviation.as_str()) {
                            (*dance).to_string()
                        } else {
                            format!(
                                "{}{}",
                                abbreviation
                                    .chars()
                                    .next()
                                    .expect("A dance abbreviation must not be empty.")
                                    .to_uppercase(),
                                abbreviation.chars().skip(1).collect::<String>()
                            )
                        }
                    })
                    .expect("Dance abbreviations are not empty, so there is one.");

                if get_potential_genre(track)
                    .iter()
                    .any(|genre| genre == &main_dance)
                {
                    lints.push(LintResult {
                        severity: LintSeverity::Critical,
                        message: "Genre and main dance abbreviation do not correspond.".to_owned(),
                        suggested_fix: "Decide which dance is the main dance and adjust genre or \
                                        abbreviation accordingly."
                            .to_owned(),
                        fix_operations: vec![],
                    });
                }
            }

            // TODO: check MPM in range for respective dances
        } else {
            lints.push(LintResult {
                severity: LintSeverity::Critical,
                message: "No title present.".to_owned(),
                suggested_fix: "Add a title.".to_owned(),
                fix_operations: vec![],
            });
        }

        lints
    }
}
