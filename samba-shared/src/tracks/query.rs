//! Interact with track queries
//!
//! Currently, track queries are parsed by tantivy's query parser. This does
//! not expose an AST or similar structs to operate on, so the functions in
//! this module allow for superficial interaction.
//!
//! At the moment, none of these functions do respect negative occurrences
//! (i.e. occurrences that should actually be excluded and therefore not be
//! part of the results).

use std::ops::Range;

use thiserror::Error;

/// Extract dance names from the query string. They may be invalid dance names
/// in terms of known dances.
#[must_use]
pub fn extract_dances(query: &str) -> Vec<String> {
    let mut dances = vec![];
    let mut last_pos = 0;
    let key_len = "dance:".len();

    while let Some(position) = query[last_pos..].find("dance:") {
        let position = last_pos + position;

        if query[position + "dance:".len()..].starts_with('"') {
            // look for matching ", if we don't find it it's an invalid
            // query which leads us to ignore the rest of the string
            if let Some(end) = query
                .get(position + key_len + 1..)
                .and_then(|slice| slice.find('"').map(|pos| position + key_len + 1 + pos))
            {
                dances.push(query[position + key_len + 1..end].to_string());
                last_pos = end;
            } else {
                last_pos = query.len();
            }
        } else {
            // otherwise look for space or end of string
            let end = query[position + 1..]
                .find(' ')
                .map_or_else(|| query.len(), |index| position + index + 1);
            dances.push(query[position + key_len..end].to_string());
            last_pos = end;
        }
    }

    dances
}

#[derive(Debug, Error)]
pub enum MpmExtractionError {
    #[error("Invalid number in query: '{0}'")]
    InvalidNumber(String),
    #[error("Invalid structure for range in query: '{0}'")]
    InvalidRangeStructure(String),
}

/// Extract the MPM range from a given query if any range specified.
///
/// # Errors
///
/// An invalid range (e.g., range separator without numbers or non-numeric
/// range boundaries) will be rejected with an error instead of treating it
/// as not present.
pub fn extract_mpm_range(query: &str) -> Result<Option<Range<usize>>, MpmExtractionError> {
    let mut mpm_range = 0..0;
    let mut last_pos = 0;
    let key_len = "mpm:".len();

    while let Some(position) = query[last_pos..].find("mpm:") {
        let position = last_pos + position;

        if query[position + "mpm:".len()..].starts_with('[')
            || query[position + "mpm:".len()..].starts_with('{')
        {
            // look for matching ", if we don't find it it's an invalid
            // query which leads us to ignore the rest of the string
            if let Some(end) = query.get(position + key_len + 1..).and_then(|slice| {
                slice
                    .find(']')
                    .or(slice.find('}'))
                    .map(|pos| position + key_len + 1 + pos)
            }) {
                // TODO: respect exclusive ranges (`{` and `}`)
                // TODO: intersect ranges/handle multiple ranges
                let mpm_range_candidate = &query[position + key_len + 1..end];
                let mut split_query = mpm_range_candidate.split(" TO ");
                let start = split_query.next().ok_or_else(|| {
                    MpmExtractionError::InvalidRangeStructure(mpm_range_candidate.to_string())
                })?;
                let stop = split_query.next().ok_or_else(|| {
                    MpmExtractionError::InvalidRangeStructure(mpm_range_candidate.to_string())
                })?;
                mpm_range = start.parse::<usize>().map_err(|_invalid_number| {
                    MpmExtractionError::InvalidNumber(start.to_string())
                })?
                    ..stop.parse::<usize>().map_err(|_invalid_number| {
                        MpmExtractionError::InvalidNumber(stop.to_string())
                    })? + 1;
                last_pos = end;
            } else {
                last_pos = query.len();
            }
        } else {
            // otherwise look for space or end of string → this is a single
            // number
            let end = query[position + 1..]
                .find(' ')
                .map_or_else(|| query.len(), |index| position + index + 1);
            let mpm =
                query[position + key_len..end]
                    .parse::<usize>()
                    .map_err(|_invalid_number| {
                        MpmExtractionError::InvalidNumber(
                            query[position + key_len..end].to_string(),
                        )
                    })?;
            mpm_range = mpm..mpm + 1;
            last_pos = end;
        }
    }

    Ok((!mpm_range.is_empty()).then_some(mpm_range))
}
