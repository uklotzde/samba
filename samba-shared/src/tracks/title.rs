use std::num::NonZeroU8;

use super::ExtractedMetadata;

#[cfg(test)]
mod tests;

#[derive(Debug, PartialEq)]
struct ParensParseResult<'a> {
    remix: Option<&'a str>,
    from: Option<&'a str>,
    comment: Option<&'a str>,
}

#[derive(Debug, PartialEq)]
struct DancePartParseResult {
    measures_per_minute: Option<NonZeroU8>,
    dance_abbreviations: Vec<String>,
    plus_flag: bool,
}

/// Parse optional (single) parentheses' content of the title.
/// In “abc (def)” this would be passed the “def” part and parse
/// it for potential movie names and comments.
// TODO: parse (from …, COMMENT[, … Remix])
fn parse_paren(paren: &'_ str) -> ParensParseResult<'_> {
    let mut result = ParensParseResult {
        remix: None,
        from: None,
        comment: None,
    };

    let end_pos = if paren.ends_with("Remix") {
        let start_pos = paren.rfind(',').unwrap_or(0);
        let remix = paren[start_pos..]
            .strip_prefix(", ")
            .unwrap_or(&paren[start_pos..]);
        result.remix = Some(remix.trim());

        start_pos
    } else {
        paren.len()
    };

    if paren.starts_with("from") {
        result.from = Some(paren["from".len()..end_pos].trim());
    } else {
        result.comment = (!paren[..end_pos].is_empty()).then_some(&paren[..end_pos]);
    }

    result
}

/// Parse the dance part of a file name, such as DF, DF-CC, RB23,
/// optionally with tag RB23+.
fn parse_dance_part(dance_part: &'_ str) -> DancePartParseResult {
    let mut result = DancePartParseResult {
        measures_per_minute: None,
        dance_abbreviations: vec![],
        plus_flag: false,
    };

    // Find the first alphabetic character from the back, i.e.
    // everything beyond that is the MPM and flag part of the
    // dance part, everything before the dance abbreviations.
    let end_pos = dance_part
        .rfind(char::is_alphabetic)
        .map_or(0, |pos| pos + 1);

    let mut mpm_part = &dance_part[end_pos..];
    if mpm_part.ends_with('+') {
        result.plus_flag = true;
        mpm_part = &mpm_part[..mpm_part.len() - 1];
    }
    result.measures_per_minute = mpm_part.parse().ok();

    result.dance_abbreviations = dance_part[0..end_pos]
        .split('-')
        .filter(|x| !x.is_empty())
        .map(|x| x.to_owned())
        .collect();

    result
}

/// Try to parse the `title` according to the following structure:
///
/// * RB Test → a Rumba with title Test
/// * RB23 Test → a Rumba of 23 mpm with title Test
/// * DF-CC Test → a Disco Fox and Cha Cha Cha track with title Test
/// * RB Test (from Movie) → a Rumba with title Test from movie Movie.
///
/// Returns an incompletely populated [`ExtractedMetadata`].
#[must_use]
pub fn parse(title: &str) -> ExtractedMetadata {
    let mut result = ExtractedMetadata {
        title: None,
        artists: vec![],
        remix: None,
        movie: None,
        comment: None,
        dance_abbreviations: vec![],
        measures_per_minute: None,
    };

    let start_pos = title
        .find(' ')
        .filter(|&pos| {
            title[..pos]
                .chars()
                .all(|c| c.is_ascii_digit() || c.is_uppercase() || c == '-' || c == '+')
        })
        .map_or(0, |pos| {
            // TODO: use plus flag
            let dance_part = &title[..pos];
            let DancePartParseResult {
                measures_per_minute,
                dance_abbreviations,
                plus_flag: _,
            } = parse_dance_part(dance_part);

            result.measures_per_minute = measures_per_minute;
            result.dance_abbreviations = dance_abbreviations;

            (pos + 1).min(title.len() - 1)
        });

    let end_pos = title
        .find('(')
        .filter(|_| title.ends_with(')'))
        .map_or(title.len(), |pos| {
            let paren_content = &title[(pos + 1).min(title.len() - 1)..title.len() - 1];
            let ParensParseResult {
                remix,
                from,
                comment,
            } = parse_paren(paren_content);

            result.remix = remix.map(|title| title.into());
            result.movie = from.map(|title| title.into());
            result.comment = comment.map(|comm| comm.into());

            pos
        });

    result.title = Some(title[start_pos..end_pos].trim().into());

    result
}
