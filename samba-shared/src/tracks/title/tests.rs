use super::{parse, parse_dance_part, parse_paren, DancePartParseResult, ParensParseResult};

// Test parsing of the parentheses' content.

#[test]
fn identify_movie_in_parens_part() {
    assert_eq!(
        ParensParseResult {
            remix: None,
            from: Some("Me, Myself and I"),
            comment: None,
        },
        parse_paren("from Me, Myself and I")
    );
}

#[test]
fn identify_non_movie_as_comment_in_parens_part() {
    assert_eq!(
        ParensParseResult {
            remix: None,
            from: None,
            comment: Some("Test comment"),
        },
        parse_paren("Test comment")
    );
}

#[test]
fn identify_remix_in_parens_part() {
    assert_eq!(
        ParensParseResult {
            remix: Some("A B C Remix"),
            from: None,
            comment: None,
        },
        parse_paren("A B C Remix")
    );
}

#[test]
fn identify_remix_and_comment_in_parens_part() {
    assert_eq!(
        ParensParseResult {
            remix: Some("A B C Remix"),
            from: None,
            comment: Some("Test, Test2"),
        },
        parse_paren("Test, Test2, A B C Remix")
    );
}

#[test]
fn identify_movie_and_remix_in_parens_part() {
    assert_eq!(
        ParensParseResult {
            remix: Some("A B C Remix"),
            from: Some("Test"),
            comment: None,
        },
        parse_paren("from Test, A B C Remix")
    );
}

// Test parsing of the dance part.

#[test]
fn identify_single_dance_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: None,
            dance_abbreviations: vec!["RB".into()],
            plus_flag: false,
        },
        parse_dance_part("RB")
    );
}

#[test]
fn identify_single_dance_with_bpm_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: Some(23.try_into().expect("23 > 0")),
            dance_abbreviations: vec!["RB".into()],
            plus_flag: false,
        },
        parse_dance_part("RB23")
    );
}

#[test]
fn identify_single_dance_with_bpm_and_flag_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: Some(23.try_into().expect("23 > 0")),
            dance_abbreviations: vec!["RB".into()],
            plus_flag: true,
        },
        parse_dance_part("RB23+")
    );
}

#[test]
fn identify_multiple_dances_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: None,
            dance_abbreviations: vec!["RB".into(), "WCS".into()],
            plus_flag: false,
        },
        parse_dance_part("RB-WCS")
    );
}

#[test]
fn identify_multiple_dances_with_flag_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: None,
            dance_abbreviations: vec!["RB".into(), "WCS".into()],
            plus_flag: true,
        },
        parse_dance_part("RB-WCS+")
    );
}

#[test]
fn identify_multiple_dances_with_bpm_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: Some(23.try_into().expect("23 > 0")),
            dance_abbreviations: vec!["RB".into(), "WCS".into()],
            plus_flag: false,
        },
        parse_dance_part("RB-WCS23")
    );
}

#[test]
fn identify_multiple_dances_with_bpm_and_flag_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: Some(23.try_into().expect("23 > 0")),
            dance_abbreviations: vec!["RB".into(), "WCS".into()],
            plus_flag: true,
        },
        parse_dance_part("RB-WCS23+")
    );
}

#[test]
fn identify_only_bpm_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: Some(32.try_into().expect("32 > 0")),
            dance_abbreviations: vec![],
            plus_flag: false,
        },
        parse_dance_part("32")
    );
}

#[test]
fn identify_bpm_and_flag_dance_part() {
    assert_eq!(
        DancePartParseResult {
            measures_per_minute: Some(32.try_into().expect("32 > 0")),
            dance_abbreviations: vec![],
            plus_flag: true,
        },
        parse_dance_part("32+")
    );
}

// Test composite title parsing.

#[test]
fn keep_mono_title() {
    assert_eq!(parse("Test").title, Some("Test".into()));
}
#[test]
fn keep_mono_title_with_spaces() {
    assert_eq!(parse("This is a test").title, Some("This is a test".into()));
}

#[test]
fn split_single_dance() {
    let parse_result = parse("RB Test");
    assert_eq!(parse_result.title, Some("Test".into()));
    assert_eq!(parse_result.dance_abbreviations, vec!["RB".to_owned()]);
}
#[test]
fn split_single_dance_with_spaces() {
    let parse_result = parse("RB This is a test");
    assert_eq!(parse_result.title, Some("This is a test".into()));
    assert_eq!(parse_result.dance_abbreviations, vec!["RB".to_owned()]);
}
#[test]
fn split_single_dance_with_mpm() {
    let parse_result = parse("RB23 Test");
    assert_eq!(parse_result.title, Some("Test".into()));
    assert_eq!(parse_result.dance_abbreviations, vec!["RB".to_owned()]);
    assert_eq!(
        parse_result.measures_per_minute,
        Some(23.try_into().expect("23 > 0"))
    );
}
#[test]
fn split_single_dance_with_mpm_and_spaces() {
    let parse_result = parse("RB23 This is a test");
    assert_eq!(parse_result.title, Some("This is a test".into()));
    assert_eq!(parse_result.dance_abbreviations, vec!["RB".to_owned()]);
    assert_eq!(
        parse_result.measures_per_minute,
        Some(23.try_into().expect("23 > 0"))
    );
}

#[test]
fn split_multiple_dances_with_mpm() {
    let parse_result = parse("RB-WCS23 Test");
    assert_eq!(parse_result.title, Some("Test".into()));
    assert_eq!(parse_result.dance_abbreviations, vec![
        "RB".to_owned(),
        "WCS".to_owned()
    ]);
    assert_eq!(
        parse_result.measures_per_minute,
        Some(23.try_into().expect("23 > 0"))
    );
}
#[test]
fn split_multiple_dances_with_mpm_and_spaces() {
    let parse_result = parse("RB-WCS23 This is a test");
    assert_eq!(parse_result.title, Some("This is a test".into()));
    assert_eq!(parse_result.dance_abbreviations, vec![
        "RB".to_owned(),
        "WCS".to_owned()
    ]);
    assert_eq!(
        parse_result.measures_per_minute,
        Some(23.try_into().expect("23 > 0"))
    );
}

#[test]
fn split_multiple_dances() {
    let parse_result = parse("RB-WCS This is a test");
    assert_eq!(parse_result.title, Some("This is a test".into()));
    assert_eq!(parse_result.dance_abbreviations, vec![
        "RB".to_owned(),
        "WCS".to_owned()
    ]);
}

#[test]
fn identify_movie_in_mono_title() {
    let parse_result = parse("Test (from Movie)");
    assert_eq!(parse_result.title, Some("Test".into()));
    assert_eq!(parse_result.movie, Some("Movie".into()));
}
#[test]
fn identify_movie_in_title_with_dance() {
    let parse_result = parse("RB Test (from Movie)");
    assert_eq!(parse_result.title, Some("Test".into()));
    assert_eq!(parse_result.movie, Some("Movie".into()));
    assert_eq!(parse_result.dance_abbreviations, vec!["RB".to_owned()]);
}
#[test]
fn identify_movie_in_title_with_dance_and_spaces() {
    let parse_result = parse("RB This is a test (from Movie)");
    assert_eq!(parse_result.title, Some("This is a test".into()));
    assert_eq!(parse_result.movie, Some("Movie".into()));
    assert_eq!(parse_result.dance_abbreviations, vec!["RB".to_owned()]);
}
#[test]
fn identify_long_movie_in_title_with_dance() {
    let parse_result = parse("RB Test (from Long Movie)");
    assert_eq!(parse_result.title, Some("Test".into()));
    assert_eq!(parse_result.movie, Some("Long Movie".into()));
    assert_eq!(parse_result.dance_abbreviations, vec!["RB".to_owned()]);
}
#[test]
fn identify_movie_in_title_with_multiple_dances() {
    let parse_result = parse("RB-WCS Test (from Movie)");
    assert_eq!(parse_result.title, Some("Test".into()));
    assert_eq!(parse_result.movie, Some("Movie".into()));
    assert_eq!(parse_result.dance_abbreviations, vec![
        "RB".to_owned(),
        "WCS".to_owned()
    ]);
}
#[test]
fn identify_movie_in_title_with_dance_and_mpm() {
    let parse_result = parse("RB23 Test (from Movie)");
    assert_eq!(parse_result.title, Some("Test".into()));
    assert_eq!(parse_result.movie, Some("Movie".into()));
    assert_eq!(parse_result.dance_abbreviations, vec!["RB".to_owned()]);
    assert_eq!(
        parse_result.measures_per_minute,
        Some(23.try_into().expect("23 > 0"))
    );
}
