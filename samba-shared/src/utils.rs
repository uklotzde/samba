use std::time::Duration;

/// Format duration milliseconds as string.
#[must_use]
pub fn format_duration_millis(duration: u128) -> String {
    let seconds = (duration / 1000) % 60;
    let minutes = (duration / 60_000) % 60;
    let hours = (duration - (minutes * 60 + seconds) * 1000) / 3_600_000;
    if hours > 0 {
        format!("{hours:#02}:{minutes:#02}:{seconds:#02}")
    } else {
        format!("{minutes:#02}:{seconds:#02}")
    }
}

/// Determine how long it will take to dance a given number of beats when
/// dancing a strict tempo of given beats per minute (we cannot use measures
/// per minute because of missing dance timing details). Calculates with
/// millisecond precision.
#[must_use]
pub const fn duration_of_beats(beats: usize, bpm: usize) -> Duration {
    Duration::from_millis((beats as u64 * 60_000).div_ceil(bpm as u64))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn seconds_no_minutes() {
        assert_eq!("00:00", &format_duration_millis(0));
        assert_eq!("00:00", &format_duration_millis(999));
        assert_eq!("00:01", &format_duration_millis(1000));
        assert_eq!("00:09", &format_duration_millis(9999));
        assert_eq!("00:10", &format_duration_millis(10000));
        assert_eq!("00:10", &format_duration_millis(10999));
        assert_eq!("00:59", &format_duration_millis(59000));
        assert_eq!("00:59", &format_duration_millis(59999));
    }

    #[test]
    fn minutes_no_hours() {
        assert_eq!("01:00", &format_duration_millis(60 * 1000));
        assert_eq!("01:00", &format_duration_millis(60 * 1000 + 999));
        assert_eq!("01:01", &format_duration_millis(61 * 1000));
        assert_eq!("01:09", &format_duration_millis(69 * 1000 + 999));
        assert_eq!("01:10", &format_duration_millis(70 * 1000));
        assert_eq!("06:00", &format_duration_millis(6 * 60 * 1000));
        assert_eq!("09:59", &format_duration_millis((9 * 60 + 59) * 1000));
        assert_eq!("10:00", &format_duration_millis(10 * 60 * 1000));
        assert_eq!("10:01", &format_duration_millis((10 * 60 + 1) * 1000));
        assert_eq!("10:10", &format_duration_millis((10 * 60 + 10) * 1000));
        assert_eq!("59:59", &format_duration_millis((59 * 60 + 59) * 1000));
        assert_eq!(
            "59:59",
            &format_duration_millis((59 * 60 + 59) * 1000 + 999)
        );
    }

    #[test]
    fn hours_no_days() {
        assert_eq!("01:00:00", &format_duration_millis(60 * 60 * 1000));
        assert_eq!("01:01:00", &format_duration_millis((60 + 1) * 60 * 1000));
        assert_eq!("01:10:00", &format_duration_millis((60 + 10) * 60 * 1000));
        assert_eq!(
            "01:10:01",
            &format_duration_millis(((60 + 10) * 60 + 1) * 1000)
        );
        assert_eq!(
            "01:10:10",
            &format_duration_millis(((60 + 10) * 60 + 10) * 1000)
        );
        assert_eq!(
            "09:59:59",
            &format_duration_millis(((9 * 60 + 59) * 60 + 59) * 1000)
        );
        assert_eq!(
            "09:59:59",
            &format_duration_millis(((9 * 60 + 59) * 60 + 59) * 1000 + 999)
        );
        assert_eq!("10:00:00", &format_duration_millis(10 * 60 * 60 * 1000));
        assert_eq!(
            "10:01:00",
            &format_duration_millis((10 * 60 + 1) * 60 * 1000)
        );
        assert_eq!(
            "10:10:00",
            &format_duration_millis((10 * 60 + 10) * 60 * 1000)
        );
        assert_eq!(
            "10:10:01",
            &format_duration_millis(((10 * 60 + 10) * 60 + 1) * 1000)
        );
        assert_eq!(
            "10:10:10",
            &format_duration_millis(((10 * 60 + 10) * 60 + 10) * 1000)
        );
        assert_eq!(
            "23:59:59",
            &format_duration_millis(((23 * 60 + 59) * 60 + 59) * 1000)
        );
        assert_eq!(
            "23:59:59",
            &format_duration_millis(((23 * 60 + 59) * 60 + 59) * 1000 + 999)
        );
    }

    #[test]
    fn days() {
        assert_eq!("24:00:00", &format_duration_millis(24 * 60 * 60 * 1000));
        assert_eq!(
            "24:01:00",
            &format_duration_millis((24 * 60 + 1) * 60 * 1000)
        );
        assert_eq!(
            "24:10:00",
            &format_duration_millis((24 * 60 + 10) * 60 * 1000)
        );
        assert_eq!(
            "24:01:01",
            &format_duration_millis(((24 * 60 + 1) * 60 + 1) * 1000)
        );
        assert_eq!(
            "24:01:10",
            &format_duration_millis(((24 * 60 + 1) * 60 + 10) * 1000)
        );
        assert_eq!(
            "24:11:11",
            &format_duration_millis(((24 * 60 + 11) * 60 + 11) * 1000)
        );
        assert_eq!("26:00:00", &format_duration_millis(26 * 60 * 60 * 1000));
        assert_eq!("100:00:00", &format_duration_millis(100 * 60 * 60 * 1000));
    }

    #[test]
    fn beats_to_time_conversion() {
        assert_eq!(Duration::from_secs(1), duration_of_beats(3, 180));
        assert_eq!(Duration::from_secs(60), duration_of_beats(180, 180));
        assert_eq!(Duration::from_millis(7200), duration_of_beats(24, 200));
    }
}
