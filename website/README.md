# samba's website

## Repository contents

This respository contains by and large the following components:

* blog posts and description pages served at https://ballroom-dancing.gitlab.io/samba,
* static assets belonging to the website,
* custom [Zola](https://www.getzola.org) shortcodes and templates,
* customizations to the [AdiDoks](https://github.com/aaranxu/adidoks) theme.

Build the site yourself using `zola build`.
