+++
title = "samba"

# The homepage contents
[extra]
lead = '''
<b>samba</b> is your support tool for ballroom practice. Primarily it is work
in progress. For now, please refer to the <a href="/samba/blog">blog</a> and
the project's <a href="https://gitlab.com/ballroom-dancing">repository</a> for
more details.
'''
url = "/TODO"
url_button = "Download"

# Menu items
[[extra.list]]
title = "Powerful music library queries"
content = '''
{{ image(
  path = "frontpage/library.jpg",
  alt = "filtering your music library with powerful queries"
) }}
'''

[[extra.list]]
title = "Manage your practice sessions"
content = '''
{{ image(
  path = "frontpage/practice-session.jpg",
  alt = "edit practice sessions with flexible queries"
) }}
'''
+++
