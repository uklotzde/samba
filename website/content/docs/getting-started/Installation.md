+++
title = "Download"
description = "Download samba from GitLab or compile it from source."
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
# lead = ""
toc = true
top = false
+++
